<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Customer extends MY_Controller {
	public function index()
	{
		$data = array();
		if (isset($_POST["customer"])) {
           $data["search"] = $_POST["customer"];
        }
        $this->load->model("Line_Model");
        $this->load->model("Customer_Model");
        $data["getActiveLine"]  = $this->Line_Model->getActiveLine();
		$data['getCustomerList']  = $this->Customer_Model->customerList();
		$data['subview'] = $this->load->view('admin/customer-list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function addcustomer()
	{
		$data = array();
		$cus_id=$this->uri->segment(4);
		$this->load->model("Customer_Model");
		$customerGetDetails  = $this->Customer_Model->addCustomer();
		if ($customerGetDetails=="updated") {
			$this->session->set_flashdata('message_success', 'Customer updated successfully');
			redirect('admin/customer');
	    } else if (!empty($customerGetDetails) && $customerGetDetails != "updated") {
	    	$this->session->set_flashdata('message_success', 'Customer added successfully');
			redirect('admin/customer');
	    } else {
		    if (!empty($cus_id)) {
             	$data["getSingleCustomer"]  = $this->Customer_Model->getSingleCustomer($cus_id);
		    }
			$this->load->model("Line_Model");
			$data["getActiveLine"]  = $this->Line_Model->getActiveLine();
			$data['subview'] = $this->load->view('admin/add-customer', $data, TRUE);
			$this->load->view('admin/_layout_main', $data);
	    }
	}
	public function editcustomer()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/edit-customer', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
}