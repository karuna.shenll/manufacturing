<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Roles extends MY_Controller {

	public function index()
	{
		$data = array();
		if (isset($_POST["role"])) {
           $data["search"] = $_POST["role"];
        }
		$this->load->model("Role_Model");
		$data['getRoleList'] = $this->Role_Model->roleList();
		$data['subview'] = $this->load->view('admin/role-list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function addrole()
	{
		$data = array();
		$role_id=$this->uri->segment(4);
		$this->load->model("Role_Model");
		$roleGetDetails  = $this->Role_Model->addRole();
		if ($roleGetDetails=="updated") {
			$this->session->set_flashdata('message_success', 'Role updated successfully');
			redirect('admin/roles');
	    } else if (!empty($roleGetDetails) && $roleGetDetails != "updated") {
	    	$this->session->set_flashdata('message_success', 'Role added successfully');
			redirect('admin/roles');
	    } else {
		    if (!empty($role_id)) {
             	$data["getSingleRole"]  = $this->Role_Model->getSingleRole($role_id);
		    }
			$data['subview'] = $this->load->view('admin/add-role', $data, TRUE);
			$this->load->view('admin/_layout_main', $data);
	    }
	}
	public function editrole()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/edit-role', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function role_name_exists(){
    	$role_id   =  $_POST["role_id"];
		$role_name = $_POST["role_name"];
		if(empty($role_id)){
			$this->db->where('role_name', $role_name);
			$query = $this->db->get('tbl_role');
			if ( $query->num_rows() > 0 ) {
				echo 'false';
                exit;
			} else {
				echo 'true';
                exit;
			}
		} else {
			echo 'true';
            exit;
		}
	}
}
