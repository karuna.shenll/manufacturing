<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Dashboard extends MY_Controller {

	public function index()
	{
		$data = array();
		$this->load->model("Dashboard_Model");
		$data["get_equp"]  = $this->Dashboard_Model->equipment_list();
		$data['subview'] = $this->load->view('admin/dashboard', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function layout($equ_id)
	{
		
		$data = array();
		$this->load->model("Dashboard_Model");
		$data["equp_task"]  = $this->Dashboard_Model->equipment_task($equ_id);
		$data['subview'] = $this->load->view('admin/layout', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function viewreport($equ_id)
	{
		$data = array();
		if (!empty($_POST['report'])) {
			$data['search'] = $_POST['report'];
		}

		$this->load->model("Dashboard_Model");
		$data["view_report"]  = $this->Dashboard_Model->viewreport($equ_id);
		$data['subview'] = $this->load->view('admin/layout-report', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function layouttask($task_log_id)
	{
		$data = array();
		$this->load->model("Dashboard_Model");
		$data["task_report"]  = $this->Dashboard_Model->task_report($task_log_id);
		$data['subview'] = $this->load->view('admin/layout-task', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
}
