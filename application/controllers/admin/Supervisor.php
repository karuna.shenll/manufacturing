<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Supervisor extends MY_Controller {
	
	public function index()
	{
		$data = array();
		if (isset($_POST["supervisor"])) {
           $data["search"] = $_POST["supervisor"];
        }
		$this->load->model("Supervisor_Model");
		$this->load->model("Area_Model");
		$data["getActiveArea"]  = $this->Area_Model->getActiveArea();
		$data['getSupervisorList'] = $this->Supervisor_Model->supervisorList();
		$data['subview'] = $this->load->view('admin/supervisor-list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function addsupervisor()
	{
		$data = array();
		$supervisor_id=$this->uri->segment(4);
		$this->load->model("Supervisor_Model");
		$this->load->model("Area_Model");
		$this->load->model("Line_Model");
		$this->load->model("Role_Model");
		$supervisorGetDetails  = $this->Supervisor_Model->addSupervisor();
		if ($supervisorGetDetails=="updated") {
			$this->session->set_flashdata('message_success', 'Supervisor updated successfully');
			redirect('admin/supervisor');
	    } else if (!empty($supervisorGetDetails) && $supervisorGetDetails != "updated") {
	    	$this->session->set_flashdata('message_success', 'Supervisor added successfully');
			redirect('admin/supervisor');
	    } else {
		    if (!empty($supervisor_id)) {
             	$data["getSingleSupervisor"]  = $this->Supervisor_Model->getSingleSupervisor($supervisor_id);
		    }
		    $data["getActiveArea"]  = $this->Area_Model->getActiveArea();
		    $data["getActiveLine"]  = $this->Line_Model->getActiveLine();
		    $data["getRoleLine"]    = $this->Role_Model->getActiveRole();
			$data['subview'] = $this->load->view('admin/add-supervisor', $data, TRUE);
			$this->load->view('admin/_layout_main', $data);
	    }
	}
	public function editsupervisor()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/edit-supervisor', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function supervisor_name_exists(){
    	$supervisor_id   =  $_POST["supervisor_id"];
		$emp_id = $_POST["emp_id"];
		if(empty($supervisor_id)){
			$this->db->where('emp_id', $emp_id);
			$query = $this->db->get('tbl_supervisor');
			if ( $query->num_rows() > 0 ) {
				echo 'false';
                exit;
			} else {
				echo 'true';
                exit;
			}
		} else {
			echo 'true';
            exit;
		}
	}
}
