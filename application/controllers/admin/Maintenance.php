<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Maintenance extends MY_Controller {

	public function index()
	{
		$data = array();
		if (isset($_POST["maintenance"])) {
           $data["search"] = $_POST["maintenance"];
        }
		$this->load->model("Group_Model");
		$this->load->model("Maintenance_Model");
		$data["getArrGroup"]    = $this->Group_Model->getActiveGroup();
		$data["getmaintenanceList"]    = $this->Maintenance_Model->maintenanceList();
		$data['subview'] = $this->load->view('admin/maintenance-list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function addmaintenance()
	{
		$data = array();
		$mainten_id=$this->uri->segment(4);
		$this->load->model("Maintenance_Model");
		$maintenanceGetDetails  = $this->Maintenance_Model->addMaintenance();
		if ($maintenanceGetDetails=="updated") {
			$this->session->set_flashdata('message_success', 'Maintenance updated successfully');
			redirect('admin/maintenance');
	    } else if (!empty($maintenanceGetDetails) && $maintenanceGetDetails != "updated") {
	    	$this->session->set_flashdata('message_success', 'Maintenance added successfully');
			redirect('admin/maintenance');
	    } else {
		    if (!empty($mainten_id)) {
             	$data["getSingleMaintenance"]  = $this->Maintenance_Model->getSingleMaintenance($mainten_id);
		    }
		    $this->load->model("Group_Model");
			$this->load->model("Line_Model");
			$this->load->model("Equipment_Model");
			$data["getArrGroup"]         = $this->Group_Model->getActiveGroup();
			$data["getActiveLine"]       = $this->Line_Model->getActiveLine();
			$data["getActiveEquipment"]  = $this->Equipment_Model->getEquipmentLine();
			$data['subview'] = $this->load->view('admin/add-maintenance', $data, TRUE);
			$this->load->view('admin/_layout_main', $data);
	    }
		
	}
	public function editmaintenance()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/edit-maintenance', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
}
