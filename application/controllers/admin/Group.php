<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Group extends MY_Controller {

	public function index()
	{
		$data = array();
		if (isset($_POST["group"])) {
           $data["search"] = $_POST["group"];
        }
		$this->load->model("Group_Model");
		$data['getGroupList'] = $this->Group_Model->groupList();
		$data['subview'] = $this->load->view('admin/group-list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function addgroup()
	{
		$data = array();
		$group_id=$this->uri->segment(4);
		$this->load->model("Group_Model");
		$groupGetDetails  = $this->Group_Model->addGroup();
		if ($groupGetDetails=="updated") {
			$this->session->set_flashdata('message_success', 'Group updated successfully');
			redirect('admin/group');
	    } else if (!empty($groupGetDetails) && $groupGetDetails != "updated") {
	    	$this->session->set_flashdata('message_success', 'Group added successfully');
			redirect('admin/group');
	    } else {
		    if (!empty($group_id)) {
             	$data["getSingleGroup"]  = $this->Group_Model->getSingleGroup($group_id);
		    }
			$data['subview'] = $this->load->view('admin/add-group', $data, TRUE);
			$this->load->view('admin/_layout_main', $data);
	    }
	}
	public function editgroup()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/edit-group', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function group_name_exists(){
    	$group_id   =  $_POST["group_id"];
		$group_name = $_POST["group_name"];
		if(empty($group_id)){
			$this->db->where('group_name', $group_name);
			$query = $this->db->get('tbl_group');
			if ( $query->num_rows() > 0 ) {
				echo 'false';
                exit;
			} else {
				echo 'true';
                exit;
			}
		} else {
			echo 'true';
            exit;
		}
	}
}
