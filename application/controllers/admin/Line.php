<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Line extends MY_Controller {

	public function index()
	{
		$data = array();
		if (isset($_POST["line"])) {
           $data["search"] = $_POST["line"];
        }
        $this->load->model("Area_Model");
		$this->load->model("Line_Model");
		$data["getArrArea"]  = $this->Area_Model->getActiveArea();
		$data['getLineList']  = $this->Line_Model->lineList();
		$data['subview'] = $this->load->view('admin/line-list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function addline()
	{
		$data = array();
		$this->load->model("Area_Model");
		$this->load->model("Equipment_Model");
		$this->load->model("Line_Model");
		$line_id=$this->uri->segment(4);
		$lineGetDetails  = $this->Line_Model->addLine();
		if ($lineGetDetails=="updated") {
			$this->session->set_flashdata('message_success', 'Line updated successfully');
			redirect('admin/line');
	    } else if (!empty($lineGetDetails) && $lineGetDetails != "updated") {
	    	$this->session->set_flashdata('message_success', 'Line added successfully');
			redirect('admin/line');
	    } else {
		    if (!empty($line_id)) {
             	$data["getSingleLine"]  = $this->Line_Model->getSingleLine($line_id);
             	$data["getArrLogLine"]  = $this->Line_Model->getLogLine($line_id);
		    }
			$data["getArrArea"]  = $this->Area_Model->getActiveArea();
			$data["getArrEquipment"]  = $this->Equipment_Model->getEquipmentLine();
			$data['subview'] = $this->load->view('admin/add-line', $data, TRUE);
			$this->load->view('admin/_layout_main', $data);
	    }
	}
	public function editline()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/edit-line', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function reports()
	{
        $data = array();
		$data['subview'] = $this->load->view('admin/reports', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
}
