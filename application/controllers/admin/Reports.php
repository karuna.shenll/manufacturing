<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll 
 */
class Reports extends MY_Controller {

	public function index()
	{
		$data = array();
		if (isset($_POST["report"])) {
			$data["search"] = $_POST["report"];
		}
		$this->load->model("Report_Model");
		$data['getReportList'] = $this->Report_Model->reportList();
		$data['subview'] = $this->load->view('admin/reports-list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function viewreports($line_id)
	{
		$data = array();
		if (!empty($_POST['report'])) {
			$data['search'] = $_POST['report'];
		}
        $this->load->model("Report_Model");
		$data['viewreports'] = $this->Report_Model->viewreports($line_id);
		$data['subview'] = $this->load->view('admin/view-reports', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
}
