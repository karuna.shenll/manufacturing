<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Logout extends MY_Controller {
	public function index() {
		$this->load->database();
		$this->load->helper(array('form', 'url'));
		$this->load->library('session');
		$this->session->unset_userdata('message_faulier');
		$this->session->unset_userdata('__ci_last_regenerate');
		$this->session->sess_destroy();
		redirect('admin/login');
		$this->session->sess_destroy();
	}
}