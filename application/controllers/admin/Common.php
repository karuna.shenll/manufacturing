<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Common extends MY_Controller {
    public function cronjob()
    {
        $this->load->model('Common_Model');
        $data['getcronJobs'] = $this->Common_Model->cronJobs();
        $data['subview'] = $this->load->view('admin/cron-jobs', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
	public function commonDeleteAjax() {
		$this->load->model('Common_Model');
		$deleteid = $_POST["deleteid"];
		$deltype  = $_POST["deltype"];
		if (!empty($deleteid) && !empty($deltype)) {
            if (strtolower($deltype) == "area") {
                $field = "area_id";
                $table = "tbl_area";
            }
            if (strtolower($deltype) == "group") {
                $field = "group_id";
                $table = "tbl_group";
            }
            if (strtolower($deltype) == "equipment") {
                $field = "equ_id";
                $table = "tbl_equipment";
            }
            if(strtolower($deltype) == "role"){
            	$field = "role_id";
                $table = "tbl_role";
            }
            if(strtolower($deltype) == "supervisor"){
            	$field = "supervisor_id";
                $table = "tbl_supervisor";
            }
            if(strtolower($deltype) == "employee"){
            	$field = "e_id";
                $table = "tbl_employee";
            }
            if(strtolower($deltype) == "customer"){
            	$field = "cus_id";
                $table = "tbl_customer";
            }
            if(strtolower($deltype) == "maintenance"){
                $field = "mainten_id";
                $table = "tbl_maintenance";
            }
            if(strtolower($deltype) == "assigntask"){
                $field = "tbl_assign_task";
                $table = "tbl_assign_task";
            }
            if (strtolower($deltype) == "line") {
                $field    = "line_id";
                $table    = "tbl_line";
                $logfield = "line_id";
                $tables   = "tbl_line_log";
                $logdata  =  $this->Common_Model->DeleteLog($deleteid,$logfield,$tables);
            }
            if (strtolower($deltype) == "task") {
                $field    = "task_id";
                $table    = "tbl_task";
                $logfield = "task_id";
                $tables   = "tbl_task_log";
                $logdata  =  $this->Common_Model->DeleteLog($deleteid,$logfield,$tables);
            }
            $senddata =  $this->Common_Model->Delete($deleteid,$field,$table);
	        if ($senddata) {
	        	$msg = ucfirst($deltype).' deleted successfully';
	            $this->session->set_flashdata('message_failure',$msg);
	            echo "success"; exit;
	        } else {
	           echo "failure"; exit;
	        }
		} else {
			echo "failure"; exit;
		}
	}
    public function commonAddtaskDeleteAjax() {
        $this->load->model('Common_Model');
        $deleteid = $_POST["deleteid"];
        $deltype  = $_POST["deltype"];
        if (!empty($deleteid) && !empty($deltype)) {
            $field   = "task_log_id";
            $table   = "tbl_task_log";
            $this->load->model('Common_Model');
            $senddata =  $this->Common_Model->Delete($deleteid,$field,$table);
            if ($senddata) {
                $msg = 'task deleted successfully';
                $this->session->set_flashdata('message_failure',$msg);
                echo "success"; exit;
            } else {
               echo "failure"; exit;
            }
        } else {
           echo "failure"; exit;
        }
    }
	public function commonLineEquAjax() { 
		$lineid = $_POST["popupid"];
		$rows='';
		if (!empty($lineid)) {
				$query= $this->db->select('*')
	                          ->from('tbl_line as line')
	                          ->Join('tbl_area as area','area.area_id=line.area_id','JOIN')
	                          ->where("line.line_id=".$lineid)
	                          ->order_by('line.line_id','desc')
	                          ->get();
	        $Linerows = $query->num_rows();
	        if ($Linerows > 0) {
	        	$rows.="<table class='table table-striped table-bordered table-hover' id='viewtable'>";
	            foreach($query->result() as $lineData) {
	            	$line_id   = $lineData->line_id;
                    $line_name = $lineData->line_name;
                    $doc_no    = $lineData->doc_no;
                    $area_name = $lineData->area_name;
                    $revision  = $lineData->revision_add;
                    $rows.="<tr><td><b>Line Name</b></td><td>".$line_name."</td></tr>";
                    $rows.="<tr><td><b>Doc Number</b></td><td>".$doc_no."</td></tr>";
                    $rows.="<tr><td><b>Area Name</b></td><td>".$area_name."</td></tr>";
                    $rows.="<tr><td><b>Revision</b></td><td>".$revision."</td></tr>";
	            }
	            $sqlQry= $this->db->select('*')
	                          ->from('tbl_line_log as linelog')
	                          ->Join('tbl_equipment as equ','equ.equ_id=linelog.equ_id','JOIN')
	                          ->where("linelog.line_id=".$line_id)
	                          ->order_by('linelog.line_log_id','desc')
	                          ->get();
	            $lineLogrows = $sqlQry->num_rows();
	            if ($lineLogrows > 0) {
	            	$j=1;
                    foreach($sqlQry->result() as $lineLogData) {
                    	$equ_name = $lineLogData->equ_name;
                    	$name='';
                    	if($j==1) {
                    		$name="Equipment Name";
                    	}
                    	$rows.="<tr><td><b>".$name."</b></td><td>".$equ_name."</td></tr>";
                    	$j++;
                    }
	            }
	            $rows.="</table>";
	            echo "success##^^##".$rows;
	        } else {
	            return false;
	        }
		}
	}

    public function commonEquipmentAjax() {
        $equipmentid = $_POST["equipmentid"];
        if (!empty($equipmentid)) {
            $query= $this->db->select('*')
                              ->from('tbl_equipment')
                              ->where('equ_id',$equipmentid)
                              ->get();
            $equrows = $query->num_rows();
            if ($equrows > 0) {
                foreach ($query->result() as $row) {
                    echo "success##$$##".$row->equ_code."##$$##".$row->equ_model;
                }
            } else {
                return false;
            }
        }
    }
    public function commonEmployeeAjax(){
        $employeeid = $_POST["employeeid"];
        if (!empty($employeeid)) {
            $query= $this->db->select('*')
                              ->from('tbl_employee')
                              ->where('emp_id',$employeeid)
                              ->get();
            $emprows = $query->num_rows();
            if ($emprows > 0) {
                foreach ($query->result() as $row) {
                    echo "success##$$##".$row->emp_name."##$$##".$row->e_id;
                }
            } else {
                return false;
            }
        }
    }
    public function commonTaskPopupAjax() {
        $taskid = $_POST["task_id"];
        if($taskid!="") {
            $query= $this->db->select('task.task_id, task.equ_id, equ.equ_code, equ.equ_model, equ.equ_name, mainten.frequency, mainten.created_date, line.line_id, line.line_name, line.revision_add,line.modified_date,area.area_name,area.area_id')
                      ->from('tbl_task as task')
                      ->Join('tbl_equipment as equ','equ.equ_id=task.equ_id','JOIN')
                      ->Join('tbl_maintenance as mainten','mainten.equ_id=task.equ_id','JOIN')
                      ->Join('tbl_line as line','line.line_id=mainten.line_id','JOIN')
                      ->Join('tbl_area as area','area.area_id=line.area_id','JOIN')
                      ->where("task.task_id=".$taskid)
                      ->order_by('task.task_id','asc')
                      ->get();
            $Taskrows = $query->num_rows();
            if ($Taskrows > 0) {
                foreach ($query->result() as $rows) {
                    $taskid    = $rows->task_id;
                    $line_name = $rows->line_id;
                    $area_name = $rows->area_id;
                    $equ_code  = $rows->equ_code;
                    $equ_model = $rows->equ_model;
                    $equ_name  = $rows->equ_id;
                    $frequency = $rows->frequency;
                }
                echo "success##$$##".$taskid."##$$##".$line_name."##$$##".$area_name."##$$##".$equ_code."##$$##".$equ_name."##$$##".$frequency;
            } else {
               return false;
            }
        } else {
            return false;
        }
    }
    public function commonAssignAjax() {
        $employeeid = $_POST["employeeid"];
        $taskid     = $_POST["task_id"];
        $empname    = $_POST["emp_name"];
        $lineid     = $_POST["line_id"];
        $areaid     = $_POST["area_id"];
        $equcode    = $_POST["equcode"];
        $equid      = $_POST["equid"];
        $frequency  = $_POST["frequency"];
        $datetimedb = date("Y-m-d H:i:s");
        $supervisorid=$this->session->userdata('supervisor_id');
        if (!empty($employeeid) && !empty($taskid)) {
            $data = array(
                "emp_id"  => $employeeid,
                "task_id" => $taskid,
                "emp_name"=> $empname,
                "line_id" => $lineid,
                "area_id" => $areaid,
                "equ_code"=> $equcode,
                "equ_id" => $equid,
                "assign_by" => $supervisorid,
                "created_date" =>$datetimedb,
                "modified_date" => $datetimedb
            );
            $this->db->insert('tbl_assign_task', $data);
            $assigntaskId=$this->db->insert_id();
            if($assigntaskId){
                $dataupdate = array(
                    "task_status" =>"Assigned",
                    "modified_date" => $datetimedb,
                );
                $this->db->where('task_id',$taskid);
                $this->db->update('tbl_task', $dataupdate);
            }
            $this->session->set_flashdata('success', 'Assign task added successfully');
            echo "success";
        } else {
            return false;
        }
    }
    public function commonAssignmeAjax() {
        $taskid     = $_POST["task_id"];
        if (!empty($taskid)) {
        $query= $this->db->select('task.task_id, task.equ_id, equ.equ_code, equ.equ_model, equ.equ_name, mainten.frequency, mainten.created_date, line.line_id, line.line_name, line.revision_add,line.modified_date,area.area_name,area.area_id')
                      ->from('tbl_task as task')
                      ->Join('tbl_equipment as equ','equ.equ_id=task.equ_id','JOIN')
                      ->Join('tbl_maintenance as mainten','mainten.equ_id=task.equ_id','JOIN')
                      ->Join('tbl_line as line','line.line_id=mainten.line_id','JOIN')
                      ->Join('tbl_area as area','area.area_id=line.area_id','JOIN')
                      ->where("task.task_id=".$taskid)
                      ->order_by('task.task_id','asc')
                      ->get();
            $Taskrows = $query->num_rows();
            if ($Taskrows > 0) {
                foreach ($query->result() as $rows) {
                    $data["task_id"] = $rows->task_id;
                    $data["line_id"] = $rows->line_id;
                    $data["area_id"] = $rows->area_id;
                    $data["equ_code"] = $rows->equ_code;
                    $data["equ_id"] = $rows->equ_id;

                }
                $datetimedb = date("Y-m-d H:i:s");
                $data["emp_id"]=$this->session->userdata('e_id');
                $data["emp_name"]=$this->session->userdata('emp_name');
                $data["created_date"]  = $datetimedb;
                $data["modified_date"] = $datetimedb;
                $this->db->insert('tbl_assign_task', $data);
                $assigntaskId=$this->db->insert_id();
                if($assigntaskId){
                    $dataupdate = array(
                        "task_status" =>"Assigned",
                        "modified_date" => $datetimedb,
                    );
                    $this->db->where('task_id',$taskid);
                    $this->db->update('tbl_task', $dataupdate);
                }
                $this->session->set_flashdata('message_success', 'Assign task added successfully');
                echo "success";
            }
        }
    }
    // public function commonStatusAjax() {
    //     $task_log_id = $_POST["task_log_id"];
    //     $task_id     = $_POST["task_id"];
    //     $selstatus   = $_POST["selstatus"];
    //     $datetimedb  = date("Y-m-d H:i:s");
    //     if (!empty($task_log_id) && !empty($task_id) && !empty($selstatus)) {
    //          $data = array(
    //             "log_status" =>$selstatus,
    //             "modified_date" => $datetimedb,
    //         );
    //         $this->db->where('task_id',$task_id);
    //         $this->db->where('task_log_id',$task_log_id);
    //         $this->db->update('tbl_task_log', $data);
         
    //         $stmt = $this->db->query("select * from tbl_task_log where task_id='" . $task_id . "' and log_status='Inprogress'");
    //         $rowcount = $stmt->num_rows();
    //         if($rowcount==0)
    //         {
    //         $dataupdate = array(
    //         "task_status" =>'Completed',
    //         "modified_date" => $datetimedb,
    //         );
    //         $this->db->where('task_id',$task_id);
    //         $this->db->update('tbl_task', $dataupdate);
    //         }  
    //         $this->session->set_flashdata('message_success', 'Status updated successfully');   
    //         echo "success";
    //     } else {
    //         return false;
    //     }
    // }
    public function commonGetStatusAjax() {
        $task_log_id = $_POST["task_log_id"];
        $task_id     = $_POST["task_id"];
        if(!empty($task_log_id) && !empty($task_id)){
              $query= $this->db->select('*')
                      ->from('tbl_task_log')
                      ->where("task_id=".$task_id)
                      ->where("task_log_id=".$task_log_id)
                      ->get();
            $Taskrows = $query->num_rows();
            if ($Taskrows > 0) {
                foreach ($query->result() as $rows) {
                    $log_status    = $rows->log_status;
                }
                echo "success##**##".$log_status;
            } else {
               return false;
            }
        } else {
            return false;
        }
    }
    public function commonTaskcompleteAjax(){
        list($day,$month,$year) = explode("/",$_POST["com_date"]);
        $observation  = $_POST["observation"];
        $action_taken = $_POST["action_taken"];
        $comments     = $_POST["comments"];
        $selstatus     = $_POST["selstatus"];
        $taskid       = $_POST["taskid"];
        $task_log_id  = $_POST["task_log_id"];
        $done_by      = $_POST["done_by"];
        $com_date     = $year."-".$month."-".$day;
        $rev_level    = $_POST["rev_level"];
        $verified_by  = $_POST["verified_by"];
        list($day,$month,$year) = explode("/",$_POST["rev_date"]);
        $rev_date     = $year."-".$month."-".$day;
        $datetimedb   = date("Y-m-d H:i:s");
        if(!empty($task_log_id) && !empty($action_taken) && !empty($comments)) {
            $data = array(
                "observation"  => $observation,
                "action_taken" => $action_taken,
                "comments"     => $comments,
                "log_status"     => $selstatus,
                "done_by"      => $done_by,
                //"log_status"    => "Completed",
                "verified_by"  => $verified_by,
                "completed_date" => $com_date,
                "revision_level" =>$rev_level,
                "revision_date" => $rev_date,                
                "modified_date" => $datetimedb,
            );
            //print_r($data); exit;
            $this->db->where('task_log_id',$task_log_id);
            $this->db->update('tbl_task_log', $data);

            // D
            $stmt = $this->db->query("select * from tbl_task_log where task_id='" . $taskid . "' and (log_status='Inprogress' or log_status IS NULL)");
            $rowcount = $stmt->num_rows();
            if($rowcount==0)
            {
            $dataupdate = array(
            "task_status" =>'Completed',
            "modified_date" => $datetimedb,
            );
            $this->db->where('task_id',$taskid);
            $this->db->update('tbl_task', $dataupdate);
            } 
            else{
            $dataupdate = array(
            "task_status" =>'Inprogress',
            "modified_date" => $datetimedb,
            );
            $this->db->where('task_id',$taskid);
            $this->db->update('tbl_task', $dataupdate);
            } 
            //
            $this->session->set_flashdata('message_success', 'Task completed successfully');
            echo "success";
        } else {
            return false;
        }
    }
}
