<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Area extends MY_Controller {

	public function index()
	{
		$data = array();
		if (isset($_POST["area"])) {
           $data["search"] = $_POST["area"];
        }
		$this->load->model("Area_Model");
		$data['getAreaList'] = $this->Area_Model->areaList();
		$data['subview'] = $this->load->view('admin/area-list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function addarea()
	{
		$data = array();
		$area_id=$this->uri->segment(4);
		$this->load->model("Area_Model");
		$areaGetDetails  = $this->Area_Model->addArea();
		if ($areaGetDetails=="updated") {
			$this->session->set_flashdata('message_success', 'Area updated successfully');
			redirect('admin/area');
	    } else if (!empty($areaGetDetails) && $areaGetDetails != "updated") {
	    	$this->session->set_flashdata('message_success', 'Area added successfully');
			redirect('admin/area');
	    } else {
		    if (!empty($area_id)) {
             	$data["getSingleArea"]  = $this->Area_Model->getSingleArea($area_id);
		    }
			$data['subview'] = $this->load->view('admin/add-area', $data, TRUE);
			$this->load->view('admin/_layout_main', $data);
	    }
	}
	public function editarea()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/edit-area', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function area_name_exists(){
    	$area_id   =  $_POST["area_id"];
		$area_name = $_POST["area_name"];
		if(empty($area_id)){
			$this->db->where('area_name', $area_name);
			$query = $this->db->get('tbl_area');
			if ( $query->num_rows() > 0 ) {
				echo 'false';
                exit;
			} else {
				echo 'true';
                exit;
			}
		} else {
			echo 'true';
            exit;
		}
	}
}
