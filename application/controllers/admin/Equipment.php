<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Equipment extends MY_Controller {
	public function index()
	{
		$data = array();
		if (isset($_POST["equipment"])) {
           $data["search"] = $_POST["equipment"];
        }
		$this->load->model("Group_Model");
		$this->load->model("Equipment_Model");
		$data["getArrGroup"]  = $this->Group_Model->getActiveGroup();
		$data["getEquipmentList"]  = $this->Equipment_Model->equipmentList();
		$data['subview'] = $this->load->view('admin/equipment-list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function addequipment()
	{
		$data = array();
		$equ_id=$this->uri->segment(4);
		$this->load->model("Equipment_Model");
		
		$equipmentGetDetails  = $this->Equipment_Model->addEquipment();
		if ($equipmentGetDetails=="updated") {
			$this->session->set_flashdata('message_success', 'Equipment updated successfully');
			redirect('admin/equipment');
	    } else if (!empty($equipmentGetDetails) && $equipmentGetDetails != "updated") {
	    	$this->session->set_flashdata('message_success', 'Equipment added successfully');
			redirect('admin/equipment');
	    } else {
		    if (!empty($equ_id)) {
             	$data["getSingleEquipment"]  = $this->Equipment_Model->getSingleEquipment($equ_id);
		    }
		    $this->load->model("Group_Model");
			$data["getArrGroup"]  = $this->Group_Model->getActiveGroup();
			//print_r($getArrGroup); exit;
			$data['subview'] = $this->load->view('admin/add-equipment', $data, TRUE);
			$this->load->view('admin/_layout_main', $data);
	    }
	}
	public function editequipment()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/edit-equipment', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
}
