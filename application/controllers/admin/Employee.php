<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Employee extends MY_Controller {
	public function index()
	{
		$data = array();
		if (isset($_POST["employee"])) {
           $data["search"] = $_POST["employee"];
        }
		$this->load->model("Employee_Model");
		$this->load->model("Area_Model");
		$data["getActiveArea"]  = $this->Area_Model->getActiveArea();
		$data['getEmployeeList'] = $this->Employee_Model->employeeList();
		$data['subview'] = $this->load->view('admin/employee-list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function addemployee()
	{   
		$data = array();
		$employee_id=$this->uri->segment(4);
		$this->load->model("Employee_Model");
		$this->load->model("Area_Model");
		$this->load->model("Line_Model");
		$this->load->model("Role_Model");
		$employeeGetDetails  = $this->Employee_Model->addEmployee();
		if ($employeeGetDetails=="updated") {
			$this->session->set_flashdata('message_success', 'Employee updated successfully');
			redirect('admin/employee');
	    } else if (!empty($employeeGetDetails) && $employeeGetDetails != "updated") {
	    	$this->session->set_flashdata('message_success', 'Employee added successfully');
			redirect('admin/employee');
	    } else {
		    if (!empty($employee_id)) {
             	$data["getSingleEmployee"]  = $this->Employee_Model->getSingleEmployee($employee_id);
		    }
		    $data["getActiveArea"]  = $this->Area_Model->getActiveArea();
		    $data["getActiveLine"]  = $this->Line_Model->getActiveLine();
		    $data["getRoleLine"]    = $this->Role_Model->getActiveRole();
			$data['subview'] = $this->load->view('admin/add-employee', $data, TRUE);
			$this->load->view('admin/_layout_main', $data);
	    }
	}
	public function editemployee()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/edit-employee', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function employee_name_exists(){
    	$employee_id   =  $_POST["e_id"];
		$emp_id = $_POST["emp_id"];
		if(empty($employee_id)){
			$this->db->where('emp_id', $emp_id);
			$query = $this->db->get('tbl_employee');
			if ( $query->num_rows() > 0 ) {
				echo 'false';
                exit;
			} else {
				echo 'true';
                exit;
			}
		} else {
			echo 'true';
            exit;
		}
	}
}
