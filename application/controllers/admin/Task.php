<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Task extends MY_Controller {

	public function index()
	{
		$data = array();
		if (isset($_POST["task"])) {
           $data["search"] = $_POST["task"];
        }
		$this->load->model("Task_Model");
		$data['getTaskList']  = $this->Task_Model->taskList();
		$data['subview'] = $this->load->view('admin/task-list', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function addtask()
	{
		$data = array();
		$task_id=$this->uri->segment(4);
		$this->load->model("Equipment_Model");
		$this->load->model("Task_Model");
		$taskGetDetails  = $this->Task_Model->addTask();
		if ($taskGetDetails=="updated") {
			$this->session->set_flashdata('message_success', 'Task updated successfully');
			redirect('admin/task');
	    } else if (!empty($taskGetDetails) && $taskGetDetails != "updated") {
	    	$this->session->set_flashdata('message_success', 'Task added successfully');
			redirect('admin/task');
	    } else {
		    if (!empty($task_id)) {
             	$data["getSingleTask"]  = $this->Task_Model->getSingleTask($task_id);
             	$data["getLogtask"]     = $this->Task_Model->getLogtask($task_id);
		    }
			$data["getActiveEquipment"]  = $this->Equipment_Model->getEquipmentLine();
			$data['subview'] = $this->load->view('admin/add-task', $data, TRUE);
			$this->load->view('admin/_layout_main', $data);
	    }
	}
	public function edittask()
	{
		$data = array();
		$data['subview'] = $this->load->view('admin/edit-task', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
	public function viewtask()
	{
		$data = array();
		$task_id=$this->uri->segment(4);
		if (!empty($task_id)) {
			$this->load->model("Task_Model");
			$data["getLogtask"]     = $this->Task_Model->getViewtask($task_id);
		}
		$data['subview'] = $this->load->view('admin/view-task', $data, TRUE);
		$this->load->view('admin/_layout_main', $data);
	}
}
