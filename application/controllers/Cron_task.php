
<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_task extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $query = $this->db->select('*')
						  ->from('tbl_task')
						  ->get();
        $rows  = $query->result_array();
           
        foreach ($rows as $row) {
             $expired_date = $row['expired_date'];
             $date         = date('Y-m-d H:i:s');
            
            if($expired_date<=$date)
            {
            	$task_id=$row['task_id'];
				$dataupdate = array(
				"task_status" => "Expired",
				"modified_date" => $date,
				);
            $this->db->where('task_id',$task_id);
            $this->db->update('tbl_task', $dataupdate);

            }
            
        }
        
    }
    
}

?>
