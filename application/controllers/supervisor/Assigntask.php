<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Assigntask extends MY_Controller {

	public function index()
	{
		$data = array();
		// if (isset($_POST["asssigntask"])) {
  //          $data["search"] = $_POST["asssigntask"];
  //       }
		$this->load->model("Assigntask_Model");
		$data["getAssignTask"]=$this->Assigntask_Model->assigntaskList();
		$data['subview'] = $this->load->view('supervisor/assigntask-list', $data, TRUE);
		$this->load->view('supervisor/_layout_main', $data);
	}
	public function addassigntask()
	{
		$data = array();
		$data['subview'] = $this->load->view('supervisor/add-assigntask', $data, TRUE);
		$this->load->view('supervisor/_layout_main', $data);
	}
	public function editassigntask()
	{
		$data = array();
		$asstask_id=$this->uri->segment(4);
		$this->load->model("Employee_Model");
		$this->load->model("Area_Model");
		$this->load->model("Line_Model");
		$this->load->model("Equipment_Model");
		$this->load->model("Assigntask_Model");
		$data["getActiveArea"]       = $this->Area_Model->getActiveArea();
		$data["getActiveLine"]       = $this->Line_Model->getActiveLine();
		$data["getActiveEquipment"]  = $this->Equipment_Model->getEquipmentLine();
		$data['openEmployeeList']    = $this->Employee_Model->openEmployeeList();
		if (!empty($asstask_id)) {
			$data["getAssignSingle"]=$this->Assigntask_Model->assignsingleList($asstask_id);
		}
		$updateAssignTask=$this->Assigntask_Model->updateAssignTask();
		if ($updateAssignTask=="updated") {
           $this->session->set_flashdata('message_success', 'Assign task updated successfully');
			redirect('supervisor/assigntask');
		}
		$data['subview'] = $this->load->view('supervisor/edit-assigntask', $data, TRUE);
		$this->load->view('supervisor/_layout_main', $data);
	}
}
