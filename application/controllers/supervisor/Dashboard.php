<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Dashboard extends MY_Controller {

	public function index()
	{
		$data = array();
		$this->load->model("Task_Model");
		$this->load->model("Employee_Model");
		$this->load->model("Area_Model");
		$this->load->model("Line_Model");
		$this->load->model("Equipment_Model");
		$data["getActiveArea"]       = $this->Area_Model->getActiveArea();
		$data["getActiveLine"]       = $this->Line_Model->getActiveLine();
		$data["getActiveEquipment"]  = $this->Equipment_Model->getEquipmentLine();
		$data['openEmployeeList']    = $this->Employee_Model->openEmployeeList();
		$data['openTaskList']  		 = $this->Task_Model->openTaskList();
		$data['progressTaskList']    = $this->Task_Model->progressTaskList();
		$data['completedTaskList']   = $this->Task_Model->completedTaskList(); 
		$data['expiredTaskList']     = $this->Task_Model->expiredTaskList();   
		$data['subview'] = $this->load->view('supervisor/dashboard', $data, TRUE);
		$this->load->view('supervisor/_layout_main', $data);
	}
}
