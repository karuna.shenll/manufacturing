<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Login extends MY_Controller {

	public function index()
	{  
		$this->load->database();
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('Supervisor_Model');
		$login_details=  $this->Supervisor_Model->SupervisorLogin();
		if (($login_details!=NULL) && !empty($login_details)) {
			$supervisorid=$this->session->userdata('supervisor_id');
			if(!empty($supervisorid)) {
				$this->session->set_flashdata('success', 'You have successfully logged in');
				redirect('supervisor/dashboard');
			} else {
				$this->session->set_flashdata('message_failure', '');
				$this->load->view('supervisor/login');
			}
		} else {
			$submit = $this->input->post("submit");
			if (!empty($submit)){
				$this->session->set_flashdata('message_failure', 'Invalid email and password');
				$submit="";
			} else {
				$this->session->set_flashdata('message_failure', '');
			}
			$this->load->view('supervisor/login');
		}
	}
}