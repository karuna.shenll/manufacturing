<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Opentask extends MY_Controller {

	public function index()
	{
		$data = array();
		if (isset($_POST["task"])) {
           $data["search"] = $_POST["task"];
        }
		$this->load->model("Task_Model");
		$data['openTaskList']  = $this->Task_Model->openEmpTaskList();
		$data['subview'] = $this->load->view('employee/opentask-list', $data, TRUE);
		$this->load->view('employee/_layout_main', $data);
	}
	public function addopentask()
	{
		$data = array();
		$data['subview'] = $this->load->view('employee/add-supervisor', $data, TRUE);
		$this->load->view('employee/_layout_main', $data);
	}
	public function editopentask()
	{
		$data = array();
		$data['subview'] = $this->load->view('employee/edit-supervisor', $data, TRUE);
		$this->load->view('employee/_layout_main', $data);
	}
}
