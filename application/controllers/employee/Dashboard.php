<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Dashboard extends MY_Controller {

	public function index()
	{
		$data = array();
		if (isset($_POST["asssigntask"])) {
           $data["search"] = $_POST["asssigntask"];
        }
		$this->load->model("Assigntask_Model");
		$data["getAssignedTask"]=$this->Assigntask_Model->assignedTaskList();
		$data['subview'] = $this->load->view('employee/dashboard', $data, TRUE);
		$this->load->view('employee/_layout_main', $data);
	}
}
