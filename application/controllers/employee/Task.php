<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admistrator
 *
 * @author Shenll
 */
class Task extends MY_Controller {

	public function index()
	{
		$data = array();
		if (isset($_POST["task"])) {
			$data['search'] = $_POST["task"];
		}
		$this->load->model("Task_Model");
		$data['AssignedTaskList']=$this->Task_Model->AssignedTaskList();
		$data['subview'] = $this->load->view('employee/task-list', $data, TRUE);
		$this->load->view('employee/_layout_main', $data);
	}
	public function addtask()
	{
		$data = array();
		$data['subview'] = $this->load->view('employee/add-task', $data, TRUE);
		$this->load->view('employee/_layout_main', $data);
	}
	public function edittask()
	{
		$data = array();
		$taskID= $this->uri->segment(4);
		$this->load->model("Task_Model");
		$data['getViewtask']=$this->Task_Model->getViewtask($taskID);
		$data['subview'] = $this->load->view('employee/edit-task', $data, TRUE);
		$this->load->view('employee/_layout_main', $data);
	}
}
