<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Login extends MY_Controller {

	public function index()
	{  
		$this->load->database();
		$this->load->helper(array('form','url'));
		$this->load->library('session');
		$this->load->model('Employee_Model');
		$login_details=  $this->Employee_Model->EmployeeLogin();
		if (($login_details!=NULL) && !empty($login_details)) {
			$employeeid=$this->session->userdata('e_id');
			if(!empty($employeeid)) {
				$this->session->set_flashdata('success', 'You have successfully logged in');
				redirect('employee/dashboard');
			} else {
				$this->session->set_flashdata('message_failure', '');
				$this->load->view('employee/login');
			}
		} else {
			$submit = $this->input->post("submit");
			if (!empty($submit)){
				$this->session->set_flashdata('message_failure', 'Invalid email and password');
				$submit="";
			} else {
				$this->session->set_flashdata('message_failure', '');
			}
			$this->load->view('employee/login');
		}
	}
}