<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Supervisor_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}

	function SupervisorLogin() {
		$email = $this->input->post("email");
		$password = $this->input->post("password");
		$passwords=$this->Collective->encode($password);
        $role="Supervisor";
			if(!empty($email) && !empty($passwords)) {
				$stmt = $this->db->query("select * from tbl_supervisor where supervisor_email='" . $email . "' and password='" . $passwords. "' and role='".$role."'");
				$rowcount = $stmt->num_rows();
				if ($rowcount == 1) {
					$data="";
					foreach ($stmt->result() as $row) {
						//$full_name=$row->name;
						//$names = explode(' ',$full_name);
						//$first_name = $names[0];
						$data = array(
							'supervisor_id'  =>$row->supervisor_id,
							'supervisor_name' =>$row->supervisor_name,
							'supervisor_email'=>  $row->supervisor_email,
							'user_type'=> $row->role,
						);
						$this->session->set_userdata($data);
						return true;
					}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	function logout($adminlogid) {
		return true;
	}

	function supervisorList() {
        $search_name = $search_emp = $search_email=$search_mobile= $search_user=$search_area=$condition = '';
        if (isset($_POST["supervisor"])) {
            $supervisor     = $_POST["supervisor"];
            $search_emp     = $supervisor["emp_id"];
            $search_name    = $supervisor["name"];
            $search_email   = $supervisor["email"];
            $search_mobile  = $supervisor["mobile"];
            $search_user    = $supervisor["username"];
            $search_area    = $supervisor["area"];
            if (!empty($search_area)) {
                $condition.=" AND supervisor.area_id=".$search_area;
            }
        }
		$query= $this->db->select('*')
                          ->from('tbl_supervisor as supervisor')
                          ->Join('tbl_area as area','area.area_id=supervisor.area_id','JOIN')
                          ->where("supervisor.supervisor_name LIKE '%$search_name%'")
                          ->where("supervisor.emp_id LIKE '%$search_emp%'")
                          ->where("supervisor.supervisor_email LIKE '%$search_email%'")
                          ->where("supervisor.supervisor_mobile LIKE '%$search_mobile%'")
                          ->where("supervisor.user_name LIKE '%$search_user%'".$condition)
                          ->order_by('supervisor.supervisor_id','desc')
                          ->get();
        $Linerows = $query->num_rows();
        if ($Linerows > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
	function addSupervisor() {
        $emp_id     = $this->input->post("emp_id");
        $name       = $this->input->post("name");
        $mobile     = $this->input->post("mobile");
        $email_id   = $this->input->post("email_id");
        $area_id    = $this->input->post("area_id");
        $line_id    = $this->input->post("line_id");
        $username   = $this->input->post("username");
        $password   = $this->input->post("password");
        $pass       = $this->Collective->encode($password);
        $usertype   = $this->input->post("usertype");
        $superID    = $this->input->post("hndid");
        $datetimedb = date("Y-m-d H:i:s");
        if (!empty($superID)) {
            $dataupdate = array(
                "emp_id" => $emp_id,
                "supervisor_name"=> $name,
                "supervisor_email"=> $email_id,
                "supervisor_mobile"=> $mobile,
                "line_id" => implode("##",$line_id),
                "area_id" => $area_id,
                "user_name" => $username,
                "password" =>$pass,
                "role" =>$usertype,
                "modified_date" => $datetimedb,
            );
            $this->db->where('supervisor_id',$superID);
            $this->db->update('tbl_supervisor', $dataupdate);
            return "updated";
        } else if (!empty($emp_id)) {
            $data = array(
                "emp_id" => $emp_id,
                "supervisor_name"=> $name,
                "supervisor_email"=> $email_id,
                "supervisor_mobile"=> $mobile,
                "line_id" => implode("##",$line_id),
                "area_id" => $area_id,
                "user_name" => $username,
                "password" =>$pass,
                "role" =>$usertype,
                "created_date" =>$datetimedb,
                "modified_date" => $datetimedb
            );
            $this->db->insert('tbl_supervisor', $data);
            $supervisorId=$this->db->insert_id();
            return  $supervisorId;
        }
	}
	function getSingleSupervisor($superID) {
		$query= $this->db->select('*')
                          ->from('tbl_supervisor')
                          ->where('supervisor_id',$superID)
                          ->get();
        $supervisorrows = $query->num_rows();
        if ($supervisorrows > 0) {
            foreach ($query->result() as $row) {
                return  $row;
            }
        } else {
            return false;
        }
	}
}