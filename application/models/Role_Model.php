<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Role_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}
	function roleList() {
        $search_name = $search_status = $condition = '';
        if (isset($_POST["role"])) {
            $role          = $_POST["role"];
            $search_name   = $role["role_name"];
            $search_status = $role["status"];
            if (!empty($search_status)) {
                $condition.=" AND status=".$search_status;
            }
        }
		$query= $this->db->select('*')
                          ->from('tbl_role')
                          ->where("role_name LIKE '%$search_name%'".$condition)
                          ->order_by('role_id','desc')
                          ->get();
        $Arearows = $query->num_rows();
        if ($Arearows > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
	function addRole() {
        $role     = $this->input->post("role");
        $status   = $this->input->post("status");
        $roleID     = $this->input->post("hndid");
        $datetimedb  = date("Y-m-d H:i:s");
        if (!empty($roleID)) {
        	$dataupdate = array(
                "role_name" => $role,
                "status" => $status,
                "modified_date" => $datetimedb
            );
            $this->db->where('role_id',$roleID);
            $this->db->update('tbl_role', $dataupdate);
            return "updated";
        } else if (!empty($role)) {
            $data = array(
                "role_name" =>$role,
                "status" => $status,
                "created_date"=>$datetimedb,
                "modified_date" => $datetimedb
            );
            $this->db->insert('tbl_role', $data);
            $roleId=$this->db->insert_id();
            return  $roleId;
        }
	}
	function getSingleRole($roleID) {
		$query= $this->db->select('*')
                          ->from('tbl_role')
                          ->where('role_id',$roleID)
                          ->get();
        $Rolerows = $query->num_rows();
        if ($Rolerows > 0) {
            foreach ($query->result() as $row) {
                return  $row;
            }
        } else {
            return false;
        }
	}
    function getActiveRole() {
        $query= $this->db->select('*')
                          ->from('tbl_role')
                          ->where('status = "1"')
                          ->get();
        $rolerows = $query->num_rows();
        if ($rolerows > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
}