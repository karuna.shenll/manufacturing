<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Group_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}
	function groupList() {
        $search_name = $search_status = $condition = '';
        if (isset($_POST["group"])) {
            $group         = $_POST["group"];
            $search_name   = $group["group_name"];
            $search_status = $group["status"];
            if (!empty($search_status)) {
                $condition.=" AND status=".$search_status;
            }
        }
		$query= $this->db->select('*')
                          ->from('tbl_group')
                          ->where("group_name LIKE '%$search_name%'".$condition)
                          ->order_by('group_id','desc')
                          ->get();
        $Arearows = $query->num_rows();
        if ($Arearows > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
	function addGroup() {
        $group_name  = $this->input->post("group_name");
        $status      = $this->input->post("status");
        $groupID     = $this->input->post("hndid");
        $datetimedb  = date("Y-m-d H:i:s");
        if (!empty($groupID)) {
        	$dataupdate = array(
                "group_name" => $group_name,
                "status" => $status,
                "modified_date" => $datetimedb
            );
            $this->db->where('group_id',$groupID);
            $this->db->update('tbl_group', $dataupdate);
            return "updated";
        } else if (!empty($group_name)) {
            $data = array(
                "group_name" =>$group_name,
                "status" => $status,
                "created_date"=>$datetimedb,
                "modified_date" => $datetimedb
            );
            $this->db->insert('tbl_group', $data);
            $groupId=$this->db->insert_id();
            return  $groupId;
        }
	}
	function getSingleGroup($groupID) {
		$query= $this->db->select('*')
                          ->from('tbl_group')
                          ->where('group_id',$groupID)
                          ->get();
        $grouprows = $query->num_rows();
        if ($grouprows > 0) {
            foreach ($query->result() as $row) {
                return  $row;
            }
        } else {
            return false;
        }
	}
    function getActiveGroup() {
        $query= $this->db->select('*')
                          ->from('tbl_group')
                          ->where('status = "1"')
                          ->get();
        $grouprows = $query->num_rows();
        if ($grouprows > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
}