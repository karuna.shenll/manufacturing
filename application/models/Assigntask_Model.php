<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Assigntask_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}
	function assigntaskList() {
        $search_empid = $search_ename = $search_lname = $search_aname = $search_code = $search_name =$condition = '';

        if (isset($_POST["asssigntask"])) {
          $asssigntask   = $_POST["asssigntask"];
          $search_empid  = $asssigntask["emp_id"];
          $search_ename  = $asssigntask["emp_name"];
          $search_lname  = $asssigntask["line_name"];
          $search_aname  = $asssigntask["area_name"];
          $search_code   = $asssigntask["equ_code"];
          $search_name   = $asssigntask["equ_name"];
        }

    	$query= $this->db->select('assigntask.assign_task_id, task.task_id, equ.equ_id, equ.equ_code, equ.equ_model, equ.equ_name, line.line_name, area.area_name,emp.emp_id,emp.emp_name')
                    ->from('tbl_assign_task as assigntask')
                    ->JOIN('tbl_task as task','task.task_id = assigntask.task_id','JOIN')
                    ->Join('tbl_equipment as equ','equ.equ_id = assigntask.equ_id','JOIN')
                    ->Join('tbl_line as line','line.line_id  = assigntask.line_id','JOIN')
                    ->Join('tbl_area as area','area.area_id = assigntask.area_id','JOIN')
                    ->Join('tbl_employee as emp','emp.e_id = assigntask.emp_id','JOIN')
                    ->where("emp.emp_id LIKE '%$search_empid%'")
                    ->where("emp.emp_name LIKE '%$search_ename%'")
                    ->where("line.line_name LIKE '%$search_lname%'")
                    ->where("area.area_name LIKE '%$search_aname%'")
                    ->where("equ.equ_code LIKE '%$search_code%'")
                    ->where("equ.equ_name LIKE '%$search_name%'")
                    ->where("task.task_status='Assigned'")
                    ->order_by('assigntask.assign_task_id','desc')
                    ->get();
        $assignTaskrows = $query->num_rows();

        if ($assignTaskrows > 0) {
          return $query->result();
        } else {
          return false;
        }
    }

    function assignsingleList($asstask_id){
        $query= $this->db->select('assigntask.*,emp.emp_id as empcode,emp.e_id as eid')
                    ->from('tbl_assign_task as assigntask')
                    ->Join('tbl_employee as emp','emp.e_id = assigntask.emp_id','JOIN')
                    ->where("assigntask.assign_task_id=",$asstask_id)
                    ->get();
        $assignTaskrows = $query->num_rows();
        if ($assignTaskrows > 0) {
            foreach ($query->result() as $row) {
                return $row;
            }
        } else {
            return false;
        }
    }
    function updateAssignTask(){
        $asstaskId  = $this->input->post("hndid");
        $eid        = $this->input->post("hndeid");
        $taskid     = $this->input->post("hndtaskid");
        $emp_name   = $this->input->post("emp_name");
        $line_id    = $this->input->post("line_id");
        $area_id    = $this->input->post("area");
        $equ_code   = $this->input->post("equ_code");
        $equ_id     = $this->input->post("equipment_name");
        $datetimedb = date("Y-m-d H:i:s");
        if (!empty($asstaskId)) {
            $dataupdate = array(
                "task_id"  => $taskid,
                "emp_id"   => $eid,
                "emp_name" => $emp_name,
                "line_id"  => $line_id,
                "area_id"  => $area_id,
                "equ_code" => $equ_code,
                "equ_id"   => $equ_id,
                "modified_date" => $datetimedb,
            );
            $this->db->where('assign_task_id',$asstaskId);
            $this->db->update('tbl_assign_task', $dataupdate);
            return "updated";
        } else {
            return false;
        }
    }
    function assignedTaskList() {
        $employeeid=$this->session->userdata('e_id');
        $current_date=date("Y-m-d");
        $query= $this->db->select('assigntask.assign_task_id, task.task_id, equ.equ_id, equ.equ_code, equ.equ_model, equ.equ_name, line.line_name, area.area_name,emp.emp_id,emp.emp_name,task.task_status')
                    ->from('tbl_assign_task as assigntask')
                    ->JOIN('tbl_task as task','task.task_id = assigntask.task_id','JOIN')
                    ->Join('tbl_equipment as equ','equ.equ_id = assigntask.equ_id','JOIN')
                    ->Join('tbl_line as line','line.line_id  = assigntask.line_id','JOIN')
                    ->Join('tbl_area as area','area.area_id = assigntask.area_id','JOIN')
                    ->Join('tbl_employee as emp','emp.e_id = assigntask.emp_id','JOIN')
                    ->where("task.task_status='Assigned'")
                    ->where("assigntask.emp_id =".$employeeid." AND DATE_FORMAT(task.modified_date,'%Y-%m-%d') ='".$current_date."'")
                    ->order_by('assigntask.assign_task_id','desc')
                    ->get();
        $assignTaskrows = $query->num_rows();
        if ($assignTaskrows > 0) {
          return $query->result();
        } else {
          return false;
        }
    }
}