<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Customer_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}

	function customerList() {
        $search_name = $search_email=$search_mobile= $search_line=$search_products=$condition='';
        if (isset($_POST["customer"])) {
            $customer       = $_POST["customer"];
            $search_name    = $customer["customer_name"];
            $search_email   = $customer["email"];
            $search_mobile  = $customer["mobile"];
            $search_line    = $customer["line_id"];
            $search_products= $customer["products"];
            if($search_line){
                $condition.=" AND customer.line_id=".$search_line;
            }
        }
		$query= $this->db->select('*')
                          ->from('tbl_customer as customer')
                          ->Join('tbl_line as line','line.line_id=customer.line_id','JOIN')
                          ->where("customer.customer_name LIKE '%$search_name%'")
                          ->where("customer.email LIKE '%$search_email%'")
                          ->where("customer.products LIKE '%$search_products%'")
                          ->where("customer.mobile LIKE '%$search_mobile%'".$condition)
                          ->order_by('customer.cus_id','desc')
                          ->get();
        $Linerows = $query->num_rows();
        if ($Linerows > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
	function addcustomer() {
        $customer_name = $this->input->post("customer_name");
        $mobile        = $this->input->post("mobile");
        $email         = $this->input->post("email");
        $address       = $this->input->post("address");
        $products      = $this->input->post("products");
        $line_id       = $this->input->post("line_id");
        $cusID         = $this->input->post("hndid");
        $datetimedb    = date("Y-m-d H:i:s");
        if (!empty($cusID)) {
            $dataupdate = array(
                "customer_name" =>$customer_name,
                "mobile"=>$mobile,
                "email"=>$email,
                "address"=>$address,
                "products" =>$products,
                "line_id" =>$line_id,
                "modified_date" =>$datetimedb,
            );
            $this->db->where('cus_id',$cusID);
            $this->db->update('tbl_customer', $dataupdate);
            return "updated";
        } else if (!empty($customer_name)) {
            $data = array(
                "customer_name" =>$customer_name,
                "mobile"=>$mobile,
                "email"=>$email,
                "address"=>$address,
                "products" =>$products,
                "line_id" =>$line_id,
                "created_date" =>$datetimedb,
                "modified_date" => $datetimedb
            );
            $this->db->insert('tbl_customer', $data);
            $customerId=$this->db->insert_id();
            return  $customerId;
        }
	}
	function getSingleCustomer($cusID) {
		$query= $this->db->select('*')
                          ->from('tbl_customer')
                          ->where('cus_id',$cusID)
                          ->get();
        $customerrows = $query->num_rows();
        if ($customerrows > 0) {
            foreach ($query->result() as $row) {
                return  $row;
            }
        } else {
            return false;
        }
	}
}