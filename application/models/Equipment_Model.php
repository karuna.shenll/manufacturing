<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Equipment_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}
	function equipmentList() {
        $search_name = $search_code = $search_model = $search_group = $condition = '';
        if (isset($_POST["equipment"])) {
            $equipment        = $_POST["equipment"];
            $search_code      = $equipment['equ_code'];
            $search_model     = $equipment['equ_model'];
            $search_name      = $equipment['equ_name'];
            $search_group     = $equipment['group'];
            if (!empty($search_group)) {
                $condition.= " AND group.group_id=".$search_group;
            }
        }
		$query= $this->db->select('*')
                          ->from('tbl_equipment as equ ')
                          ->join('tbl_group as group','group.group_id = equ.group_id', 'JOIN')
                          ->where("equ.equ_code LIKE '%$search_code%'")
                          ->where("equ.equ_model LIKE '%$search_model%'")
                          ->where("equ.equ_name LIKE '%$search_name%'".$condition)
                          ->order_by('equ.equ_id','desc')
                          ->get();
        $Arearows = $query->num_rows();
        if ($Arearows > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
	function addEquipment() {
        $equ_code    = $this->input->post("equipment_code");
        $equ_model   = $this->input->post("equipment_model");
        $equ_name    = $this->input->post("equipment_name");
        $group_id    = $this->input->post("group");
        $equID       = $this->input->post("hndid");
        $datetimedb  = date("Y-m-d H:i:s");
        if (!empty($equID)) {
        	$dataupdate = array(
                "equ_code"  => $equ_code,
                "equ_model" => $equ_model,
                "equ_name"  => $equ_name,
                "group_id"  => $group_id,
                "modified_date" => $datetimedb
            );
            $this->db->where('equ_id',$equID);
            $this->db->update('tbl_equipment', $dataupdate);
            return "updated";
        } else if (!empty($equ_code)) {
            $data = array(
                "equ_code"  => $equ_code,
                "equ_model" => $equ_model,
                "equ_name"  => $equ_name,
                "group_id"  => $group_id,
                "created_date" => $datetimedb,
                "modified_date" => $datetimedb
            );
            $this->db->insert('tbl_equipment', $data);
            $equId=$this->db->insert_id();
            return  $equId;
        }
	}
	function getSingleEquipment($equID) {
		$query= $this->db->select('*')
                          ->from('tbl_equipment')
                          ->where('equ_id',$equID)
                          ->get();
        $equrows = $query->num_rows();
        if ($equrows > 0) {
            foreach ($query->result() as $row) {
                return  $row;
            }
        } else {
            return false;
        }
	}
    function getEquipmentLine() {
        $query= $this->db->select('*')
                          ->from('tbl_equipment')
                          ->get();
        $equrows = $query->num_rows();
        if ($equrows > 0) {
                return  $query->result();
        } else {
            return false;
        }
    }
}