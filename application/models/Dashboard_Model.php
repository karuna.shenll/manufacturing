<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Dashboard_Model extends MY_Model {

	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}

	function equipment_list() {
		$query = $this->db->query("select * from tbl_equipment where equ_id <> ''");
		$rows = $query->num_rows();
      	$get_equp = $query->result();
     
        $get_equp = json_decode(json_encode($get_equp), True);
        foreach ($get_equp as $key => $value) {
        	$query1 = $this->db->query("select * from tbl_task_log where equ_id =".$value['equ_id']);
			$rows1 = $query1->num_rows();
			$rows22=$query1->row_array();

			$query2 = $this->db->query("select * from tbl_task where task_id =".$value['equ_id']);
			$rows2 = $query2->num_rows();
	      
			$result1 = $query1->result();
			$result1 = json_decode(json_encode($result1), True);
			$get_equp[$key]['subtask'] = $result1;  

			$result2 = $query2->result();
			$result2 = json_decode(json_encode($result2), True);
			$get_equp[$key]['task'] = $result2;  
        }
        return $get_equp;
	}

	function equipment_task($equ_id) {
		$query = $this->db->query("select * from tbl_task_log where equ_id ='".$equ_id."' order by created_date asc");
      	$equp_task = $query->result_array();

      	$odd_data = $even_data = $evenArr = $oddArr = array();
      	foreach ($equp_task as $key => $value) {
	 		$task_log_id=$value['task_log_id'];

	 		$equ_id=$value['equ_id'];

	 		if(($key % 2) ==  0){
				$even_data['task_name'] = $value['task_name'];
				$even_data['log_status'] = $value['log_status'];
				$even_data['task_log_id'] = $value['task_log_id'];
				$evenArr[] = $even_data;
			} else {
				$odd_data['task_name'] = $value['task_name'];
				$odd_data['log_status'] = $value['log_status'];
				$odd_data['task_log_id'] = $value['task_log_id'];
				$oddArr[] = $odd_data;	
			}
      	}
      

      	$querys = $this->db->query("select * from tbl_equipment where equ_id ='".$equ_id."'");
      	$equp_name = $querys->row_array();

      	$queryss = $this->db->query("select task_status from tbl_task where equ_id ='".$equ_id."'");
      	$task_status = $queryss->row_array();

      	$data['odd_names'] = $evenArr;
      	$data['even_names'] = $oddArr;
      	$data['equp_name'] = $equp_name['equ_name'];
      	$data['equ_id'] = $equp_name['equ_id'];
      	$data['task_status'] = $task_status['task_status'];
      	// print_r($data);

        return $data;
	}

	function task_report($task_log_id) {

		$query = $this->db->query("select * from tbl_task_log where task_log_id ='".$task_log_id."'");
		$rows = $query->result_array();

		foreach ($rows as $key => $value) {
			# code...
			$task_log_id=$value['task_log_id'];
			$equ_id=$value['equ_id'];
			
			$query1 = $this->db->query("select * from tbl_equipment where equ_id ='".$equ_id."'");
			$equ = $query1->row_array();

			$query2  = $this->db->query("select * from tbl_maintenance where equ_id ='".$equ_id."'");
			$main = $query2->row_array();

			$data['task_name'] = $value['task_name'];
			$data['observation'] = $value['observation'];
			$data['action_taken'] = $value['action_taken'];
			$data['comments'] = $value['comments'];
			$data['log_status'] = $value['log_status'];
			$data['done_by'] = $value['done_by'];
			$data['verified_by'] = $value['verified_by'];
			$data['revision_level'] = $value['revision_level'];
			$data['revision_date'] = date("d/m/Y", strtotime($value['revision_date']));
			$data['created_date']  = date("d/m/Y", strtotime($value['created_date']));
			 //echo 

			$data['equ_name'] = $equ['equ_name'];
			$data['equ_id'] = $equ['equ_id'];
			$data['frequency'] = $main['frequency'];

			$month = $equ['created_date'];
			$d = date_parse_from_format("Y-m-d", $month);
			$month_num = $d["month"];
			$year = $d["year"];
			$monthName = date('F', mktime(0, 0, 0, $month_num, 10));
			$data['month_name'] = $monthName; 
			$data['year'] = $year;
		
        }
        
        return $data;
	}


	function viewreport($equ_id){

		if (!empty($_POST['report'])) {
			$report = $_POST['report'];
			$search_name = trim($report['select_report']);
		} else {
			$search_name='Cluster1';
		}

		if($search_name=='Cluster1'){
			$mon=0;
			$months=6;
			$cluster=1;
		}

		if($search_name=='Cluster2'){
			$mon=6;
			$months=12;
			$cluster=2;
		}

		$resultArr = array();

		for($i=$mon; $i<$months; $i++){
			$first_date = date("Y-m-d", mktime(0, 0, 0, date("01")+$i, 1)); 
			$last_date = date("Y-m-d", mktime(0, 0, 0, date("01")+$i+1, 0));

			$month_number=$i+1;
			$month_name = date("M", mktime(0, 0, 0, $month_number, 10));
			$monthname['month_name']=$month_name;
			$month_data[]=$monthname;


			$query= $this->db->select('*')
				->from('tbl_equipment')
				->where('equ_id',$equ_id)
				->get();
			$equ_row = $query->row_array();
			$data['equ_name']=$equ_row["equ_name"];

			$query1= $this->db->select('*')
				->from('tbl_line as tl')
				->join('tbl_line_log as tlg','tl.line_id = tlg.line_id', 'JOIN')
				->where('tlg.equ_id',$equ_id)
				->get();
			$line_row = $query1->row_array();
			$data['line_name']=$line_row["line_name"];


			$res = $mainten = array();
			$query3= $this->db->select('count(*) as rec_count,frequency')
			->from('tbl_maintenance')
			->where("created_date BETWEEN '".$first_date."' and '".$last_date."'")
			->where('equ_id',$equ_id)
			->group_by('frequency')
			->get();
			$mainten = $query3->result_array();

			$mainten = json_decode(json_encode($mainten), true);
			foreach ($mainten as $key => $value) {
				foreach ($value as $key1 => $value1) {
					$res[$value['frequency']] = $value['rec_count'];
				}
			}
			$resultArr[$month_name] = $res;
		}

		$data['cluster_monthly_frequency']=$resultArr;
		$data['month_name']=$month_data;
		$data['equ_id']=$equ_id;
		$data['cluster']=$cluster;

		return $data;
	}


}