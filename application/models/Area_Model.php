<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Area_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}
	function areaList() {
        $search_name = $search_status = $condition = '';
        if (isset($_POST["area"])) {
            $area          = $_POST["area"];
            $search_name   = $area["area_name"];
            $search_status = $area["status"];
            if (!empty($search_status)) {
                $condition.=" AND status=".$search_status;
            }
        }
		$query= $this->db->select('*')
                          ->from('tbl_area')
                          ->where("area_name LIKE '%$search_name%'".$condition)
                          ->order_by('area_id','desc')
                          ->get();
        $Arearows = $query->num_rows();
        if ($Arearows > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
	function addArea() {
        $area     = $this->input->post("area");
        $status   = $this->input->post("status");
        $areaID     = $this->input->post("hndid");
        $datetimedb  = date("Y-m-d H:i:s");
        if (!empty($areaID)) {
        	$dataupdate = array(
                "area_name" => $area,
                "status" => $status,
                "modified_date" => $datetimedb
            );
            $this->db->where('area_id',$areaID);
            $this->db->update('tbl_area', $dataupdate);
            return "updated";
        } else if (!empty($area)) {
            $data = array(
                "area_name" =>$area,
                "status" => $status,
                "created_date"=>$datetimedb,
                "modified_date" => $datetimedb
            );
            $this->db->insert('tbl_area', $data);
            $areaId=$this->db->insert_id();
            return  $areaId;
        }
	}
	function getSingleArea($areaID) {
		$query= $this->db->select('*')
                          ->from('tbl_area')
                          ->where('area_id',$areaID)
                          ->get();
        $Auctionrows = $query->num_rows();
        if ($Auctionrows > 0) {
            foreach ($query->result() as $row) {
                return  $row;
            }
        } else {
            return false;
        }
	}
    function getActiveArea() {
        $query= $this->db->select('*')
                          ->from('tbl_area')
                          ->where('status = "1"')
                          ->get();
        $grouprows = $query->num_rows();
        if ($grouprows > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
}