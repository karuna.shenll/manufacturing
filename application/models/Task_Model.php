<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Task_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}
	function taskList() {
        $search_name = $search_code = $search_model=$condition = '';
        if (isset($_POST["task"])) {
            $task         = $_POST["task"];
            $search_code  = $task["equ_code"];
            $search_model = $task["equ_model"];
            $search_name  = $task["equ_name"];
        }
		$query= $this->db->select('*')
                  ->from('tbl_task as task')
                  ->Join('tbl_equipment as equ','equ.equ_id=task.equ_id','JOIN')
                  ->where("equ.equ_code LIKE '%$search_code%'")
                  ->where("equ.equ_model LIKE '%$search_model%'")
                  ->where("equ.equ_name LIKE '%$search_name%'")
                  ->order_by('task.task_id','desc')
                  ->get();
        $Taskrows = $query->num_rows();
        if ($Taskrows > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
	function addTask() {
        $equ_code  = $this->input->post("equipment_code");
        $equ_model = $this->input->post("equipment_model");
        $equ_id    = $this->input->post("equipment_name");
        $Arrtask   = $this->input->post("add_task");
        $logtaskid = $this->input->post("hidden_log");
        $taskID    = $this->input->post("hndid");
        $datetimedb = date("Y-m-d H:i:s");
        if (!empty($taskID)) {
            $dataupdate = array(
                "equ_code"  => $equ_code,
                "equ_model" => $equ_model,
                "equ_id"  => $equ_id,
                "modified_date" => $datetimedb
            );
            $this->db->where('task_id',$taskID);
            $this->db->update('tbl_task', $dataupdate);
            
            if (count($Arrtask)>0) {
                $querys = $this->db->select('task_log_id')
                    ->from('tbl_task_log')
                    ->where('task_id',$taskID)
                    ->get();
                $keep_id = array();
                if ($querys->num_rows() > 0) {
                    foreach ($querys->result() as $row) {
                        $keep_id[] = $row->task_log_id;
                    }
                }
                $inc=0;
                foreach ($logtaskid as $taskLogData) {
                    if (in_array($taskLogData,$keep_id)) {
                        $task=$Arrtask[$inc];
                        if (!empty($task)) {
                            $dataUpdateLog = array (
                                "task_id" => $taskID,
                                "task_name" =>$task,
                                "equ_id" =>$equ_id,
                                "modified_date" => $datetimedb
                            );
                            $this->db->where('task_log_id', $taskLogData);
                            $this->db->update('tbl_task_log', $dataUpdateLog);
                        }
                    } else {
                        $task=$Arrtask[$inc];
                        if (!empty($task)) {
                            $dataLog = array (
                                "task_id" => $taskID,
                                "task_name" =>$task,
                                "equ_id" =>$equ_id,
                                "created_date" =>$datetimedb,
                                "modified_date" => $datetimedb
                            );
                            $this->db->insert('tbl_task_log', $dataLog);
                            $task_logId=$this->db->insert_id();
                        }
                    }
                $inc++;
                }
            }
            return "updated";
        } else if (!empty($equ_code)) {
            $query= $this->db->select('*')
                          ->from('tbl_maintenance')
                          ->where('equ_id',$equ_id)
                          ->get();
            $maintenancerows = $query->num_rows();
            if ($maintenancerows > 0) {
                $maintenData = $query->result();
                $frequency = $maintenData[0]->frequency;

                if (strtolower($frequency) == "weekly") {
                    $ts = strtotime($datetimedb);
                    $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
                    $start_date = date('Y-m-d', $start);
                    $end_date = date('Y-m-d', strtotime('next saturday', $start));
                    $expired_date = $end_date." "."23:59:59";
                }
                if (strtolower($frequency) == "monthly") {
                    $end_date =  date("Y-m-t", strtotime($datetimedb));
                    $expired_date = $end_date." "."23:59:59";
                }
                if (strtolower($frequency) == "quateraly") {
                    $current_quarter = ceil(date('n') / 3);
                    $first_date = date('Y-m-d', strtotime(date('Y') . '-' . (($current_quarter * 3) - 2) . '-1'));
                    $end_date = date('Y-m-t', strtotime(date('Y') . '-' . (($current_quarter * 3)) . '-1'));
                    $expired_date = $end_date." "."23:59:59";
                }
                if (strtolower($frequency) == "half-yearly") {
                    $currentmounth = date("m-Y",strtotime($datetimedb));
                    if ($currentmounth <= "06-".date("Y")) {
                       $yearEnd = date('Y-m-d', strtotime('last day of june'));
                       $expired_date = $yearEnd." "."23:59:59";
                    }
                    if ($currentmounth >= "07-".date("Y")) {
                        $yearEnd = date('Y-m-d', strtotime('last day of december'));
                        $expired_date = $yearEnd." "."23:59:59";
                    }
                }
                if (strtolower($frequency) == "yearly") {
                    $yearEnd = date('Y-m-d', strtotime('last day of december'));
                    $expired_date = $yearEnd." "."23:59:59";
                }

                $data = array(
                    "equ_code"  => $equ_code,
                    "equ_model" => $equ_model,
                    "equ_id"  => $equ_id,
                    "expired_date"  => $expired_date,
                    "task_status"=>"Open",
                    "created_date" =>$datetimedb,
                    "modified_date" => $datetimedb
                );
                $this->db->insert('tbl_task', $data);
                $taskId=$this->db->insert_id();
                if (!empty($taskId)) { 
                    if (count($Arrtask)>0) {
                        foreach ($Arrtask as $key => $task) {
                            if (!empty($task)) {
                                $dataLog = array (
                                    "task_id" => $taskId,
                                    "task_name" =>$task,
                                    "equ_id" =>$equ_id,
                                    "created_date" =>$datetimedb,
                                    "modified_date" => $datetimedb
                                );
                                $this->db->insert('tbl_task_log', $dataLog);
                                $task_logId=$this->db->insert_id();
                            }
                        }
                    }
                    return  $task_logId;
                }
            } else {
                return false;
            }
        }
	}
	function getSingleTask($taskID) {
		$query= $this->db->select('*')
                          ->from('tbl_task')
                          ->where('task_id',$taskID)
                          ->get();
        $Linerows = $query->num_rows();
        if ($Linerows > 0) {
            foreach ($query->result() as $row) {
                return  $row;
            }
        } else {
            return false;
        }
	}
    function getLogtask($taskID){
        $query= $this->db->select('*')
                          ->from('tbl_task_log')
                          ->where('task_id',$taskID)
                          ->order_by('task_log_id','ASC')
                          ->get();
        $equrows = $query->num_rows();
        if ($equrows > 0) {
                return  $query->result();
        } else {
            return false;
        }
    }
    function getViewtask($taskID) {
        $query= $this->db->select('task.task_id, task.equ_id, equ.equ_code, equ.equ_model, equ.equ_name, mainten.frequency, mainten.created_date, mainten.line_id, line.doc_no, line.revision_add,line.modified_date')
                  ->from('tbl_task as task')
                  ->Join('tbl_equipment as equ','equ.equ_id=task.equ_id','JOIN')
                  ->Join('tbl_maintenance as mainten','mainten.equ_id=task.equ_id','JOIN')
                  ->Join('tbl_line as line','line.line_id=mainten.line_id','JOIN')
                  ->where("task.task_id=".$taskID)
                  ->order_by('task.task_id','asc')
                  ->get();
        $Taskrows = $query->num_rows();
        if ($Taskrows > 0) {
            foreach ($query->result() as $rows) {
                $taskID = $rows->task_id;
                $equ_id = $rows->equ_id;
                $taskDetails["taskdetails"] = array(
                    "task_id"   => $rows->task_id,
                    "equ_code"  => $rows->equ_code,
                    "equ_model" => $rows->equ_model,
                    "equ_name"  => $rows->equ_name,
                    "frequency" => $rows->frequency,
                    "created_date"=> $rows->created_date,
                    "doc_no" => $rows->doc_no,
                    "revision_add"=> $rows->revision_add,
                    "revision_date"=> $rows->modified_date
                );
            }
            $querys = $this->db->select('*')
                          ->from('tbl_task_log')
                          ->where('task_id',$taskID)
                          ->order_by('task_log_id','ASC')
                          ->get();
            $tasklogrows = $querys->num_rows();
            if ($tasklogrows > 0) {
                foreach ($querys->result() as $logs) { 
                    $taskDetails["logtaskdetails"][] = array(
                        "log_task_id" => $logs->task_log_id,
                        "task_id"     => $logs->task_id,
                        "task_name"   => $logs->task_name,
                        "log_status"   => $logs->log_status,
                        "observation" => $logs->observation,
                        "action_taken"=> $logs->action_taken,
                        "comments"    => $logs->comments,
                        "done_by"     => $logs->done_by,
                        "verified_by" => $logs->verified_by,
                        "completed_date"=> $logs->completed_date
                    );
                }
            } 
            $Qry = $this->db->select('*')
                          ->from('tbl_assign_task as assign')
                          ->join('tbl_supervisor as super','ON super.supervisor_id=assign.assign_by','JOIN')
                          ->where('assign.task_id',$taskID)
                          ->where('assign.equ_id',$equ_id)
                          ->order_by('assign.assign_task_id','ASC')
                          ->get();
            $asstasklogrows = $Qry->num_rows();
            if ($asstasklogrows > 0) {
                foreach ($Qry->result() as $assign) { 
                    $taskDetails["assingtask"]= array(
                        "supervisor_name" => $assign->supervisor_name,
                    );
                }
            } 
            return $taskDetails;
        } else {
            return false;
        }
    }
    function openTaskList(){
        $query= $this->db->select('*')
                  ->from('tbl_task as task')
                  ->Join('tbl_equipment as equ','equ.equ_id=task.equ_id','JOIN')
                  ->where("task.task_status='Open'")
                  ->get();
        $Taskrows = $query->num_rows();
        if ($Taskrows > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function progressTaskList() {
        $query= $this->db->select('*')
                  ->from('tbl_task as task')
                  ->Join('tbl_equipment as equ','equ.equ_id=task.equ_id','JOIN')
                  ->where("task.task_status='Inprogress'")
                  ->get();
        $Taskrows = $query->num_rows();
        if ($Taskrows > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function completedTaskList() {
        $query= $this->db->select('*')
                  ->from('tbl_task as task')
                  ->Join('tbl_equipment as equ','equ.equ_id=task.equ_id','JOIN')
                  ->where("task.task_status='Completed'")
                  ->get();
        $Taskrows = $query->num_rows();
        if ($Taskrows > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function expiredTaskList() {
        $query= $this->db->select('*')
                  ->from('tbl_task as task')
                  ->Join('tbl_equipment as equ','equ.equ_id=task.equ_id','JOIN')
                  ->where("task.task_status='Expired'")
                  ->get();
        $Taskrows = $query->num_rows();
        if ($Taskrows > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function openEmpTaskList(){
        $search_name = $search_code = $search_model=$condition = '';
        if (isset($_POST["task"])) {
            $task         = $_POST["task"];
            $search_code  = $task["equcode"];
            $search_model = $task["equmodel"];
            $search_name  = $task["equname"];
        }
        $query= $this->db->select('*')
                  ->from('tbl_task as task')
                  ->Join('tbl_equipment as equ','equ.equ_id=task.equ_id','JOIN')
                  ->where("equ.equ_code LIKE '%$search_code%'")
                  ->where("equ.equ_model LIKE '%$search_model%'")
                  ->where("equ.equ_name LIKE '%$search_name%'")
                  ->where("task.task_status='Open'")
                  ->order_by('task.task_id','desc')
                  ->get();
        $Taskrows = $query->num_rows();
        if ($Taskrows > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function AssignedTaskList(){
        $search_name = $search_code = $search_model=$condition = '';
        if (isset($_POST["task"])) {
            $task         = $_POST["task"];
            $search_code  = $task["equcode"];
            $search_model = $task["equmodel"];
            $search_name  = $task["equname"];
        }
        $employeeid=$this->session->userdata('e_id');
        $query= $this->db->select('assigntask.assign_task_id, task.task_id, equ.equ_id, equ.equ_code, equ.equ_model, equ.equ_name, line.line_name, area.area_name,emp.emp_id,emp.emp_name,task.task_status')
                    ->from('tbl_assign_task as assigntask')
                    ->JOIN('tbl_task as task','task.task_id = assigntask.task_id','JOIN')
                    ->Join('tbl_equipment as equ','equ.equ_id = assigntask.equ_id','JOIN')
                    ->Join('tbl_line as line','line.line_id  = assigntask.line_id','JOIN')
                    ->Join('tbl_area as area','area.area_id = assigntask.area_id','JOIN')
                    ->Join('tbl_employee as emp','emp.e_id = assigntask.emp_id','JOIN')
                    //->where("task.task_status ='Assigned' OR task.task_status='Inprogress' OR task.task_status='Completed'")
                    ->where("equ.equ_code LIKE '%$search_code%'")
                    ->where("equ.equ_model LIKE '%$search_model%'")
                    ->where("equ.equ_name LIKE '%$search_name%'")
                    ->where("assigntask.emp_id =".$employeeid)
                    ->order_by('assigntask.assign_task_id','desc')
                    ->get();
        $assignTaskrows = $query->num_rows();
        if ($assignTaskrows > 0) {
          return $query->result();
        } else {
          return false;
        }
    }
}