<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Common_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}

	function Delete($deleteid,$field,$table) {
		if (!empty($deleteid)) {
			$this ->db-> where($field, $deleteid);
	        $this ->db-> delete($table);
	        return true;
	    } else {
	    	return false;
	    }
	}
	function DeleteLog($deleteid,$field,$table) {
		if (!empty($deleteid)) {
			$this ->db-> where($field, $deleteid);
	        $this ->db-> delete($table);
	        return true;
	    } else {
	    	return false;
	    }
	}
	function cronJobs() {
		
	}
}