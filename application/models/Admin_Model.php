<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Admin_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}

	function AdminLogin() {
		$email = $this->input->post("email");
		$password = $this->input->post("password");
		$passwords=$this->Collective->encode($password);
        $role ="Superadmin";
			if(!empty($email) && !empty($passwords)) {
				$stmt = $this->db->query("select * from tbl_admin where email='" . $email . "' and password='" . $passwords. "' and role='".$role."'");
				$rowcount = $stmt->num_rows();
				if ($rowcount == 1) {
					$data="";
					foreach ($stmt->result() as $row) {
						$full_name=$row->name;
						$names = explode(' ',$full_name);
						$first_name = $names[0];
						$data = array(
							'admin_id'  =>$row->admin_id,
							'admin_name' => $first_name,
							'admin_email'=>  $row->email,
							'user_type'=> "Superadmin",
						);
						$this->session->set_userdata($data);
						return true;
					}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	function logout($adminlogid) {
		return true;
	}
}