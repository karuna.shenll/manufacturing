<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Report_Model extends MY_Model {

	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}

	function reportList() {

    $search_area = $search_line=$search_name=$search_doc=$search_from=$search_to = $searchenddate=$searchenddate=$condition='';
    if (isset($_POST["report"])) {
        $report         = $_POST["report"];
        $search_area    = $report['area_name'];
        $search_line    = $report['line_name']; 
        $search_name    = $report['equname']; 
        $search_doc     = $report['doc_no'];
        $search_from    = $report['from_date'];
        $search_to      = $report['to_date']; 
        if (!empty($search_from) && empty($search_to)) {
            $start_date       = str_replace("/", "-",$search_from);
            $searchstartdate = date("Y-m-d",strtotime($start_date));
            $condition.=" AND DATE_FARMAT('%Y-%m-%d',task.created_date) >='".$searchstartdate."'";
        }
        if (empty($search_from) && !empty($search_to)) {
            $end_date       = str_replace("/", "-",$search_to);
            $searchenddate = date("Y-m-d",strtotime($end_date));
            $condition.=" AND DATE_FARMAT('%Y-%m-%d',task.created_date) <= '".$searchenddate."'";
        }
        if (!empty($search_from) && !empty($search_to)) {
            $start_date       = str_replace("/", "-",$search_from);
            $end_date       = str_replace("/", "-",$search_to);
            $searchstartdate = date("Y-m-d",strtotime($start_date));
            $searchenddate  = date("Y-m-d",strtotime($end_date));
            $condition.=" AND DATE_FARMAT('%Y-%m-%d',task.created_date) >= '".$searchstartdate. "' and DATE_FARMAT('%Y-%m-%d',task.created_date) <= '".$searchenddate. "'";
        }
    }

    $query= $this->db->select('task.task_id, task.equ_id, equ.equ_code, equ.equ_model, equ.equ_name, mainten.frequency, mainten.created_date, mainten.line_id, line.line_name,line.doc_no, line.revision_add,line.modified_date,area.area_name')
    ->from('tbl_task as task')
    ->Join('tbl_equipment as equ','equ.equ_id=task.equ_id','JOIN')
    ->Join('tbl_maintenance as mainten','mainten.equ_id=task.equ_id','JOIN')
    ->Join('tbl_line as line','line.line_id=mainten.line_id','JOIN')
    ->Join('tbl_area as area','area.area_id=line.area_id','JOIN')
    ->where("equ.equ_name LIKE '%$search_name%'")
    ->where("line.line_name LIKE '%$search_line%'")
    ->where("area.area_name LIKE '%$search_area%'")
    ->where("line.doc_no LIKE '%$search_doc%'".$condition)
    ->where("task.task_status='completed' OR task_status='Expired'")
    ->order_by('task.task_id','asc')
    ->get();
    $Taskrows = $query->num_rows();
    if ($Taskrows > 0) {
      return $query->result();
    } else {
      return false;
    }
	}

  function viewreports($line_id) {
    $data=array();

    if (!empty($_POST['report'])) {
      $report = $_POST['report'];
      $search_name = trim($report['select_report']);
    } else {
      $search_name='Cluster1';
    }

    if($search_name=='Cluster1'){
      $mon=0;
      $months=6;
      $cluster=1;
    }

    if($search_name=='Cluster2'){
      $mon=6;
      $months=12;
      $cluster=2;
    }

    $resultArr = $res = $emptyArr = $mainten = array();
    $frequencyArr=array('Weekly','Monthly','Quaterly','Half-Yearly','Yearly');
    for($i=$mon; $i<$months; $i++){
      $first_date = date("Y-m-d", mktime(0, 0, 0, date("01")+$i, 1)); 
      $last_date = date("Y-m-d", mktime(0, 0, 0, date("01")+$i+1, 0));

      $month_number=$i+1;
      $month_name = date("M", mktime(0, 0, 0, $month_number, 10));
      $month_data[]=$month_name;
     
      $query3= $this->db->select('count(*) as rec_count,frequency,equ_id')
      ->from('tbl_maintenance')
      //->where('line_id',$line_id)
      ->where("created_date BETWEEN '".$first_date."' and '".$last_date."'")
      ->group_by('frequency')
      ->get();
      $mainten = $query3->result_array();

      $mainten = json_decode(json_encode($mainten), true);
      foreach ($mainten as $key => $value) {
        foreach ($frequencyArr as $key1 => $value1) {
          if ($value1==$value['frequency'])
            $resultArr[$value['equ_id']][$month_name][$value['frequency']] = $value['rec_count'];
          else
            $resultArr[$value['equ_id']][$month_name][$value1] = 0;
        }
      }
    }
    //ksort($resultArr);

    $query= $this->db->select('*')
    ->from('tbl_line as tl')
    ->Join('tbl_line_log as tlg','tl.line_id=tlg.line_id','JOIN')
    ->where('tl.line_id',$line_id)
    ->get();
    $line_array = $query->result_array();

    foreach ($line_array as $key => $value) {
      $data['line_name']=$value["line_name"];

      $query= $this->db->select('*')
      ->from('tbl_equipment')
      ->where('equ_id',$value["equ_id"])
      ->get();
      $equ_row = $query->row_array();
      $result_data['equ_name']=$equ_row["equ_name"];
      $result_data['equ_code']=$equ_row["equ_code"];

      $array_data[$equ_row["equ_id"]]=$result_data;
    }

    $data['equ_name']=$array_data;
    $data['month_name']=$month_data;
    $data['search']=$search_name;

     //print_r($resultArr);
    //print_r($resultArr);

    $frequencyArr=array('Weekly','Monthly','Quaterly','Half-Yearly','Yearly');
    foreach ($resultArr as $res_key => $month) {
      // print_r($res_key); 
      foreach ($month_data as $key => $value) {
        // print_r($key);
        foreach ($frequencyArr as $key1 => $value1) {
           // print_r($value1);
          $emptyArr[$res_key][$value][$value1] = 0;
        }
      }
    } 
     //print_r($emptyArr);

    foreach ($emptyArr as $key => $value) {
      $data['cluster_monthly_frequency'][$key] = array_merge($value, $resultArr[$key]);
    }
    
     //print_r($data['cluster_monthly_frequency']);
    return $data;
  }


}