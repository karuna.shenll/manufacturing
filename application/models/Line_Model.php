<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Line_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}
	function lineList() {
        $search_name = $search_doc = $search_area=$condition = '';
        if (isset($_POST["line"])) {
            $line         = $_POST["line"];
            $search_name  = $line["line_name"];
            $search_doc   = $line["doc_no"];
            $search_area  = $line["area_id"];
            if (!empty($search_area)) {
                $condition.=" AND line.area_id=".$search_area;
            }
        }
		$query= $this->db->select('*')
                          ->from('tbl_line as line')
                          ->Join('tbl_area as area','area.area_id=line.area_id','JOIN')
                          ->where("line.line_name LIKE '%$search_name%'")
                          ->where("line.doc_no LIKE '%$search_doc%'".$condition)
                          ->order_by('line.line_id','desc')
                          ->get();
        $Linerows = $query->num_rows();
        if ($Linerows > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
	function addLine() {
        $line_name  = $this->input->post("line_name");
        $doc_no     = $this->input->post("doc_no");
        $area_id    = $this->input->post("area_id");
        $revision   = $this->input->post("sel_revision");
        $Arreqiupment  = $this->input->post("sel_eqiupment");
        $lineID     = $this->input->post("hndid");
        $hnd_log_id = $this->input->post("hnd_log_id");
        $datetimedb  = date("Y-m-d H:i:s");
        if (!empty($lineID)) {
            $dataupdate = array(
                "line_name" => $line_name,
                "doc_no"    => $doc_no,
                "area_id"   => $area_id,
                "revision_add" => $revision,
                "modified_date" => $datetimedb
            );
            $this->db->where('line_id',$lineID);
            $this->db->update('tbl_line', $dataupdate);
            $deletequery = $this->db->where('line_id',$lineID)
                    ->from('tbl_line_log')
                    ->delete();
                if (count($Arreqiupment)>0) {
                    foreach ($Arreqiupment as $key => $eqiupment) {
                        if (!empty($eqiupment)) {
                            $dataLog = array (
                                "line_id" => $lineID,
                                "equ_id"   =>$eqiupment,
                                "created_date" =>$datetimedb,
                                "modified_date" => $datetimedb
                            );
                            $this->db->insert('tbl_line_log', $dataLog);
                            $line_logId=$this->db->insert_id();
                        }
                    }
                }
            return "updated";
        } else if (!empty($line_name)) {
            $data = array(
                "line_name" => $line_name,
                "doc_no"    => $doc_no,
                "area_id"   => $area_id,
                "revision_add" => $revision,
                "created_date" =>$datetimedb,
                "modified_date" => $datetimedb
            );
            $this->db->insert('tbl_line', $data);
            $lineId=$this->db->insert_id();
            if (!empty($lineId)) { 
                if (count($Arreqiupment)>0) {
                    foreach ($Arreqiupment as $key => $eqiupment) {
                        if (!empty($eqiupment)) {
                            $dataLog = array (
                                "line_id" => $lineId,
                                "equ_id"   =>$eqiupment,
                                "created_date" =>$datetimedb,
                                "modified_date" => $datetimedb
                            );
                            $this->db->insert('tbl_line_log', $dataLog);
                            $line_logId=$this->db->insert_id();
                        }
                    }
                }
                return  $line_logId;
            }
        }
	}
	function getSingleLine($lineID) {
		$query= $this->db->select('*')
                          ->from('tbl_line')
                          ->where('line_id',$lineID)
                          ->get();
        $Linerows = $query->num_rows();
        if ($Linerows > 0) {
            foreach ($query->result() as $row) {
                return  $row;
            }
        } else {
            return false;
        }
	}
    function getLogLine($lineID){
        $query= $this->db->select('*')
                          ->from('tbl_line_log as linelog')
                          ->Join('tbl_equipment as equ','equ.equ_id=linelog.equ_id','JOIN')
                          ->where('linelog.line_id',$lineID)
                          ->order_by('linelog.line_log_id','ASC')
                          ->get();
        $equrows = $query->num_rows();
        if ($equrows > 0) {
                return  $query->result();
        } else {
            return false;
        }
        
    }
    function getActiveLine() {
        $query= $this->db->select('*')
                          ->from('tbl_line')
                          ->get();
        $grouprows = $query->num_rows();
        if ($grouprows > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
}