<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Employee_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}

	function EmployeeLogin() {
		$email = $this->input->post("email");
		$password = $this->input->post("password");
		$passwords=$this->Collective->encode($password);
        $role = "Employee";
			if(!empty($email) && !empty($passwords)) {
				$stmt = $this->db->query("select * from tbl_employee where emp_email='" . $email . "' and password='" . $passwords. "' and role='".$role."'");
				$rowcount = $stmt->num_rows();
				if ($rowcount == 1) {
					$data="";
					foreach ($stmt->result() as $row) {
						//$full_name=$row->name;
						//$names = explode(' ',$full_name);
						//$first_name = $names[0];
						$data = array(
							'e_id'  =>$row->e_id,
							'emp_name' => $row->emp_name,
							'emp_email'=>  $row->emp_email,
							'user_type'=> $row->role,
						);
						$this->session->set_userdata($data);
						return true;
					}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	function logout($adminlogid) {
		return true;
	}

	function employeeList() {
        $search_name = $search_emp = $search_email=$search_mobile= $search_user=$search_area=$condition = '';
        if (isset($_POST["employee"])) {
            $employee     = $_POST["employee"];
            $search_emp     = $employee["emp_id"];
            $search_name    = $employee["name"];
            $search_email   = $employee["email"];
            $search_mobile  = $employee["mobile"];
            $search_user    = $employee["username"];
            $search_area    = $employee["area"];
            if (!empty($search_area)) {
                $condition.=" AND employee.area_id=".$search_area;
            }
        }
		$query= $this->db->select('*')
                          ->from('tbl_employee as employee')
                          ->Join('tbl_area as area','area.area_id=employee.area_id','JOIN')
                          ->where("employee.emp_name LIKE '%$search_name%'")
                          ->where("employee.emp_id LIKE '%$search_emp%'")
                          ->where("employee.emp_email LIKE '%$search_email%'")
                          ->where("employee.emp_mobile LIKE '%$search_mobile%'")
                          ->where("employee.user_name LIKE '%$search_user%'".$condition)
                          ->order_by('employee.e_id','desc')
                          ->get();
        $Employeerows = $query->num_rows();
        if ($Employeerows > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
	function addEmployee() {
        $emp_id     = $this->input->post("emp_id");
        $name       = $this->input->post("name");
        $mobile     = $this->input->post("mobile");
        $email_id   = $this->input->post("email_id");
        $area_id    = $this->input->post("area_id");
        $line_id    = $this->input->post("line_id");
        $username   = $this->input->post("username");
        $password   = $this->input->post("password");
        $pass       = $this->Collective->encode($password);
        $skill      = $this->input->post("skill");
        $usertype   = $this->input->post("usertype");
        $empID      = $this->input->post("hndid");
        $datetimedb = date("Y-m-d H:i:s");
        if (!empty($empID)) {
            $dataupdate = array(
                "emp_id" => $emp_id,
                "emp_name"=> $name,
                "emp_email"=> $email_id,
                "emp_mobile"=> $mobile,
                "line_id" => implode("##",$line_id),
                "area_id" => $area_id,
                "user_name" => $username,
                "password" =>$pass,
                "skills" =>$skill,
                "role" =>$usertype,
                "modified_date" => $datetimedb,
            );
            $this->db->where('e_id',$empID);
            $this->db->update('tbl_employee', $dataupdate);
            return "updated";
        } else if (!empty($emp_id)) {
            $data = array(
                "emp_id" => $emp_id,
                "emp_name"=> $name,
                "emp_email"=> $email_id,
                "emp_mobile"=> $mobile,
                "line_id" => implode("##",$line_id),
                "area_id" => $area_id,
                "user_name" => $username,
                "password" =>$pass,
                "skills" =>$skill,
                "role" =>$usertype,
                "created_date" =>$datetimedb,
                "modified_date" => $datetimedb,
            );
            $this->db->insert('tbl_employee', $data);
            $employeeId=$this->db->insert_id();
            return  $employeeId;
        }
	}
	function getSingleEmployee($empID) {
		$query= $this->db->select('*')
                          ->from('tbl_employee')
                          ->where('e_id',$empID)
                          ->get();
        $employeerows = $query->num_rows();
        if ($employeerows > 0) {
            foreach ($query->result() as $row) {
                return  $row;
            }
        } else {
            return false;
        }
	}
    function openEmployeeList(){
        $query= $this->db->select('*')
                          ->from('tbl_employee')
                          ->where('role="Employee"')
                          ->get();
        $employeerows = $query->num_rows();
        if ($employeerows > 0) {
                return  $query->result();
        } else {
            return false;
        }
    }
}