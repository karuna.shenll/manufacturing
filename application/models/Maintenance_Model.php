<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Maintenance_Model extends MY_Model {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Collective');
	}
	function maintenanceList() {
        $search_name = $search_line = $search_frequency = $search_group = $condition = '';
        if (isset($_POST["maintenance"])) {
            $maintenance        = $_POST["maintenance"];
            $search_name      = $maintenance['equipment_name'];
            $search_line      = $maintenance['line_name'];
            $search_frequency = $maintenance['sel_frequency'];
            $search_group     = $maintenance['group_id'];
            if (!empty($search_group)) {
                $condition.= " AND group.group_id=".$search_group;
            }
        }
		$query= $this->db->select('*')
                          ->from('tbl_maintenance as main ')
                          ->join('tbl_group as group','group.group_id = main.group_id', 'JOIN')
                          ->join('tbl_line as line','line.line_id = main.line_id', 'JOIN')
                          ->join('tbl_equipment as equ','equ.equ_id = main.equ_id', 'JOIN')
                          ->where("main.frequency LIKE '%$search_frequency%'")
                          ->where("line.line_name LIKE '%$search_line%'")
                          ->where("equ.equ_name LIKE '%$search_name%'".$condition)
                          ->order_by('main.mainten_id','desc')
                          ->get();
        $Maintenancerows = $query->num_rows();
        if ($Maintenancerows > 0) {
            return $query->result();
        } else {
            return false;
        }
	}
	function addMaintenance() {
        $equ_id     = $this->input->post("sel_equipment");
        $group_id   = $this->input->post("sel_group");
        $line_id    = $this->input->post("sel_line");
        $frequency  = $this->input->post("sel_frequency");
        $exe_period = $this->input->post("exe_period");
        if (strtolower($exe_period)=="monthly") {
           $sel_week ='1';
        } else{
           $sel_week = $this->input->post("sel_week"); 
        }
        $comments   = $this->input->post("comments");
        $maintenID  = $this->input->post("hndid");
        $datetimedb = date("Y-m-d H:i:s");
        if (!empty($maintenID)) {
        	$dataupdate = array(
                "equ_id"  => $equ_id,
                "group_id" => $group_id,
                "line_id"  => $line_id,
                "frequency"  => $frequency,
                "execution_period" => $exe_period,
                "execution_week"  => $sel_week,
                "comments"  => $comments,
                "modified_date" => $datetimedb
            );
            $this->db->where('mainten_id',$maintenID);
            $this->db->update('tbl_maintenance', $dataupdate);
            return "updated";
        } else if (!empty($equ_id)) {
            $data = array(
                "equ_id"  => $equ_id,
                "group_id" => $group_id,
                "line_id"  => $line_id,
                "frequency"  => $frequency,
                "execution_period" => $exe_period,
                "execution_week"  => $sel_week,
                "comments"  => $comments,
                "created_date" => $datetimedb,
                "modified_date" => $datetimedb
            );
            $this->db->insert('tbl_maintenance',$data);
            $maintenId=$this->db->insert_id();
            return  $maintenId;
        }
	}
	function getSingleMaintenance($maintenID) {
		$query= $this->db->select('*')
                          ->from('tbl_maintenance')
                          ->where('mainten_id',$maintenID)
                          ->get();
        $maintenrows = $query->num_rows();
        if ($maintenrows > 0) {
            foreach ($query->result() as $row) {
                return  $row;
            }
        } else {
            return false;
        }
	}
    function getEquipmentLine() {
        $query= $this->db->select('*')
                          ->from('tbl_maintenance')
                          ->get();
        $maintenrows = $query->num_rows();
        if ($maintenrows > 0) {
                return  $query->result();
        } else {
            return false;
        }
    }
}