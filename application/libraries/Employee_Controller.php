<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * Description of Admin_Controller
 *
 * @author pc mart ltd
 */
class Employee_Controller extends MY_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		date_default_timezone_set('Asia/Kolkata');
	}
}