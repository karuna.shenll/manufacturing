<?php
error_reporting(0);
$navigationURL = reset(explode("?",end(explode("/",$_SERVER["REQUEST_URI"]))));
if (is_numeric($navigationURL)) {
    $navigationURL=$this->uri->segment(3);
}
?>
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 0px">
			<li class="sidebar-toggler-wrapper hide">
				<div class="sidebar-toggler">
					<span></span>
				</div>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='dashboard'|| $navigationURL=='layout' || $navigationURL=='viewreport' || $navigationURL=='layouttask' )?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/dashboard'; ?>" class="nav-link nav-toggle">
					<i class="dashboard-image"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='supervisor' || $navigationURL=='addsupervisor' || $navigationURL=='editsupervisor')?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/supervisor'; ?>" class="nav-link nav-toggle">
					<i class="icon-supervisor"></i>
					<span class="title">Manage Supervisor</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item  <?php echo ($navigationURL=='employee' || $navigationURL=='addemployee' ||  $navigationURL=='editemployee')?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/employee'; ?>" class="nav-link nav-toggle">
					<i class="icon-employee"></i>
					<span class="title">Manage Employee</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item  <?php echo ($navigationURL=='equipment' || $navigationURL=='addequipment' || $navigationURL=='editequipment')?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/equipment'?>" class="nav-link nav-toggle">
					<i class="icon-equipment"></i>
					<span class="title">Manage Equipment</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='area' || $navigationURL=='addarea' || $navigationURL=='editarea' )?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/area'; ?>" class="nav-link nav-toggle">
					<i class="icon-area"></i>
					<span class="title">Manage Area</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='line'|| $navigationURL=='addline' || $navigationURL=='editline')?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/line'; ?>" class="nav-link nav-toggle">
					<i class="icon-assembly"></i>
					<span class="title">Manage Line</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='customer' || $navigationURL=='addcustomer' || $navigationURL=='editcustomer')?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/customer'; ?>" class="nav-link nav-toggle">
					<i class="icon-client"></i> 
					<span class="title">Manage Customer</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='maintenance' || $navigationURL=='addmaintenance' || $navigationURL=='editmaintenance')?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/maintenance'; ?>" class="nav-link nav-toggle">
					<i class="icon-mainten"></i> 
					<span class="title">Manage Maintenance</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='task' || $navigationURL=='addtask' || $navigationURL=='edittask' || $navigationURL=='viewtask')?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/task'; ?>" class="nav-link nav-toggle">
					<i class="icon-task"></i> 
					<span class="title">Manage Task</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='roles' || $navigationURL=='addrole' || $navigationURL=='editrole')?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/roles'; ?>" class="nav-link nav-toggle">
					<i class="icon-role"></i> 
					<span class="title">Manage Roles</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='group' || $navigationURL=='addgroup' || $navigationURL=='editgroup')?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/group'; ?>" class="nav-link nav-toggle">
					<i class="icon-category"></i> 
					<span class="title">Manage Group</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='reports' || $navigationURL=='viewreports')?'active open':''; ?>">
				<a href="<?php echo base_url().'admin/reports'; ?>" class="nav-link nav-toggle">
					<i class="icon-report"></i> 
					<span class="title">Manage Reports</span>
					<span class="selected"></span>
				</a>
			</li>
		</ul>
	</div>
</div>