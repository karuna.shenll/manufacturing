<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/client.png" class="imgbasline"> Customer List</div>
            <div class="actions">
                <a href="client/addclient" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Customer</a>
            </div>
        </div>
        <div class="portlet-body">
        	<div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="admin[client_name]" id="client_name" placeholder="Customer Name" value="">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="admin[email]" id="email" placeholder="Email" value="">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="admin[mobile]" id="mobile" placeholder="Mobile" value="">
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="<?php echo base_url()."admin/client"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
	            	<thead>
	                    <tr>
	                        <th style="width: 50px;">SI.NO</th>
	                        <th>Customer Name</th>
	                        <th>Email</th>
	                        <th>Mobile</th>
	                        <th>Products</th>
	                        <th>Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td>Customer 1</td>
	                        <td>customer1@gmail.com</td>
	                        <td>9303939939</td>
	                        <td>Products1</td>
	                        <td> <a href="client/editclient" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td>Customer 2</td>
	                        <td>customer2@gmail.com</td>
	                        <td>95764564564</td>
	                        <td>Products2</td>
	                        <td> <a href="client/editclient" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr>
	                    <tr>
	                        <td> 3 </td>
	                        <td>Customer 3</td>
	                        <td>customer3@gmail.com</td>
	                        <td>8494884949</td>
	                        <td>Products3</td>
	                        <td> <a href="client/editclient" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr>
	                    <tr>
	                        <td> 4 </td>
	                        <td>Customer 4</td>
	                        <td>customer4@gmail.com</td>
	                        <td>9404974789</td>
	                        <td>Products4</td>
	                        <td> <a href="client/editclient" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr>
	                    <tr>
	                        <td> 5 </td>
	                        <td>Customer 5</td>
	                        <td>customer5@gmail.com</td>
	                        <td>6738030900</td>
	                        <td>Products5</td>
	                        <td> <a href="client/editclient" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>