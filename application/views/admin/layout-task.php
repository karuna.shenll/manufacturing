<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tasklist.png" class="imgbasline"> View Task
            </div>
            <div class="actions">
                <a  href="<?php echo site_url('admin/dashboard/layout/'.$task_report['equ_id']); ?>"  class="btn red customrestbtn"> <i class="fa fa-chevron-left"></i> Back</a>
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_task" id="frm_task" action="" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <h3 class="emptaskhead">PM ADHERENCE REPORT </h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                            <table class="table table-striped table-bordered table-hover" id="employeesheet">
                                <thead>
                                    <tr>
                                        <th nowrap>Machine Name :</th>
                                        <th><?php echo $task_report['equ_name']; ?></th>
                                        <th></th>
                                        <th></th>
                                        <th>Month : <?php echo $task_report['month_name'];  ?></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th>Frequency :</th>
                                        <th><?php echo $task_report['frequency']; ?></th>
                                        <th></th>
                                        <th>Machine ID : <?php echo $task_report['equ_id']; ?></th>
                                        <th>Year : <?php echo $task_report['year'];  ?></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Activcity Description</th>
                                        <th>Observation</th>
                                        <th>Action Taken</th>
                                        <th>Comments</th>
                                        <th>status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <?php echo $task_report['task_name']; ?>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="observation" id="observation" placeholder="Observation" value="<?php echo $task_report['observation']; ?>" disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="action_taken" id="action_taken" placeholder="Action Taken" value="<?php echo $task_report['action_taken']; ?>" disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="comments" id="comments" placeholder="Comments" value="<?php echo $task_report['comments']; ?>" disabled>
                                        </td>
                                         <td>
                                            <input type="text" class="form-control" name="comments" id="comments" placeholder="Comments" value="<?php echo $task_report['log_status']; ?>" disabled>
                                        </td>
                                    </tr>
                                   
                                    <tr>
                                        <td></td><td><b>Done by : <input type="text"  name="done_by" id="done_by" placeholder="Done by" value="<?php echo $task_report['done_by']; ?>" style="width:200px;height:32px" disabled></b></td><td></td><td></td><td></td><td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td><b>Date : &nbsp;&nbsp; &nbsp; &nbsp;<input type="text"  name="date" id="date" placeholder="Date" value="<?php echo $task_report['created_date'];  ?>" disabled style="width:200px;height:32px"></b></td><td></td><td><b>Verified by :</td><td> <input type="text"  name="verified_by" id="verified_by" placeholder="Verified by" value="<?php echo $task_report['verified_by']; ?>" class="form-control" disabled></b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td>DOCUMENT NO : FA_PM_309</td><td></td><td></td><td></td><td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td>REVISION LEVEL : <input type="text"  name="date" id="date" placeholder="REVISION LEVEL" value="<?php echo $task_report['revision_level']; ?>" disabled style="width:200px;height:32px"></td><td></td><td></td><td></td><td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td>REVISION DATE : <input type="text"  name="date" id="date" placeholder="REVISION DATE" value="<?php echo $task_report['revision_date']; ?>" disabled style="width:200px;height:32px"></td><td></td><td></td><td></td><td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-actions text-right">
                    <a href="<?php echo site_url('admin/dashboard/layout/'.$task_report['equ_id']); ?>"  class="btn red customrestbtn"> <i class="fa fa-chevron-left"></i> Back</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>