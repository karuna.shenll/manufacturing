<?php
$equ_id=$equ_code=$equ_model=$equ_name=$group_id='';
$mode="Add";
if(!empty($getSingleEquipment)) {
	$equ_id    = $getSingleEquipment->equ_id;
	$equ_code  = $getSingleEquipment->equ_code;
	$equ_model = $getSingleEquipment->equ_model;
	$equ_name  = $getSingleEquipment->equ_name;
	$group_id  = $getSingleEquipment->group_id;
	$mode="Edit";
}
?>
<div class="page-content">
	<div class="portlet box blue boardergrey">
		<div class="portlet-title">
			<div class="caption">
				<img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/equipment.png" class="imgbasline"> <?php echo $mode?> Equipment
			</div>
		</div>
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form name="frm_equipment" id="frm_equipment" action="" class="horizontal-form" method="POST">
				<input type="hidden" name="hndid" value="<?php echo $equ_id?>">
				<div class="form-body">
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Equipment Code</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="equipment_code" id="equipment_code" placeholder="Equipment Code" value="<?php echo $equ_code?>">
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Equipment Model</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="equipment_model" id="equipment_model" placeholder="Equipment Model" value="<?php echo $equ_model?>">
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Equipment Name</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="equipment_name" id="equipmentname" placeholder="Equipment Name" value="<?php echo $equ_name?>">
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Group</label>
								<div class="col-md-8">
									<select class="form-control" name="group" id="group">
										<option value="">Select Group</option>
										<?php
											foreach($getArrGroup as $groupData) {
												$selected = "";
												if ($group_id == $groupData->group_id) {
                                                    $selected ="selected";
												}
                                        		echo "<option value=".$groupData->group_id." ".$selected.">".$groupData->group_name."</option>";
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
				</div>
				<div class="form-actions formbtncenter">
					<button type="submit" class="btn green customsavebtn">
						<i class="fa fa-check"></i> Save
					</button>
					<a href="<?php echo base_url() ?>admin/equipment"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>