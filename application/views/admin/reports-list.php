<?php
	$search_area = $search_line=$search_name=$search_doc=$search_from=$search_to='';
    if (!empty($search)) {
        $search_area    = $search['area_name'];
        $search_line    = $search['line_name']; 
        $search_name    = $search['equname']; 
        $search_doc     = $search['doc_no'];
        $search_from    = $search['from_date'];
        $search_to      = $search['to_date']; 
    }
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
        <div class="alert alert-success alert-dismissible">
          	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/report.png" class="imgbasline"> Report List</div>
        </div>
        <div class="portlet-body">
        	<form id="frm_reports" name="frm_reports" method="POST">
	        	<div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="report[area_name]" id="area_name" placeholder="Area" value="<?php echo $search_area?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="report[line_name]" id="line_name" placeholder="Line Name" value="<?php echo $search_line?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="report[equname]" id="equname" placeholder="Eqiupment Name" value="<?php echo $search_name?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="report[doc_no]" id="doc_no" placeholder="Doc No" value="<?php echo $search_doc?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="report[from_date]" id="from_date" placeholder="From Date" value="<?php echo $search_from?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="report[to_date]" id="to_date" placeholder="To Date" value="<?php echo $search_to?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."admin/reports"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Line Name</th>
		                        <th>Doc No</th>
		                        <th>Area</th>
		                        <th>Eqiupment</th>
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                    if (!empty($getReportList)) {
		                    	$sno=1;
		                    	foreach ($getReportList as $ReportList) {
		                    ?>
		                    <tr>
		                        <td><?php echo $sno ?></td>
		                        <td><?php echo $ReportList->line_name?></td>
		                        <td><?php echo $ReportList->doc_no?></td>
		                        <td><?php echo $ReportList->area_name?></td>
		                        <td><?php echo $ReportList->equ_name?></td>
		                        <td> 
		                        	<a href="reports/viewreports/<?php echo $ReportList->line_id ?>" class="btn btn-info btn-xs customviewbtn"><i class="fa fa-ticket"></i> Report</a>
		                        </td>
		                    </tr>
		                    <?php
		                    	$sno++;
			                    }
			                }
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>