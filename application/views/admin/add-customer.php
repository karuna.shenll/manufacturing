<?php
$cus_id=$cus_name=$cus_email=$cus_mobile=$line_id=$products=$address='';
$mode="Add";
if(!empty($getSingleCustomer)) {
	$cus_id     = $getSingleCustomer->cus_id;
	$cus_name   = $getSingleCustomer->customer_name;
	$cus_email  = $getSingleCustomer->email;
	$cus_mobile = $getSingleCustomer->mobile;
	$products   = $getSingleCustomer->products;
	$address    = $getSingleCustomer->address;
	$line_id    = $getSingleCustomer->line_id;
	$mode="Edit";
}
?>
<div class="page-content">
	<div class="portlet box blue boardergrey">
		<div class="portlet-title">
			<div class="caption">
				<img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/client.png" class="imgbasline"> <?php echo $mode?> Customer
			</div>
		</div>
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form name="frm_customer" id="frm_customer" action="" class="horizontal-form" method="POST">
				<input type="hidden" name="hndid" value="<?php echo $cus_id ?>">
				<div class="form-body">
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Customer Name</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="customer_name" id="customer_name" placeholder="Customer Name" value="<?php echo $cus_name ?>">
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Mobile</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" value="<?php echo $cus_mobile ?>">
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $cus_email ?>">
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Address</label>
								<div class="col-md-8">
									<textarea class="form-control" name="address" id="address"><?php echo $address ?></textarea> 
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Products </label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="products" id="products" placeholder="Products" value="<?php echo $products ?>">
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Line</label>
                                <div class="col-md-8">
                                    <select name="line_id" id="line_id" class="form-control">
                                    	<option value="">Select Line</option>
                                    	<?php
                                    	foreach($getActiveLine as $lineData) {
                                    		$sel="";
		                            		if($line_id==$lineData->line_id){
		                            			$sel="selected";
		                            		}
                                            echo "<option value=".$lineData->line_id." ".$sel.">".$lineData->line_name."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
							</div>
						</div>
						<!--/span-->
					</div>
				</div>
				<div class="form-actions formbtncenter">
					<button type="submit" class="btn green customsavebtn">
						<i class="fa fa-check"></i> Save
					</button>
					<a href="<?php echo base_url() ?>admin/customer"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>