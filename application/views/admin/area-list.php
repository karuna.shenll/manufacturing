<?php
	$search_area = $search_status='';
    if (!empty($search)) {
        $search_area      = $search['area_name'];
        $search_status    = $search['status']; 
    }
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
            <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <?php
    $msgs=$this->session->flashdata('message_failure');
    if(!empty($msgs)) {
    ?>
        <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
            <?php echo $msgs ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/area.png" class="imgbasline"> Area List</div>
            <div class="actions">
                <a href="area/addarea" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Area</a>
            </div>
        </div>
        <div class="portlet-body">
        	<form name="frm_arealist" id="frm_arealist" method="POST" action="">
	        	<div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="area[area_name]" id="area_name" placeholder="Area" value="<?php echo $search_area ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <select class="form-control" name="area[status]" id="status">
									<option value="">Select Status</option>
									<option value="1" <?php echo ($search_status=="1")?"selected":"";?>>Active</option>
									<option value="2" <?php echo ($search_status=="2")?"selected":"";?>>Inactive</option>
								</select>
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."admin/area"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Area</th>
		                        <th>Status</th>
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                    	if (!empty($getAreaList)){
		                    		$sno = 1;
		                    		foreach ($getAreaList as $areaData) {
		                    			$label  = ($areaData->status== "1")?"success":"danger";
		                    			$status = ($areaData->status== "1")?"Active":"Inactive";
		                    ?>
					                    <tr>
					                        <td><?php echo $sno ?></td>
					                        <td><?php echo $areaData->area_name ?></td>
					                        <td><span class="label label-sm label-<?php echo $label ?> labelradius" style="padding: 1px 13px;"><?php echo $status ?></span></td>
					                        <td> <a href="area/addarea/<?php echo $areaData->area_id ?>" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn deleteRowid" data-id="<?php echo $areaData->area_id ?>" data-type="area" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
					                    </tr>
		                    <?php   
		                            $sno++; 
		                            }
		                    	}
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>