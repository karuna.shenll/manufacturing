<?php
$this->load->helper('common_helper');
?>
<div class="page-content"  id="dashboard">
    <?php 
        $msg=$this->session->flashdata('success');
        if(!empty($msg)){ ?>
            <div>
                <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $msg ?>
                </div>
            </div>
        <?php }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/dashboard.png" class="imgbasline"> Dashboard
            </div>
            <div class="actions">
                <!--  <a href="task/addtask" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Task</a> -->
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                
                    <?php 
                        if (count($get_equp)>0) {
                            foreach ($get_equp as $key => $val) { ?>                               
                                      <?php foreach ($val['task'] as $key => $value2) { 
                                        $task_status = $value2['task_status']; 
                                      }?>
                                   
				    					<div class="col-md-6">
												<a href="<?php echo site_url('admin/dashboard/layout/'.$val['equ_id']); ?>" class="dashboardatag">
													<div class="col-md-12 dashboardout">
														<div class="col-md-12 dashboard-head-name"><b><?php echo $val['equ_name']; ?></b></div>
                                                         
														<div class="col-md-12 dashboardheight"></div>
															<div class="col-md-10 text-center dashtext">
															<?php 
                                                            if(count($val['task'])>0){ 
                                                            $colors="";
                                                            foreach ($val['subtask'] as $key => $value) { 
															$task_status = $value2['task_status'];
                                                            $log_status = $value['log_status']; 
															$colors=get_color($task_status,$log_status);
															?>
															<div class="col-md-2 dashcount" style="background-color: <?php echo $colors; ?>"> </div>
															<?php } }
                                                            else
                                                            {
                                                            ?>
                                                            <div class="col-md-12" style="text-align: center"><b>-- Task is Empty --</b></div>
                                                            <?php
                                                            }
                                                             ?>
															</div>
															<div class="col-md-12 dashboardheight"> </div>
                                                             
													</div>
												</a>
											</div>
                                   
                                  
                            <?php } 
                        }else{
                            ?>
                            <div align="center"> No Record Found</div>
                            <?php
                        } 
                    ?>
                
            </div>
        </div> 
    </div>
</div>