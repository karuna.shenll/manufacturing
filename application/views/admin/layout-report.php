<!-- BEGIN CONTENT BODY -->
<?php
$this->load->helper('common_helper');
if($view_report['cluster']==1){
	$jan_week=(!empty($view_report['cluster_monthly_frequency']['Jan']['Weekly'])?'W':'');
	$jan_mon=(!empty($view_report['cluster_monthly_frequency']['Jan']['Monthly'])?'M':'');
	$jan_quat=(!empty($view_report['cluster_monthly_frequency']['Jan']['Quateraly'])?'Q':'');
	$jan_half=(!empty($view_report['cluster_monthly_frequency']['Jan']['Half-Yearly'])?'H':'');
	$jan_year=(!empty($view_report['cluster_monthly_frequency']['Jan']['Yearly'])?'Y':'');

	$feb_week=(!empty($view_report['cluster_monthly_frequency']['Feb']['Weekly'])?'W':'');
	$feb_mon=(!empty($view_report['cluster_monthly_frequency']['Feb']['Monthly'])?'M':'');
	$feb_quat=(!empty($view_report['cluster_monthly_frequency']['Feb']['Quateraly'])?'Q':'');
	$feb_half=(!empty($view_report['cluster_monthly_frequency']['Feb']['Half-Yearly'])?'H':'');
	$feb_year=(!empty($view_report['cluster_monthly_frequency']['Feb']['Yearly'])?'Y':'');

	$mar_week=(!empty($view_report['cluster_monthly_frequency']['Mar']['Weekly'])?'W':'');
	$mar_mon=(!empty($view_report['cluster_monthly_frequency']['Mar']['Monthly'])?'M':'');
	$mar_quat=(!empty($view_report['cluster_monthly_frequency']['Mar']['Quateraly'])?'Q':'');
	$mar_half=(!empty($view_report['cluster_monthly_frequency']['Mar']['Half-Yearly'])?'H':'');
	$mar_year=(!empty($view_report['cluster_monthly_frequency']['Mar']['Yearly'])?'Y':'');

	$apr_week=(!empty($view_report['cluster_monthly_frequency']['Apr']['Weekly'])?'W':'');
	$apr_mon=(!empty($view_report['cluster_monthly_frequency']['Apr']['Monthly'])?'M':'');
	$apr_quat=(!empty($view_report['cluster_monthly_frequency']['Apr']['Quateraly'])?'Q':'');
	$apr_half=(!empty($view_report['cluster_monthly_frequency']['Apr']['Half-Yearly'])?'H':'');
	$apr_year=(!empty($view_report['cluster_monthly_frequency']['Apr']['Yearly'])?'Y':'');

	$may_week=(!empty($view_report['cluster_monthly_frequency']['May']['Weekly'])?'W':'');
	$may_mon=(!empty($view_report['cluster_monthly_frequency']['May']['Monthly'])?'M':'');
	$may_quat=(!empty($view_report['cluster_monthly_frequency']['May']['Quateraly'])?'Q':'');
	$may_half=(!empty($view_report['cluster_monthly_frequency']['May']['Half-Yearly'])?'H':'');
	$may_year=(!empty($view_report['cluster_monthly_frequency']['May']['Yearly'])?'Y':'');

	$jun_week=(!empty($view_report['cluster_monthly_frequency']['Jun']['Weekly'])?'W':'');
	$jun_mon=(!empty($view_report['cluster_monthly_frequency']['Jun']['Monthly'])?'M':'');
	$jun_quat=(!empty($view_report['cluster_monthly_frequency']['Jun']['Quateraly'])?'Q':'');
	$jun_half=(!empty($view_report['cluster_monthly_frequency']['Jun']['Half-Yearly'])?'H':'');
	$jun_year=(!empty($view_report['cluster_monthly_frequency']['Jun']['Yearly'])?'Y':'');
}

if($view_report['cluster']==2){
	$jul_week=(!empty($view_report['cluster_monthly_frequency']['Jul']['Weekly'])?'W':'');
	$jul_mon=(!empty($view_report['cluster_monthly_frequency']['Jul']['Monthly'])?'M':'');
	$jul_quat=(!empty($view_report['cluster_monthly_frequency']['Jul']['Quateraly'])?'Q':'');
	$jul_half=(!empty($view_report['cluster_monthly_frequency']['Jul']['Half-Yearly'])?'H':'');
	$jul_year=(!empty($view_report['cluster_monthly_frequency']['Jul']['Yearly'])?'Y':'');

	$aug_week=(!empty($view_report['cluster_monthly_frequency']['Aug']['Weekly'])?'W':'');
	$aug_mon=(!empty($view_report['cluster_monthly_frequency']['Aug']['Monthly'])?'M':'');
	$aug_quat=(!empty($view_report['cluster_monthly_frequency']['Aug']['Quateraly'])?'Q':'');
	$aug_half=(!empty($view_report['cluster_monthly_frequency']['Aug']['Half-Yearly'])?'H':'');
	$aug_year=(!empty($view_report['cluster_monthly_frequency']['Aug']['Yearly'])?'Y':'');

	$sep_week=(!empty($view_report['cluster_monthly_frequency']['Sep']['Weekly'])?'W':'');
	$sep_mon=(!empty($view_report['cluster_monthly_frequency']['Sep']['Monthly'])?'M':'');
	$sep_quat=(!empty($view_report['cluster_monthly_frequency']['Sep']['Quateraly'])?'Q':'');
	$sep_half=(!empty($view_report['cluster_monthly_frequency']['Sep']['Half-Yearly'])?'H':'');
	$sep_year=(!empty($view_report['cluster_monthly_frequency']['Sep']['Yearly'])?'Y':'');

	$oct_week=(!empty($view_report['cluster_monthly_frequency']['Oct']['Weekly'])?'W':'');
	$oct_mon=(!empty($view_report['cluster_monthly_frequency']['Oct']['Monthly'])?'M':'');
	$oct_quat=(!empty($view_report['cluster_monthly_frequency']['Oct']['Quateraly'])?'Q':'');
	$oct_half=(!empty($view_report['cluster_monthly_frequency']['Oct']['Half-Yearly'])?'H':'');
	$oct_year=(!empty($view_report['cluster_monthly_frequency']['Oct']['Yearly'])?'Y':'');

	$nov_week=(!empty($view_report['cluster_monthly_frequency']['Nov']['Weekly'])?'W':'');
	$nov_mon=(!empty($view_report['cluster_monthly_frequency']['Nov']['Monthly'])?'M':'');
	$nov_quat=(!empty($view_report['cluster_monthly_frequency']['Nov']['Quateraly'])?'Q':'');
	$nov_half=(!empty($view_report['cluster_monthly_frequency']['Nov']['Half-Yearly'])?'H':'');
	$nov_year=(!empty($view_report['cluster_monthly_frequency']['Nov']['Yearly'])?'Y':'');

	$dec_week=(!empty($view_report['cluster_monthly_frequency']['Dec']['Weekly'])?'W':'');
	$dec_mon=(!empty($view_report['cluster_monthly_frequency']['Dec']['Monthly'])?'M':'');
	$dec_quat=(!empty($view_report['cluster_monthly_frequency']['Dec']['Quateraly'])?'Q':'');
	$dec_half=(!empty($view_report['cluster_monthly_frequency']['Dec']['Half-Yearly'])?'H':'');
	$dec_year=(!empty($view_report['cluster_monthly_frequency']['Dec']['Yearly'])?'Y':'');
}

$search_name ='';
if (!empty($search)) {
echo  $search_name = $search['select_report'];      
}
?>
<div class="page-content">
		<?php
	    $msg=$this->session->flashdata('message_success');
	    if(!empty($msg)) { ?>
		    <div class="col-md-12 col-sm-12 col-xs-12">
		        <div class="alert alert-success alert-dismissible">
		          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		              <?php echo $msg ?>
		        </div>
		    </div>
	<?php } ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/report.png" class="imgbasline">Reports</div>
            <div class="actions">
               <a href="<?php echo site_url('admin/dashboard/layout/'.$view_report['equ_id']); ?>" class="btn red btn-sm customrestbtn"><i class="fa fa-chevron-left"></i> Back</a>
            </div>
        </div>
        <div class="portlet-body">
        	<div class="row">
        		<form action="" method="post">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <select  class="form-control" name="report[select_report]" id="select_report">       
                            	<option value="Cluster1" <?php echo ($search_name=="Cluster1")?"selected":""; ?>>Cluster1</option>
                           		<option value="Cluster2" <?php echo ($search_name=="Cluster2")?"selected":""; ?>>Cluster2</option>
                           </select>
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="submit" class="btn btn-warning customsearchtbtn searchreport"> <i class="fa fa-search"></i> Search</button>
	        				<a href="<?php echo site_url('admin/dashboard/viewreport/'.$view_report['equ_id']); ?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </form>
	        </div>
        	<div class="table-responsive" style="overflow-x: auto;margin-top:0px;"><!--suppliertbl-->
	            <table class="table table-striped table-bordered" id="reportsheet">
		            <tbody>
		            	<tr>
			            	<td colspan="33" class="bordernone">
			            		<h3 class="emptaskhead">Master Preventive Maintenance  Schedule</h3>
			            	</td>
			            </tr>
		            	<tr>
			            	<td colspan="32" class="borderright bordertop borderleft" style="border-bottom: :1px solid #000000;">
			            	</td>
			            	<td nowrap class="borderright">
			            		<b>Year : 2018</b>
			            	</td>
			            </tr>
			            
			            <tr>
			            	<td class="text-center" colspan="3" nowrap>
			            		<b>Location : <?php echo $view_report["line_name"]  ?></b>
			            	</td>
			            	<?php 
			            	foreach ($view_report["month_name"] as  $month_name){
			            	?>
			            	<td class="text-center" colspan="5">
			            		<b><?php echo $month_name['month_name']; ?></b>
			            	</td>
			            <?php } ?>
			            </tr>
			            <tr>
			            	<td nowrap><b>OPN No</b></td>
			            	<td nowrap><b>Equipment Name</b></td>
			            	<td nowrap><b>Plan Vs Actuall</b></td>
			            	<td nowrap class="rotate">Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            </tr>
			            <tr>
			            	
			            	<td class="centeropentd" nowrap rowspan="2"><b>1505</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b><?php echo $view_report["equ_name"]; ?></b></td>
			            	<td nowrap class="text-center">Plan</td>

			            <?php if($view_report['cluster']==1){ ?>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jan_week); ?>"><?php echo $jan_week; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jan_mon); ?>"><?php echo $jan_mon; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jan_quat); ?>"><?php echo $jan_quat; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jan_half); ?>"><?php echo $jan_half; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jan_year); ?>"><?php echo $jan_year; ?></td>

			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($feb_week); ?>"><?php echo $feb_week; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($feb_mon); ?>"><?php echo $feb_mon; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($feb_quat); ?>"><?php echo $feb_quat; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($feb_half); ?>"><?php echo $feb_half; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($feb_year); ?>"><?php echo $feb_year; ?></td>

			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($mar_week); ?>"><?php echo  $mar_week; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($mar_mon); ?>"><?php echo $mar_mon; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($mar_quat); ?>"><?php echo $mar_quat; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($mar_half); ?>"><?php echo $mar_half; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($mar_year); ?>"><?php echo $mar_year; ?></td>

			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($apr_week); ?>"><?php echo  $apr_week; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($apr_mon); ?>"><?php echo $apr_mon; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($apr_quat); ?>"><?php echo $apr_quat; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($apr_half); ?>"><?php echo $apr_half; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($apr_year); ?>"><?php echo $apr_year; ?></td>

			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($may_week); ?>"><?php echo  $may_week; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($may_mon); ?>"><?php echo $may_mon; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($may_quat); ?>"><?php echo $may_quat; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($may_half); ?>"><?php echo  $may_half; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($may_year); ?>"><?php echo $may_year; ?></td>

			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jun_week); ?>"><?php echo $jun_week; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jun_mon); ?>"><?php echo  $jun_mon; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jun_quat); ?>"><?php echo $jun_quat; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jun_half); ?>"><?php echo $jun_half; ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jun_year); ?>"><?php echo $jun_year; ?></td>
			            <?php } ?>

			            <?php if($view_report['cluster']==2){ ?>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jul_week); ?>"><?php echo $jul_week;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jul_mon); ?>"><?php echo $jul_mon;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jul_quat); ?>"><?php echo $jul_quat;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jul_half); ?>"><?php echo $jul_half;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($jul_year); ?>"><?php echo $jul_year;  ?></td>

			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($aug_week); ?>"><?php echo $aug_week;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($aug_mon); ?>"><?php echo $aug_mon;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($aug_quat); ?>"><?php echo $aug_quat;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($aug_half); ?>"><?php echo $aug_half;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($aug_year); ?>"><?php echo $aug_year;  ?></td>

			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($sep_week); ?>"><?php echo $sep_week;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($sep_mon); ?>"><?php echo $sep_mon;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($sep_quat); ?>"><?php echo $sep_quat;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($sep_half); ?>"><?php echo $sep_half;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($sep_year); ?>"><?php echo $sep_year;  ?></td>

			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($oct_week); ?>"><?php echo $oct_week;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($oct_mon); ?>"><?php echo $oct_mon;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($oct_quat); ?>"><?php echo $oct_quat;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($oct_half); ?>"><?php echo $oct_half;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($oct_year); ?>"><?php echo $oct_year;  ?></td>

			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($nov_week); ?>"><?php echo $nov_week;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($nov_mon); ?>"><?php echo $nov_mon;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($nov_quat); ?>"><?php echo $nov_quat;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($nov_half); ?>"><?php echo $nov_half;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($nov_year); ?>"><?php echo $nov_year;  ?></td>

			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($dec_week); ?>"><?php echo $dec_week;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($dec_mon); ?>"><?php echo $dec_mon;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($dec_quat); ?>"><?php echo $dec_quat;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($dec_half); ?>"><?php echo $dec_half;  ?></td>
			            	<td nowrap class="text-center" style="background-color: <?php echo set_clr($dec_year); ?>"><?php echo $dec_year;  ?></td>
			            <?php } ?>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>

			              <tr>
			            	<td class="bordernone"></td>
			            	<td class="bordernone"></td>
			            	<td class="bordernone"></td>
			            	<td class="borderright"></td>
			            	<td class="bordernone"></td>
			            	<td class="bordernone"></td>
			            	<td class="bordernone"></td>
			            	<td class="bordernone"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright borderbottom"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="bordernone"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="bordernone"></td>
			            	<td class="bordernone"></td>
			            </tr>
			        
			            <tr>
			            	<td nowrap rowspan="6" class="borderleft borderright borderbottom bordertop"></td>
			            	<td nowrap rowspan="6" colspan="2" class="borderbottom">Weekly ( Every once in Week)<br><br>Monthly ( once in a Month)<br><br> Quateraly (every 3 Months)<br><br>Half Yearly (Once in 6 Months) <br><br>Yearly (Once in a year)	<br><br>PM Completed</td>
			            	<td nowrap class="text-center wcolor bordertop" >W</td>
			            	<td nowrap class="text-center borderbottom" colspan="4"></td>
			            	<td nowrap class="text-center" colspan="5"><b>Maintenance Engineer</b></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap class="text-center" colspan="6"><b>Manufacturing Manager</b></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap class="text-center" colspan="5"><b>REVISION</b></td>
			            	<td nowrap class="text-center" colspan="5"><b>DOCUMENT NO</b></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center borderbottom" colspan="4"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom" ></td>
			            	<td nowrap class="text-center borderbottom" colspan="6"></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5" ></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center borderbottom" colspan="4"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom" ></td>
			            	<td nowrap class="text-center borderbottom" colspan="6"></td>
			            	<td nowrap class="text-center borderbottom" ></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center borderbottom" colspan="4"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap class="text-center borderbottom" colspan="6"></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5">4.0</td>
			            	<td nowrap class="text-center borderbottom" colspan="5">FA_AD_008</td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center ycolor">Y</td>
			            	<td nowrap class="text-center borderbottom" colspan="4"></td>
			            	<td nowrap colspan="5"><b>Sign :</b></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap colspan="6"><b>Sign :</b></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center ccolor">C</td>
			            	<td nowrap class="text-center borderbottom" colspan="4"></td>
			            	<td nowrap colspan="5"><b>Date :<b></td>
			            	<td nowrap class="text-center borderbottom" ></td>
			            	<td nowrap colspan="6"><b>Date :<b></td>
			            	<td nowrap class="text-center borderbottom" ></td>
			            	<td nowrap class="text-center" colspan="5"></td>
			            	<td nowrap class="text-center" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            </tr>
			            <tr>
			            	<td colspan="33" class="bordernone"></td>
			            </tr>
			            <tr>
			            	<td nowrap rowspan="5" class="bordernone">Rev date<br><br>28-Dec-14<br><br>2-Jan-15<br><br>13-May-15<br><br>22-Jun-15</td>
			            	<td nowrap rowspan="5" colspan="2" class="bordernone">Rev History<br><br>Final inspection station removed<br><br> Vibration station combined Cat tester<br><br>Annaul PM added for pointer press(1535)<br><br>Mask assy station modified</td>
			            	<td nowrap class="text-center bordernone"></td>
			            	<td nowrap class="text-center bordernone" colspan="4"></td>
			            	<td nowrap class="text-center bordernone" colspan="5" ></td>
			            	<td nowrap class="text-center bordernone"></td>
			            	<td nowrap class="text-center bordernone" colspan="6"></td>
			            	<td nowrap class="text-center bordernone"></td>
			            	<td nowrap class="text-center bordernone" colspan="5"></td>
			            	<td nowrap class="text-center bordernone" colspan="5" ></td>
			            	<td nowrap class="text-center bordernone" ></td>
			            	<td nowrap class="text-center bordernone"></td>
			            </tr>
			    
		            </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>