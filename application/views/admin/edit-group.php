
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/category.png" class="imgbasline"> Edit Group
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_group" id="frm_group" action="<?php echo base_url() ?>admin/category" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Group Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="group_name" id="group_name" placeholder="Group Name" value="Group 1">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Status</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <div class="icheck-inline">
                                            <label class="">
                                                <div class="iradio_flat-green" style="position: relative;"><input type="radio" name="radio2" class="icheck" data-radio="iradio_flat-green" checked><ins class="iCheck-helper"></ins></div> Active 
                                            </label>
                                            <label class="">
                                                <div class="iradio_flat-green " style="position: relative;"><input type="radio" name="radio2" class="icheck" data-radio="iradio_flat-green" ><ins class="iCheck-helper"></ins></div> Inactive  
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>admin/group"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>