<?php
$mode="Add";
$supervisor_id =$emp_id =$supervisor_name=$supervisor_email=$supervisor_mobile=$line_id=$area_id=$password=$user_name="";
if (!empty($getSingleSupervisor)) {
	$supervisor_id 		= $getSingleSupervisor->supervisor_id;
	$emp_id        		= $getSingleSupervisor->emp_id;
	$supervisor_name 	= $getSingleSupervisor->supervisor_name;
	$supervisor_email   = $getSingleSupervisor->supervisor_email;
	$supervisor_mobile  = $getSingleSupervisor->supervisor_mobile;
	$line_id          	= explode("##",$getSingleSupervisor->line_id);
	$area_id          	= $getSingleSupervisor->area_id;
	$password          	= $this->Collective->decode($getSingleSupervisor->password);
	$user_name          = $getSingleSupervisor->user_name;
	$mode="Edit";
}
?>
<div class="page-content">
	<div class="portlet box blue boardergrey">
		<div class="portlet-title">
			<div class="caption">
				<img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/supervisor.png" class="imgbasline"> <?php echo $mode ?> Supervisor
			</div>
		</div>
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form name="frm_supervisor" id="frm_supervisor" action="" class="horizontal-form" method="POST">
				<input type="hidden" name="hndid" value="<?php echo $supervisor_id?>">
				<div class="form-body">
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Emp ID</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="emp_id" id="emp_id" placeholder="Emp ID" value="<?php echo $emp_id ?>">
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Name</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $supervisor_name ?>">
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Mobile</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" value="<?php echo $supervisor_mobile ?>">
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Email id</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email_id" id="email_id" placeholder="Email id" value="<?php echo $supervisor_email ?>">
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Area</label>
								<div class="col-md-8">
									<select class="form-control" name="area_id" id="area_id">
										<option value="">Select Area</option>
										<?php
											foreach ($getActiveArea as $areaData) {
												$sel='';
												if($area_id==$areaData->area_id){
													$sel="selected";
												}
												echo "<option value=".$areaData->area_id." ".$sel.">".$areaData->area_name."</option>";
											}
										?>
									</select>
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Line</label>
								<div class="col-md-8">
									<select name="line_id[]" id="assembly" class="mt-multiselect btn btn-default" multiple="multiple" data-label="left" data-select-all="true" data-width="100%" data-filter="true" data-action-onchange="true">
										<?php
                                        foreach ($getActiveLine as $lineData) {
                                        	    $selected='';
												if(in_array($lineData->line_id,$line_id)){
													$selected="selected";
												}
												echo "<option value=".$lineData->line_id." ".$selected.">".$lineData->line_name."</option>";
											}
										?>
                                    </select>
                                    <label id="assembly-error" class="error" for="assembly" style="display:none;">Please select line</label>
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Username</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $user_name ?>">
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Password</label>
								<div class="col-md-8">
									<input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password?>">
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Confirm Password</label>
								<div class="col-md-8">
									<input type="password" class="form-control" name="conpassword" id="conpassword" placeholder="Confirm Password" value="<?php echo $password?>">
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Role</label>
								<div class="col-md-8">
									<?php
										foreach($getRoleLine as $getRoleLine) {
											if(strtolower($getRoleLine->role_name)=="supervisor") {
									?>
									<input type="text" class="form-control" name="usertype" id="usertype" placeholder="Role type" value="<?php echo $getRoleLine->role_name ?>" readonly>
									<?php
									  }
								    }
									?>
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
				</div>
				<div class="form-actions formbtncenter">
					<button type="submit" class="btn green customsavebtn">
						<i class="fa fa-check"></i> Save
					</button>
					<a href="<?php echo base_url() ?>admin/supervisor"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>