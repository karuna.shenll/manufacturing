<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/employee.png" class="imgbasline"> Edit Employee
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_employee" id="frm_employee" action="<?php echo base_url() ?>admin/employee" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Emp ID</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_id" id="emp_id" placeholder="Emp ID" value="12345">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="Ram">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Mobile</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" value="9393993999">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Email id</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="email_id" id="email_id" placeholder="Email id" value="ram@gmail.com">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Area</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="area" id="area" placeholder="Area" value="F1">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Line</label>
                                <div class="col-md-8">
                                    <select name="assembly" id="assembly" class="mt-multiselect btn btn-default" multiple="multiple" data-label="left" data-select-all="true" data-width="100%" data-filter="true" data-action-onchange="true">
                                        <option value="Audio Cat" selected="selected">Audio Cat</option>
                                        <option value="Cluster Eol">Cluster Eol</option>
                                        <option value="Cluster Cat">Cluster Cat</option>
                                        <option value="Pointer Press" selected="selected">Pointer Press</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Skill</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="skill" id="skill" placeholder="Skill" value="skill">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Username</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="ramename">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                             <div class="form-group">
                                <label class="control-label col-md-4">Password</label>
                                <div class="col-md-8">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="123456">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>admin/employee"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>