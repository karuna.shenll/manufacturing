<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/assembly.png" class="imgbasline"> Edit Line
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_client" id="frm_client" action="<?php echo base_url() ?>admin/assembly" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Line Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="assembly_name" id="assembly_name" placeholder="Line Name" value="B515 LOC/ULCR">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Doc No</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="doc_no" id="doc_no" placeholder="Doc No" value="FA_MC_108">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Area</label>
                                <div class="col-md-8">
                                   <select id='area' name='area' class="form-control">
                                        <option  value="">Select Area</option>
                                        <option  value="FA" selected>FA</option>
                                        <option  value="FB">FB</option>
                                        <option  value="FC">FC</option>
                                        <option  value="FD">FD</option>
                                        <option  value="FE">FE</option>
                                   </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Revision Add</label>
                                <div class="col-md-8">
                                    <select name="sel_revision" id="sel_revision" class="form-control">
                                        <option  value="">Select Revision</option>
                                        <option  value="1300" selected>BA Stepper Motor Press</option>
                                        <option  value="1301">BA Dialplate Assy</option>
                                        <option  value="1302">BA Mask & Lens Assy </option>
                                        <option  value="1303">BA Pointer Press</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Eqiupment</label>
                                <div class="col-md-7">
                                    <select name="sel_eqiupment" id="sel_eqiupment" class="form-control">
                                        <option value="">Select Eqiupment</option>
                                        <option value="BA Stepper Motor Press">BA Stepper Motor Press</option>
                                        <option value="BA Dialplate Assy">BA Dialplate Assy</option>
                                        <option value="BA Mask & Lens Assy">BA Mask & Lens Assy </option>
                                        <option value="BA Pointer Press" selected="selected">BA Pointer Press</option>
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-primary customaddmorebtn" name="addmore" id="addmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="appendContent">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            
                        </div>
                        <!--/span-->
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>admin/assembly"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>