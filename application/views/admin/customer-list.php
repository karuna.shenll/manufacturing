<?php
    $search_name = $search_email=$search_mobile= $search_line=$search_products='';
    if (!empty($search)) {
		$search_name    = $search["customer_name"];
		$search_email   = $search["email"];
		$search_mobile  = $search["mobile"];
		$search_line    = $search["line_id"];
		$search_products= $search["products"];
	}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <?php
    $msgs=$this->session->flashdata('message_failure');
    if(!empty($msgs)) {
    ?>
        <div class="alert alert-danger alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msgs ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/client.png" class="imgbasline"> Customer List</div>
            <div class="actions">
                <a href="customer/addcustomer" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Customer</a>
            </div>
        </div>
        <div class="portlet-body">
        	<form id="frm_cuslist" id="" method="POST">
	        	<div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="customer[customer_name]" id="customer_name" placeholder="Customer Name" value="<?php echo $search_name ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="customer[email]" id="email" placeholder="Email" value="<?php echo $search_email ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="customer[mobile]" id="mobile" placeholder="Mobile" value="<?php echo $search_mobile ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <select name="customer[line_id]" id="line_id" class="form-control">
	                            	<option value="">Select Line</option>
	                            	<?php
	                            	foreach($getActiveLine as $lineData) {
	                            		$sel="";
	                            		if($search_line==$lineData->line_id){
	                            			$sel="selected";
	                            		}
	                                    echo "<option value=".$lineData->line_id." ".$sel.">".$lineData->line_name."</option>";
	                                }
	                                ?>
	                            </select>
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                          <input type="text" class="form-control" name="customer[products]" id="products" placeholder="Products" value="<?php echo $search_products ?>">
	                        </div>
		        		</div>
		        		
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."admin/customer"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Customer Name</th>
		                        <th>Email</th>
		                        <th>Mobile</th>
		                        <th>Products</th>
		                        <th>Line</th>
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                    $sno=1;
                            if(!empty($getCustomerList)) {
                                foreach($getCustomerList as $customerdata) {
		                    ?>
		                    <tr>
		                        <td><?php echo $sno ?></td>
		                        <td><?php echo $customerdata->customer_name ?></td>
		                        <td><?php echo $customerdata->email ?></td>
		                        <td><?php echo $customerdata->mobile ?></td>
		                        <td><?php echo $customerdata->products ?></td>
		                        <td><?php echo $customerdata->line_name ?></td>
		                        <td> <a href="customer/addcustomer/<?php echo $customerdata->cus_id ?>" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn deleteRowid" data-id="<?php echo $customerdata->cus_id ?>" data-type="customer" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
		                    </tr>
		                    <?php
		                        $sno++;
		                		}
		                	}      
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>