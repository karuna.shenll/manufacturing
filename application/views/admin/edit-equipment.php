<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/equipment.png" class="imgbasline"> Edit Equipment
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_equipment" id="frm_equipment" action="<?php echo base_url() ?>admin/equipment" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Equipment Code</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="equipment_code" id="equipment_code" placeholder="Equipment Code" value="1300">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Equipment Model</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="equipment_model" id="equipment_model" placeholder="Equipment Model" value="EMO001">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Equipment Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="equipment_name" id="equipment_name" placeholder="Equipment Name" value="BA Stepper Motor Press">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Group</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="category" id="category">
                                        <option value="">Select Group</option>
                                        <option value="1" selected>PM Stepping Motors</option>
                                        <option value="2">Brushless DC Motors</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>admin/equipment"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>