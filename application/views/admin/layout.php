<?php
$this->load->helper('common_helper');
?>
<style type="text/css">
    .table-bordered, .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
        border: 1px solid #000;
        font-weight: bold;
    }
    .bordernone{
      border: 0px !important;
      background-color: #fff !important;
    }
    .borderright{
      border-right: 0px;
    }
    .borderbottom{
      border-bottom: 0px;
    }
    .borderleft{
      border-left: 0px;
    }
    .bordertop{
      border-top: 0px;
    }
    .table-bordered{
        border: 0;
    }
    .table-striped>tbody>tr{
        background-color: none !important; 
    }  
    .boxgreen{
      background-color:#92D050;
      border: 1px solid #000 !important;
    }
    .boxyellow{
      background-color:#FFFF00;
      border: 1px solid #000 !important;
    }
</style>

<div class="page-content"  id="dashboard">
    <?php
    $msg=$this->session->flashdata('success');
    if(!empty($msg)){
    ?>
    <div>
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo $msg ?>
        </div>
    </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/dashboard.png" class="imgbasline"> Layout</div>
            <div class="actions">
               <a href="<?php echo base_url(); ?>admin/dashboard" class="btn red btn-sm customrestbtn"><i class="fa fa-chevron-left"></i> Back</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
               <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                    <table class="table table-striped table-bordered" id="adminlayout1" style="width: 1000px" align="center">
                       <!--- First <tr> row --> 
                        <tr>
                            <td class="bordernone"></td>
                            <td class="bordernone" style="width:15px;"></td>
                            <?php
                                $total_count="";
                                $odd_count=count($equp_task["odd_names"]);
                                $even_count=count($equp_task["even_names"]);

                                if($odd_count>$even_count)
                                    $total_count=$odd_count*2-3;
                                else
                                $total_count=$even_count*2-1;   


                                $other_task_name = $other_task_log_id = $other_task_status = "";
                                $First_task_name = $First_task_log_id = $First_task_status= "";
                            
                                $task_status=$equp_task['task_status'];
                                $equ_id=$equp_task['equ_id'];
                              
                                if(count($equp_task["odd_names"]>0)) {
                                    $lastElOdd = array_values(array_slice($equp_task["odd_names"], -1))[0];
                                    // print_r($lastElOdd);
                                    foreach ($equp_task["odd_names"] as $key => $odd_names) {

                                        $color=get_color($task_status,$odd_names['log_status']);

                                        if($key==0) {
                                            $First_task_name = $odd_names['task_name'];
                                            $First_task_log_id = $odd_names['task_log_id'];
                                            $First_task_status = $odd_names['log_status'];
                                            $first_color=get_color($task_status,$First_task_status);
                                        } else {
                                            $other_task_name = $odd_names['task_name'];
                                            $other_task_log_id = $odd_names['task_log_id'];
                                            $other_task_status = $odd_names['log_status'];
                                        }

                                        if($other_task_log_id!="") { ?>
                                            <td class="text-center oddborder" style="vertical-align:middle; background-color: <?php echo $color; ?>"><a class="dashboardatag" href="<?php echo site_url('admin/dashboard/layouttask/'.$other_task_log_id); ?>"><?php echo $other_task_name; ?></a></td>
                                            <?php if ($other_task_log_id!=$lastElOdd['task_log_id']) { ?>
                                                <td class="bordernone" style="width:15px;"></td>
                                            <?php }
                                        }
                                    }
                                }
                            ?>
                        </tr>
                        <!--- Second <tr> row --> 
                        <tr>
                            <?php for ($i=0; $i < $total_count+2; $i++) { ?>
                                <td class="bordernone" style="width:15px;height:15px;"></td>
                            <?php }  ?>
                        </tr>

                        <!--- third <tr> row --> 

                        <tr>
                            <td class="text-center boxgreen oddborder" style="vertical-align:middle;  background-color: <?php echo $first_color; ?>"><a class="dashboardatag" href="<?php echo site_url('admin/dashboard/layouttask/'.$First_task_log_id); ?>"><?php if(count($equp_task["odd_names"]>0)) { echo $First_task_name; } ?></a></td>

                            <td class="bordernone"></td>

                            <td colspan="<?php echo $total_count; ?>" class="text-center" style="vertical-align:middle;background-color: #FF99CC;"><a href="<?php echo site_url('admin/dashboard/viewreport/'.$equ_id); ?>" class="dashboardatag"><b><?php echo $equp_task["equp_name"]; ?></b></a></td>
                        </tr>
                        <!--- Fourth <tr> row --> 

                        <tr>
                            <?php for ($i=0; $i < $total_count+2; $i++) { ?>
                                <td class="bordernone" style="width:15px;height:15px;"></td>
                            <?php }  ?>
                        </tr>
                        <!--- Fifth <tr> row --> 
                        <tr>
                            <td class="bordernone"></td>

                            <td class="bordernone" style="width:15px;height:15px;"></td>

                            <?php $lastElEven = array_values(array_slice($equp_task["even_names"], -1))[0];
                            foreach ($equp_task["even_names"] as $even_names) {
                                $color=get_color($task_status,$even_names['log_status']);
                                ?>
                                <td class="text-center boxyellow evenborder" style="vertical-align:middle;  background-color: <?php echo $color; ?>"><a class="dashboardatag" href="<?php echo site_url('admin/dashboard/layouttask/'.$even_names['task_log_id']); ?>"> <?php echo $even_names['task_name']; ?></a></td>
                                <?php if ($even_names['task_log_id']!=$lastElEven['task_log_id']) { ?>
                                    <td class="bordernone" style="width:15px;"></td>
                                <?php }
                            }  ?>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


   <!--   <?php 
                    if (count($equp_task)>0) {
                        foreach ($equp_task as $key => $val) { ?>
                            <div class="col-md-6">
                                <?php foreach ($val['task'] as $key => $value2) { 
                                $task_status = $value2['task_status']; 
                                }?>
                                <?php if(count($val['subtask'])>0){ ?>
                                <div class="col-md-12" style="margin-top: 10px; ">
                                   <div class="col-md-12" align="center" style="border:1px solid; padding: 20px; background-color: #FF99CC;"><b><?php echo $val['equ_name']; ?></b></div>
                                </div>   
                                <?php } ?>
                            </div>
                        <?php }  } 
                    ?>

                    <?php 
                        if (count($equp_task)>0) {
                            foreach ($equp_task as $key => $val) { ?>
                                <div class="col-md-6">
                                    <a href="<?php echo site_url('admin/dashboard/'); ?>" class="dashboardatag">
                                    <?php foreach ($val['task'] as $key => $value2) { 
                                    $task_status = $value2['task_status']; 
                                    }?>
                                    <?php if(count($val['subtask'])>0){ ?>
                                    <?php foreach ($val['subtask'] as $key => $value) {
                                    ?>
                                    <div class="col-md-12">
                                       <div class="col-md-12" style="border:1px solid; padding: 20px; margin-top: 10px; background-color: #bffc9f;" align="center"><b><?php echo $value['task_name']; ?></b> </div>
                                    </div>
                                    <?php } ?> 
                                    </a>
                                    </div>
                                <?php } } } ?> -->