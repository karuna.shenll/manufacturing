<?php
	$search_name = $search_doc=$search_area='';
    if (!empty($search)) {
        $search_name  = $search['line_name'];
        $search_doc   = $search['doc_no']; 
        $search_area  = $search['area_id']; 
    }
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <?php
    $msgs=$this->session->flashdata('message_failure');
    if(!empty($msgs)) {
    ?>
        <div class="alert alert-danger alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msgs ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/assembly.png" class="imgbasline"> Line List</div>
            <div class="actions">
                <a href="line/addline" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Line</a>
            </div>
        </div>
        <div class="portlet-body">
        	<form name="frm_linelist" id="frm_linelist" method="POST">
	        	<div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="line[line_name]" id="line_name" placeholder="Line Name" value="<?php echo $search_name ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="line[doc_no]" id="doc_no" placeholder="Doc No" value="<?php echo $search_doc ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <select class="form-control" name="line[area_id]" id="area_id">
	                           		<option value="">select Area</option>
		                           	<?php 
			                           	foreach ($getArrArea as $areaData ) {
			                           		$sel='';
			                           		if ($areaData->area_id==$search_area) {
			                                    $sel="selected";
			                           		}
			                           		echo "<option value=".$areaData->area_id." ".$sel.">".$areaData->area_name."</option>";

			                           	}
		                           	?>
	                           </select>
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."admin/line"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Line Name</th>
		                        <th>Doc No</th>
		                        <th>Area</th>
		                        <!-- <th>Eqiupment</th> -->
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                        if (!empty($getLineList)) {
		                        	$sno=1;
		                        	foreach ($getLineList as $lineListData) {
		                    ?>
			                    <tr>
			                        <td><?php echo $sno ?></td>
			                        <td><?php echo $lineListData->line_name?></td>
			                        <td><?php echo $lineListData->doc_no?></td>
			                        <td><?php echo $lineListData->area_name?></td>
			                        <td> 
			                        	<a href="line/addline/<?php echo  $lineListData->line_id ?>" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn deleteRowid" data-id="<?php echo  $lineListData->line_id ?>" data-type="line" title="Delete"><i class="fa fa-trash"></i> Delete</a>
			                        	<a href="javascript:void(0);" class="btn btn-info btn-xs customviewbtn viewPopupequ" data-popup="<?php echo  $lineListData->line_id ?>"><i class="fa fa-eye"></i> View</a>
			                        </td>
			                    </tr>
		                    <?php
		                            $sno++;
									}
								}
		                    ?>
		                </tbody>
		            </table>
		        </div>
	        </div>
	    </div>
	</form>
</div>
<div id="viewLineModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
		    <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title"><b>View Line</b></h4>
		    </div>
		    <div class="modal-body">
		        <div id="appendLinedetail">
		        </div>
		    </div>
		    <div class="modal-footer">
		        <button type="button" class="btn red customrestbtn" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
		    </div>
	    </div>
    </div>
</div>