<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/task.png" class="imgbasline"> Edit Task
            </div>
        </div>
        <div class="portlet-body">
            <!-- BEGIN FORM-->
            <form name="frm_task" id="frm_task" action="<?php echo base_url() ?>admin/task" class="form-horizontal" method="POST">
                <div class="form-group margin-top-20">
                    <label class="control-label col-md-3">Equipment Code
                        <span class="required" aria-required="true"></span>
                    </label>
                    <div class="col-md-5"> 
                        <input type="text" class="form-control" name="equipment_code" id="equipment_code" placeholder="Equipment Code" value="EM001">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Equipment Model
                        <span class="required" aria-required="true"></span>
                    </label>
                    <div class="col-md-5"> 
                        <input type="text" class="form-control" name="equipment_model" id="equipment_model" placeholder="Equipment Model" value="MO001">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Equipment Name
                        <span class="required" aria-required="true"></span>
                    </label>
                    <div class="col-md-5"> 
                        <input type="text" class="form-control" name="equipment_name" id="equipment_name" placeholder="Equipment Name" value="BA Dialplate Assy">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Add Task (Position)
                        <span class="required" aria-required="true"></span>
                    </label>
                    <div class="col-md-5"> 
                        <input type="text" class="form-control" name="add_task" id="add_task" placeholder="Add Task (Position)" value="All pneumatic fitting should be in good  condition.">
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="btn btn-primary customaddmorebtn" name="addtaskmore" id="addtaskmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                    </div>
                </div>
                <div id="appendTaskContent">
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>admin/task"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>