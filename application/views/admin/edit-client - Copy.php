<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/client.png" class="imgbasline"> Edit Customer
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_client" id="frm_client" action="<?php echo base_url() ?>admin/client" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Customer Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="client_name" id="client_name" placeholder="Customer Name" value="Customer 1">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Mobile</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" value="930039399">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Email</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="Customer1@gmail.com">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Address</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" name="address" id="address">test address</textarea> 
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Products </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="components " id="components " placeholder="Products" value="test products">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Line</label>
                                <div class="col-md-8">
                                    <select name="sel_assembly" id="sel_assembly" class="form-control">
                                        <option value="">Select Line</option>
                                        <option value="B515 LOC/ULCR">B515 LOC/ULCR</option>
                                        <option value="B515/HA ">B515/HA</option>
                                        <option value="B562">B562</option>
                                        <option value="VW/SKODA/S101 " selected="selected">VW/SKODA/S101 </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>admin/client"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>