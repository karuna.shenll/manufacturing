<?php
    $mode="Add";
    $task_id=$equ_id=$equ_code=$equ_model=$first_task=$log_tasks=$tasks=$first_task_id="";
    if (!empty($getSingleTask)) {
        $task_id    = $getSingleTask->task_id;
        $equ_id     = $getSingleTask->equ_id;
        $equ_code   = $getSingleTask->equ_code;
        $equ_model  = $getSingleTask->equ_model;
        $mode="Edit";
    }
    if(!empty($getLogtask)){
        foreach($getLogtask as $logtask) {
            $tasks[]       = $logtask->task_name;
            $first_task    = $tasks["0"];
            $log_status[]  = $logtask->log_status;
            $ArrLogids[]   = $logtask->task_log_id;
            $first_task_id = $ArrLogids["0"];
        }
    }
    $task_count=count($tasks);
?>
<div class="page-content">
    <?php
    $msg=$this->session->flashdata('message_failure');
    if(!empty($msg)) {
    ?>
        <div class="alert alert-danger alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/task.png" class="imgbasline"> <?php echo $mode ?> Task
            </div>
        </div>
        <div class="portlet-body">
            <!-- BEGIN FORM-->
            <form name="frm_task" id="frm_task" action="" class="form-horizontal" method="POST">
                <input type="hidden" name="hndid" value="<?php echo $task_id; ?>">
                <div class="form-group margin-top-20">
                    <label class="control-label col-md-3">Equipment Name
                        <span class="required" aria-required="true"></span>
                    </label>
                    <div class="col-md-5"> 
                        <select class="form-control" name="equipment_name" id="equipment_name">
                            <option value="">Select Equipment Name</option>
                        <?php
                            foreach ($getActiveEquipment as $equipmentName) {
                                $sel ='';
                                if ($equ_id == $equipmentName->equ_id) {
                                    $sel ="selected";
                                }
                                echo "<option value=".$equipmentName->equ_id." ".$sel.">".$equipmentName->equ_name."</option>";
                            }
                        ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Equipment Code
                        <span class="required" aria-required="true"></span>
                    </label>
                    <div class="col-md-5"> 
                        <input type="text" class="form-control" name="equipment_code" id="equipment_code" placeholder="Equipment Code" value="<?php echo $equ_code?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Equipment Model
                        <span class="required" aria-required="true"></span>
                    </label>
                    <div class="col-md-5"> 
                        <input type="text" class="form-control" name="equipment_model" id="equipment_model" placeholder="Equipment Model" value="<?php echo $equ_model?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Add Task (Position)
                        <span class="required" aria-required="true"></span>
                    </label>
                    <div class="col-md-5"> 
                        <input type="text" class="form-control" name="add_task[]" id="add_task" placeholder="Add Task (Position)" value="<?php echo $first_task ?>">
                        <input type="hidden" class="form-control" name="hidden_log[]" id="hidden_log"  value="<?php echo $first_task_id ?>">
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="btn btn-primary customaddmorebtn" name="addtaskmore" id="addtaskmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                    </div>
                </div>
                <div id="appendTaskContent">
                    <?php
                        if(!empty($task_id)) {
                            if(!empty($tasks)) {
                                $count=(count($tasks)-1);
                                for($k=0;$k<=$count;$k++) {
                                    if($k!=0) {
                                    $log_tasks = $tasks[$k];
                                    $status    = $log_status[$k];
                                    $taskLogid    = $ArrLogids[$k];
                                    

                                    if (!empty($status)) {
                                       $btn="disabled";
                                    } else {
                                        $btn="";
                                    }
                        ?>
                            <div class="col-md-12 col-sm-12 col-xs-12 paddingleftright">
                                <div class="form-group">
                                    <label class="control-label col-md-3"><span class="required" aria-required="true"></span></label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="add_task[]" id="add_task_<?php echo $k?>" placeholder="Add Task (Position)" value="<?php echo $log_tasks?>">
                                        <input type="hidden" class="form-control" name="hidden_log[]" id="hidden_log"  value="<?php echo $taskLogid ?>">
                                    </div>
                                    <div class="col-md-1">
                                        <button type="button" class="btn btn-danger customaddmorebtn removetasklog" name="removetask" id="removetasklog" data-remove="<?php echo $taskLogid ?>" data-type="removeadd" <?php echo $btn?>><span class="glyphicon glyphicon-remove" aria-hidden="true" ></span></button>
                                    </div>
                                </div>
                            </div>
                    <?php    
                               }
                            }
                        }
                    }
                    ?>
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>admin/task"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>