<?php
	$search_name = $search_code = $search_model=$condition = '';
    if (!empty($search)) {
        $search_code  = $search["equ_code"];
        $search_model = $search["equ_model"];
        $search_name  = $search["equ_name"];
    }
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <?php
    $msgs=$this->session->flashdata('message_failure');
    if(!empty($msgs)) {
    ?>
        <div class="alert alert-danger alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msgs ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/task.png" class="imgbasline"> Task List</div>
            <div class="actions">
                <a href="task/addtask" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Task</a>
            </div>
        </div>
        <div class="portlet-body">
        	<form name="frm_tasklist" id="frm_tasklist" action="" method="POST">
	        	<div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="task[equ_code]" id="equ_code" placeholder="Equipment Code" value="<?php echo $search_code?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="task[equ_model]" id="equ_model" placeholder="Equipment Model" value="<?php echo $search_model?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="task[equ_name]" id="equ_name" placeholder="Equipment Name" value="<?php echo $search_name?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."admin/task"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Equipment Code</th>
		                        <th>Equipment Model</th>
		                        <th>Equipment Name</th>
		                        <th>Status</th>
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                        $sno = 1;
		                    	if (!empty($getTaskList)) {
		                    		foreach ($getTaskList as $taskData) {
		                    			$status = $taskData->task_status;
		                    			if (strtolower($status)=="open") {
                                        	$label="success";
		                    			}
		                    			if (strtolower($status)=="expired") {
                                        	$label="danger";
		                    			}
		                    			if (strtolower($status)=="assigned") {
                                        	$label="info";
		                    			}
		                    			if (strtolower($status)=="inprogress") {
                                        	$label="warning";
		                    			}
		                    			if (strtolower($status)=="completed") {
                                        	$label="completed";
		                    			}
		                    		
		                    ?>
		                    <tr>
		                        <td><?php echo $sno ?></td>
		                        <td><?php echo $taskData->equ_code ?></td>
		                        <td><?php echo $taskData->equ_model ?></td>
		                        <td><?php echo $taskData->equ_name ?></td>
		                        <td><span class="label label-sm label-<?php echo $label?> labelradius" style="padding: 1px 13px;"><?php echo $taskData->task_status ?></span></td>
		                        <td> <a href="task/addtask/<?php echo $taskData->task_id ?>" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn deleteRowid"  data-id="<?php echo $taskData->task_id ?>" data-type="task" title="Delete"><i class="fa fa-trash"></i> Delete</a><a href="task/viewtask/<?php echo $taskData->task_id ?>" class="btn btn-info btn-xs customviewbtn"><i class="fa fa-eye"></i> View</a></td>
		                    </tr>
		                    <?php
		                        $sno++;
		                    		}
		                    	}
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>