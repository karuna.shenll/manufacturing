<?php
	$search_code = $search_model = $search_name = $search_group='';
    if (!empty($search)) {
        $search_code      = $search['equ_code'];
        $search_model     = $search['equ_model'];
        $search_name      = $search['equ_name'];
        $search_group     = $search['group'];
         
    }
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <?php
    $msgs=$this->session->flashdata('message_failure');
    if(!empty($msgs)) {
    ?>
        <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msgs ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/equipment.png" class="imgbasline"> Equipment List</div>
            <div class="actions">
                <a href="equipment/addequipment" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Equipment</a>
            </div>
        </div>
        <div class="portlet-body">
        	<form name="frm_equipmentlist" id="frm_equipmentlist" method="POST" action="">
	        	<div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="equipment[equ_code]" id="equ_code" placeholder="Equipment Code" value="<?php echo $search_code?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="equipment[equ_model]" id="equ_model" placeholder="Equipment Model" value="<?php echo $search_model?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="equipment[equ_name]" id="equ_name" placeholder="Equipment Name" value="<?php echo $search_name?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <select class="form-control" name="equipment[group]" id="group">
									<option value="">Select Group</option>
									<?php
									    
										foreach($getArrGroup as $groupData) {
											$selected = "";
											if ($groupData->group_id == $search_group) {
												$selected = "selected";
											}
	                                		echo "<option value=".$groupData->group_id." ".$selected.">".$groupData->group_name."</option>";
										}
									?>
								</select>
	                        </div>
		        		</div>
		        		<div class="col-md-12 text-center">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."admin/equipment"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Equipment Code</th>
		                        <th>Equipment Model</th>
		                        <th>Equipment Name</th>
		                        <th>Group</th>
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                    	$sno=1;
		                    	if (!empty($getEquipmentList)) {
		                    		foreach ($getEquipmentList as $equipmentData) {	
		                    ?>
		                    		<tr>
				                        <td><?php echo $sno ?></td>
				                        <td><?php echo $equipmentData->equ_code ?></td>
				                        <td><?php echo $equipmentData->equ_model ?></td>
				                        <td><?php echo $equipmentData->equ_name ?></td>
				                        <td><?php echo $equipmentData->group_name ?></td>
				                        <td> <a href="equipment/addequipment/<?php echo $equipmentData->equ_id ?>" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn deleteRowid" data-id="<?php echo $equipmentData->equ_id ?>" data-type="equipment" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
					                </tr>
		                    <?php
		                    		$sno++;
		                    		}
		                    	}
		                    ?>
		                    <!-- <tr>
		                        <td> 1 </td>
		                        <td>1300</td>
		                        <td>EM001</td>
		                        <td>BA Stepper Motor Press</td>
		                        <td>Brushless DC Motors </td>
		                        <td> <a href="equipment/editequipment" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
		                    </tr> -->
		                   <!--  <tr>
		                        <td> 2 </td>
		                        <td>1310</td>
		                        <td>EM002</td>
		                        <td>BA Dialplate Assy</td>
		                        <td>PM Stepping Motors </td>
		                        <td> <a href="equipment/editequipment" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
		                    </tr>
		                    <tr>
		                        <td> 3 </td>
		                        <td>1320</td>
		                        <td>EM003</td>
		                        <td>BA Mask & Lens Assy </td>
		                        <td>Brushless DC Motors</td>
		                        <td> <a href="equipment/editequipment" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
		                    </tr>
		                    <tr>
		                        <td> 4 </td>
		                        <td>1330</td>
		                        <td>EM004</td>
		                        <td>BA Pointer Press</td>
		                        <td>PM Stepping Motors</td>
		                        <td> <a href="equipment/editequipment" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
		                    </tr>
		                    <tr>
		                        <td> 5 </td>
		                        <td>1340</td>
		                        <td>EM005</td>
		                        <td>BA Final Assy</td>
		                        <td>Brushless DC Motors</td>
		                        <td> <a href="equipment/editequipment" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
		                    </tr> -->
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>