<?php
$area_id = $area_name= $status= '';
$mode="Add";
if (!empty($getSingleArea)) {
   $area_id   = $getSingleArea->area_id;
   $area_name = $getSingleArea->area_name;
   $status    = $getSingleArea->status;
   $mode="Edit";
}
?>
<div class="page-content">
	<div class="portlet box blue boardergrey">
		<div class="portlet-title">
			<div class="caption">
				<img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/area.png" class="imgbasline"> <?php echo $mode ?> Area
			</div>
		</div>
		<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<form name="frm_area" id="frm_area" action="" class="horizontal-form" method="POST">
				<input type="hidden" name="hndid" value="<?php echo $area_id ?>">
				<div class="form-body">
					<div class="row">
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Area</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="area" id="area" placeholder="Area" value="<?php echo $area_name ?>">
								</div>
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6 paddingbottom">
							<div class="form-group">
								<label class="control-label col-md-4">Status</label>
								<div class="col-md-8">
                                    <div class="input-group">
                                        <div class="icheck-inline">
                                            <label class="">
                                                <div class="iradio_flat-green" style="position: relative;"><input type="radio" name="status" class="icheck" data-radio="iradio_flat-green" value="1" <?php echo ($status=="1")?"checked":""; ?> checked><ins class="iCheck-helper"></ins></div> Active 
                                            </label>
                                            <label class="">
                                                <div class="iradio_flat-red " style="position: relative;"><input type="radio" name="status" class="icheck" data-radio="iradio_flat-red" value="2"  <?php echo ($status=="2")?"checked":""; ?> ><ins class="iCheck-helper"></ins></div> Inactive  
                                            </label>
                                        </div>
                                    </div>
                                    <label id="status-error" class="error" for="status" style="display:none">Please select status</label>
								</div>
							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
				</div>
				<div class="form-actions formbtncenter">
					<button type="submit" class="btn green customsavebtn">
						<i class="fa fa-check"></i> Save
					</button>
					<a href="<?php echo base_url() ?>admin/area"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>