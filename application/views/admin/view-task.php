<?php
if (count($getLogtask["taskdetails"])>0) {
    $task_id        =   $getLogtask["taskdetails"]["task_id"];
    $equ_code       =   $getLogtask["taskdetails"]["equ_code"];
    $equ_model      =   $getLogtask["taskdetails"]["equ_model"];
    $equ_name       =   $getLogtask["taskdetails"]["equ_name"];
    $frequency      =   $getLogtask["taskdetails"]["frequency"];
    $created_date   =   $getLogtask["taskdetails"]["created_date"];
    $doc_no         =   $getLogtask["taskdetails"]["doc_no"];
    $revision_add   =   $getLogtask["taskdetails"]["revision_add"];
    $revision_date  =    date("d/m/Y",strtotime($getLogtask["taskdetails"]["revision_date"]));
}
 $revision_level= $verified_by ='';
if (count($getLogtask["logtaskdetails"])>0) {
    $done_by = $getLogtask["logtaskdetails"][0]["done_by"];
    $verified_by = $getLogtask["logtaskdetails"][0]["verified_by"];
    $completed_date = $getLogtask["logtaskdetails"][0]["completed_date"];
    if ($completed_date=="0000-00-00" || $completed_date=="" ){
        $completed_date ="";
    } else{
        $completed_date = date("d/m/Y",strtotime($getLogtask["logtaskdetails"][0]["completed_date"]));
    }
}
?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tasklist.png" class="imgbasline"> View Task
            </div>
            <div class="actions">
                <a href="<?php echo base_url() ?>admin/task"  class="btn red customrestbtn"> <i class="fa fa-chevron-left"></i> Back</a>
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_task" id="frm_task" action="<?php echo base_url() ?>employee/task" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <h3 class="emptaskhead">PM ADHERENCE REPORT</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                            <table class="table table-striped table-bordered table-hover" id="employeesheet">
                                <thead>
                                    <tr>
                                        <th nowrap>Machine Name :</th>
                                        <th><?php echo $equ_name ?></th>
                                        <th></th>
                                        <th></th>
                                        <th>Month : <?php echo date("F"); ?></th>
                                    </tr>
                                    <tr>
                                        <th>Frequency :</th>
                                        <th><?php echo $frequency ?></th>
                                        <th></th>
                                        <th>Machine ID : <?php echo $equ_model ?></th>
                                        <th>Year : <?php echo date("Y"); ?></th>
                                    </tr>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Activcity Description</th>
                                        <th>Observation</th>
                                        <th>Action Taken</th>
                                        <th>Comments</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($getLogtask["logtaskdetails"])>0) {
                                        $sno=1;
                                        foreach ($getLogtask["logtaskdetails"] as $addtaskdetails) {
                                    ?>
                                    <tr>
                                        <td><?php echo $sno ?></td>
                                        <td>
                                            <?php echo $addtaskdetails["task_name"] ?>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="observation" id="observation" placeholder="Observation" value="<?php echo $addtaskdetails["observation"] ?>" disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="action_taken" id="action_taken" placeholder="Action Taken" value="<?php echo $addtaskdetails["action_taken"] ?>" disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="comments" id="comments" placeholder="Comments" value="<?php echo $addtaskdetails["comments"] ?>" disabled>
                                        </td>
                                    </tr>
                                    <?php
                                        $sno++;
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <td></td><td><b>Done by : <input type="text"  name="done_by" id="done_by" placeholder="Done by" value="<?php echo $done_by?>" style="width:200px;height:32px" disabled></b></td><td></td><td></td><td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td><b>Date : &nbsp;&nbsp; &nbsp; &nbsp;<input type="text"  name="date" id="date" placeholder="Date" value="<?php echo $completed_date?>" disabled style="width:200px;height:32px"></b></td><td></td><td><b>Verified by :</td><td> <input type="text"  name="verified_by" id="verified_by" placeholder="Verified by" value="<?php echo $verified_by?>" class="form-control" disabled></b></td>
                                    </tr>
                                    <tr>
                                        <td></td><td>DOCUMENT NO : <?php echo $doc_no ?></td><td></td><td></td><td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td>REVISION LEVEL : <input type="text"  name="date" id="date" placeholder="REVISION LEVEL" value="<?php echo $revision_add?>" disabled style="width:200px;height:32px"></td><td></td><td></td><td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td>REVISION DATE : <input type="text"  name="date" id="date" placeholder="REVISION DATE" value="<?php echo $revision_date?>" disabled style="width:200px;height:32px"></td><td></td><td></td><td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-actions text-right">
                    <a href="<?php echo base_url() ?>admin/task"  class="btn red customrestbtn"> <i class="fa fa-chevron-left"></i> Back</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>