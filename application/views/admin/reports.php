<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/report.png" class="imgbasline">Reports</div>
            <div class="actions">
                <!-- <a href="assembly/addassembly" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Assembly</a> -->
            </div>
        </div>
        <div class="portlet-body">
        	<div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <select  class="form-control" name="admin[select_report]" id="select_report">
                           		<option value="">Select Report</option>
                            	<option value="Cluster1">Cluster1</option>
                           		<option value="Cluster2">Cluster2</option>
                           </select>
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn searchreport"> <i class="fa fa-search"></i> Search</button>
	        				<a href="<?php echo base_url()."admin/assembly/reports"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: auto;margin-top:0px;"><!--suppliertbl-->
	            <table class="table table-striped table-bordered clusteroneshow" id="reportsheet" style="display:none">
		            <tbody>
		            	<tr>
			            	<td colspan="33" style="border:1px solid #ffffff;">
			            		<h3 class="emptaskhead">Master Preventive Maintenance  Schedule</h3>
			            	</td>
			            </tr>
		            	<tr>
			            	<td colspan="32" style="border-top:1px solid #ffffff;border-right:1px solid #ffffff; border-bottom: :1px solid #000000;border-left:1px solid #ffffff;">
			            	</td>
			            	<td nowrap style="border-right:1px solid #ffffff;">
			            		<b>Year : 2018</b>
			            	</td>
			            </tr>
			            <tr>
			            	<td class="text-center" colspan="3" nowrap>
			            		<b>Location : HA/B515  Cluster Line</b>
			            	</td>
			            	<td class="text-center" colspan="5">
			            		<b>Jan</b>
			            	</td>
			            	<td class="text-center" colspan="5">
			            		<b>Feb</b>
			            	</td>
			            	<td class="text-center" colspan="5">
			            		<b>Mar</b>
			            	</td>
			            	<td class="text-center" colspan="5">
			            		<b>Apr</b>
			            	</td>
			            	<td class="text-center" colspan="5">
			            		<b>May</b>
			            	</td>
			            	<td class="text-center" colspan="5">
			            		<b>Jun</b>
			            	</td>
			            </tr>
			            <tr>
			            	<td nowrap><b>OPN No</b></td>
			            	<td nowrap><b>Equipment Name</b></td>
			            	<td nowrap><b>Plan Vs Actuall</b></td>
			            	<td nowrap class="rotate">Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1505</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Light Pipe Assy</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1520</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Dialplate Assy</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1535</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Pointer Press</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1535 A</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Pointer Press Minor (B515)</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1540</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Applique assy</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1550</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Final Assy</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1570 A</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>EOL Tester A</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1570 B</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>EOL Tester B</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1580 A</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>CAT Tester A</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1580 B</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>CAT Tester B</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1600</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Network</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1610</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Laser Welding MC</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;border-bottom:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap rowspan="6" style="border-right:1px solid #ffffff;border-left:1px solid #ffffff;border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap rowspan="6" colspan="2" style="border-bottom:1px solid #ffffff;">Weekly ( Every once in Week)<br><br>Monthly ( once in a Month)<br><br> Quateraly (every 3 Months)<br><br>Haly Yearly (Once in 6 Months) <br><br>Yearly (Once in a year)	<br><br>PM Completed</td>
			            	<td nowrap class="text-center wcolor" style="border-top:1px solid #ffffff;">W</td>
			            	<td nowrap class="text-center" colspan="4" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5"><b>Maintenance Engineer</b></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="6"><b>Manufacturing Manager</b></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5"><b>REVISION</b></td>
			            	<td nowrap class="text-center" colspan="5"><b>DOCUMENT NO</b></td>
			            	<td nowrap class="text-center"  style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center"  style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center" colspan="4" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5"  style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="6" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center" colspan="4" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5"  style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="6" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center" colspan="4" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5"  style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="6" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;">4.0</td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;">FA_AD_008</td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center ycolor">Y</td>
			            	<td nowrap class="text-center" colspan="4" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap colspan="5"><b>Sign :</b></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap colspan="6"><b>Sign :</b></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center ccolor">C</td>
			            	<td nowrap class="text-center" colspan="4" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap colspan="5"><b>Date :<b></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap colspan="6"><b>Date :<b></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5"></td>
			            	<td nowrap class="text-center" colspan="5"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td colspan="33" style="border:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap rowspan="5" style="border:1px solid #ffffff;">Rev date<br><br>28-Dec-14<br><br>2-Jan-15<br><br>13-May-15<br><br>22-Jun-15</td>
			            	<td nowrap rowspan="5" colspan="2" style="border:1px solid #ffffff;">Rev History<br><br>Final inspection station removed<br><br> Vibration station combined Cat tester<br><br>Annaul PM added for pointer press(1535)<br><br>Mask assy station modified</td>
			            	<td nowrap class="text-center" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="4" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="6" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border:1px solid #ffffff;"></td>
			            </tr>
		            </tbody>
	            </table>
	            <table class="table table-striped table-bordered clustertwoshow" id="reportsheet" style="display:none">
		            <tbody>
		            	<tr>
			            	<td colspan="33" style="border:1px solid #ffffff;">
			            		<h3 class="emptaskhead">Master Preventive Maintenance  Schedule</h3>
			            	</td>
			            </tr>
		            	<tr>
			            	<td colspan="32" style="border-top:1px solid #ffffff;border-right:1px solid #ffffff; border-bottom: :1px solid #000000;border-left:1px solid #ffffff;">
			            	</td>
			            	<td nowrap style="border-right:1px solid #ffffff;">
			            		<b>Year : 2018</b>
			            	</td>
			            </tr>
			            <tr>
			            	<td class="text-center" colspan="3" nowrap>
			            		<b>Location : HA/B515  Cluster Line</b>
			            	</td>
			            	<td class="text-center" colspan="5">
			            		<b>Jul</b>
			            	</td>
			            	<td class="text-center" colspan="5">
			            		<b>Aug</b>
			            	</td>
			            	<td class="text-center" colspan="5">
			            		<b>Sep</b>
			            	</td>
			            	<td class="text-center" colspan="5">
			            		<b>Oct</b>
			            	</td>
			            	<td class="text-center" colspan="5">
			            		<b>Nov</b>
			            	</td>
			            	<td class="text-center" colspan="5">
			            		<b>Dec</b>
			            	</td>
			            </tr>
			            <tr>
			            	<td nowrap><b>OPN No</b></td>
			            	<td nowrap><b>Equipment Name</b></td>
			            	<td nowrap><b>Plan Vs Actuall</b></td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            	<td nowrap>Weekly</td>
			            	<td nowrap>Monthly</td>
			            	<td nowrap>Quaterly</td>
			            	<td nowrap>Half-Yearly</td>
			            	<td nowrap>Yearly</td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1505</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Light Pipe Assy</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1520</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Dialplate Assy</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1535</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Pointer Press</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1535 A</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Pointer Press Minor (B515)</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1540</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Applique assy</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1550</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Final Assy</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1570 A</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>EOL Tester A</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1570 B</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>EOL Tester B</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1580 A</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>CAT Tester A</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1580 B</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>CAT Tester B</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1600</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Network</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td class="centeropentd" nowrap rowspan="2"><b>1610</b></td>
			            	<td class="centerequtd" nowrap rowspan="2"><b>Laser Welding MC</b></td>
			            	<td nowrap class="text-center">Plan</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center">Actuall</td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            	<td nowrap class="text-center"></td>
			            </tr>
			            <tr>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;border-bottom:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border-right:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            	<td style="border:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap rowspan="6" style="border-right:1px solid #ffffff;border-left:1px solid #ffffff;border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap rowspan="6" colspan="2" style="border-bottom:1px solid #ffffff;">Weekly ( Every once in Week)<br><br>Monthly ( once in a Month)<br><br> Quateraly (every 3 Months)<br><br>Haly Yearly (Once in 6 Months) <br><br>Yearly (Once in a year)	<br><br>PM Completed</td>
			            	<td nowrap class="text-center wcolor" style="border-top:1px solid #ffffff;">W</td>
			            	<td nowrap class="text-center" colspan="4" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5"><b>Maintenance Engineer</b></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="6"><b>Manufacturing Manager</b></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5"><b>REVISION</b></td>
			            	<td nowrap class="text-center" colspan="5"><b>DOCUMENT NO</b></td>
			            	<td nowrap class="text-center"  style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center"  style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center" colspan="4" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5"  style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="6" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center" colspan="4" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5"  style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="6" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center" colspan="4" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5"  style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="6" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;">4.0</td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;">FA_AD_008</td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center ycolor">Y</td>
			            	<td nowrap class="text-center" colspan="4" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap colspan="5"><b>Sign :</b></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap colspan="6"><b>Sign :</b></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center ccolor">C</td>
			            	<td nowrap class="text-center" colspan="4" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap colspan="5"><b>Date :<b></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap colspan="6"><b>Date :<b></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5"></td>
			            	<td nowrap class="text-center" colspan="5"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border-bottom:1px solid #ffffff;border-right:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td colspan="33" style="border:1px solid #ffffff;"></td>
			            </tr>
			            <tr>
			            	<td nowrap rowspan="5" style="border:1px solid #ffffff;">Rev date<br><br>28-Dec-14<br><br>2-Jan-15<br><br>13-May-15<br><br>22-Jun-15</td>
			            	<td nowrap rowspan="5" colspan="2" style="border:1px solid #ffffff;">Rev History<br><br>Final inspection station removed<br><br> Vibration station combined Cat tester<br><br>Annaul PM added for pointer press(1535)<br><br>Mask assy station modified</td>
			            	<td nowrap class="text-center" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="4" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="6" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" colspan="5" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border:1px solid #ffffff;"></td>
			            	<td nowrap class="text-center" style="border:1px solid #ffffff;"></td>
			            </tr>
		            </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>