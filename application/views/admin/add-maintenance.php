<?php
$mode = "Add";
$mainten_id=$equ_id=$group_id=$line_id=$frequency=$exe_period=$exe_week=$comments="";
if (!empty($getSingleMaintenance)) {
    $mainten_id = $getSingleMaintenance->mainten_id;
    $equ_id     = $getSingleMaintenance->equ_id;
    $group_id   = $getSingleMaintenance->group_id;
    $line_id    = $getSingleMaintenance->line_id;
    $frequency  = $getSingleMaintenance->frequency;
    $exe_period = $getSingleMaintenance->execution_period;
    $exe_week   = $getSingleMaintenance->execution_week;
    $comments   = $getSingleMaintenance->comments;
    $mode = "Edit";
}
?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/maintenance.png" class="imgbasline"> <?php echo $mode?> Maintenance
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_maintenance" id="frm_maintenance" action="" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="<?php echo $mainten_id?>">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Equipment Name</label>
                                <div class="col-md-8">
                                    <select name="sel_equipment" id="sel_equipment" class="form-control">
                                        <option value="">Select Equipment</option>
                                        <?php
                                            foreach ($getActiveEquipment as $equipmentData) {
                                                $sel="";
                                                if ($equ_id == $equipmentData->equ_id) {
                                                    $sel = "selected";
                                                }
                                                echo "<option value=".$equipmentData->equ_id." ".$sel.">".$equipmentData->equ_name."</option>";
                                            }
                                        ?>
                                    </select>
                                    
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Group</label>
                                <div class="col-md-8">
                                    <select id='sel_group' name='sel_group' class="form-control">
                                        <option  value="">Select Group</option>
                                        <?php
                                            foreach($getArrGroup as $groupData) {
                                                $select="";
                                                if ($group_id == $groupData->group_id) {
                                                    $select = "selected";
                                                }
                                                echo "<option value=".$groupData->group_id." ".$select.">".$groupData->group_name."</option>";
                                        
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Line Name</label>
                                <div class="col-md-8">
                                    <select id='sel_line' name='sel_line' class="form-control">
                                        <option  value="">Select Line</option>
                                        <?php
                                            foreach($getActiveLine as $lineData) {
                                                $selected="";
                                                if ($line_id == $lineData->line_id) {
                                                    $selected = "selected";
                                                }
                                               echo "<option value=".$lineData->line_id." ".$selected.">".$lineData->line_name."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Frequency</label>
                                <div class="col-md-8">
                                   <select id='sel_frequency' name='sel_frequency' class="form-control">
                                        <option  value="">Select Frequency</option>
                                        <option  value="Weekly" <?php echo ($frequency=="Weekly")?"selected":""?>>Weekly </option>
                                        <option  value="Monthly" <?php echo ($frequency=="Monthly")?"selected":""?>>Monthly</option>
                                        <option  value="Quateraly" <?php echo ($frequency=="Quateraly")?"selected":""?>>Quateraly</option>
                                        <option  value="Half-Yearly" <?php echo ($frequency=="Half-Yearly")?"selected":""?>>Half-Yearly</option>
                                        <option  value="Yearly" <?php echo ($frequency=="Yearly")?"selected":""?>>Yearly</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Execution Period</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <div class="icheck-inline">
                                            <label class="">
                                                <div class="iradio_flat-green" style="position: relative;"><input type="radio" name="exe_period" class="icheck customweekbtn" data-radio="iradio_flat-green" value="Weekly" <?php echo ($exe_period=="Weekly")?"checked":""?>><ins class="iCheck-helper"></ins></div> Weekly 
                                            </label>
                                            <label class="">
                                                <div class="iradio_flat-green " style="position: relative;"><input type="radio" name="exe_period" class="icheck customweekbtn" data-radio="iradio_flat-green" value="Monthly" <?php echo ($exe_period=="Monthly")?"checked":""?>><ins class="iCheck-helper"></ins></div> Monthly  
                                            </label>
                                        </div>
                                    </div>
                                    <label id="exe_period-error" class="error" for="exe_period" style="display:none;">Please select execution period</label>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Comments</label>
                                <div class="col-md-8">
                                    <textarea type="text" class="form-control" name="comments" id="comments" placeholder="Comments" value=""><?php echo $comments ?></textarea>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row assignweekshow" style="display:none">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Execution Weekly</label>
                                <div class="col-md-8">
                                    <select id='sel_week' name='sel_week' class="form-control">
                                        <option  value="">Select Execution Weekly</option>
                                        <?php
                                            for($w=1;$w<=50;$w++) {
                                                $s="";
                                                if ($w == $exe_week) {
                                                    $s = "selected";
                                                }
                                                echo "<option  value=".$w." ".$s.">".$w."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            
                        </div>
                        <!--/span-->
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>admin/maintenance"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a><!-- 
                     <button type="button" class="btn btn-info customassignbtn" value="saveassign">
                        <i class="fa fa-save"></i> Save & Assign
                    </button>
                     -->
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>