<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/area.png" class="imgbasline"> Edit Area
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_area" id="frm_area" action="<?php echo base_url() ?>admin/area" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Area</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="area" id="area" placeholder="Area" value="FA">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Status</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <div class="icheck-inline">
                                            <label class="">
                                                <div class="iradio_flat-green" style="position: relative;"><input type="radio" name="radio2" class="icheck" data-radio="iradio_flat-green" checked><ins class="iCheck-helper"></ins></div> Active 
                                            </label>
                                            <label class="">
                                                <div class="iradio_flat-green " style="position: relative;"><input type="radio" name="radio2" class="icheck" data-radio="iradio_flat-green" ><ins class="iCheck-helper"></ins></div> Inactive  
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>admin/area"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>