<?php
	$search_role = $search_status='';
    if (!empty($search)) {
        $search_role      = $search['role_name'];
        $search_status    = $search['status']; 
    }
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <?php
    $msgs=$this->session->flashdata('message_failure');
    if(!empty($msgs)) {
    ?>
        <div class="alert alert-danger alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msgs ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/role.png" class="imgbasline"> Roles List</div>
            <div class="actions">
                <a href="roles/addrole" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Role</a>
            </div>
        </div>
        <div class="portlet-body">
        	<form id="frm_rolelist" name="rm_rolelist" method="POST">
	        	<div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="role[role_name]" id="role_name" placeholder="Role" value="<?php echo $search_role ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <select class="form-control" name="role[status]" id="status">
									<option value="">Select Status</option>
									<option value="1" <?php ($search_status=="1")?"selected":"" ?>>Active</option>
									<option value="2" <?php ($search_status=="2")?"selected":"" ?>>Inactive</option>
								</select>
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."admin/roles"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Role</th>
		                        <th>Status</th>
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
			                    	if (!empty($getRoleList)){
			                    		$sno = 1;
			                    		foreach ($getRoleList as $roleData) {
			                    			$label  = ($roleData->status== "1")?"success":"danger";
			                    			$status = ($roleData->status== "1")?"Active":"Inactive";
			                    ?>
						                    <tr>
						                        <td><?php echo $sno ?></td>
						                        <td><?php echo $roleData->role_name ?></td>
						                        <td><span class="label label-sm label-<?php echo $label ?> labelradius" style="padding: 1px 13px;"><?php echo $status ?></span></td>
						                        <td> <a href="roles/addrole/<?php echo $roleData->role_id ?>" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn deleteRowid" data-id="<?php echo $roleData->role_id ?>" data-type="role" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
						                    </tr>
			                    <?php   
			                            $sno++; 
			                            }
			                    	}
			                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>