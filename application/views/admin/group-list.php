<?php
	$search_group = $search_status='';
    if (!empty($search)) {
        $search_group      = $search['group_name'];
        $search_status    = $search['status']; 
    }
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <?php
    $msgs=$this->session->flashdata('message_failure');
    if(!empty($msgs)) {
    ?>
        <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
            <?php echo $msgs ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/category.png" class="imgbasline"> Group List</div>
            <div class="actions">
                <a href="group/addgroup" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Group</a>
            </div>
        </div>
        <div class="portlet-body">
        	<form name="frm_grouplist" id="frm_grouplist" method="POST" action="">
	        	<div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="group[group_name]" id="group_name" placeholder="Group Name" value="<?php echo $search_group ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <select class="form-control" name="group[status]" id="status">
									<option value="">Select Status</option>
									<option value="1" <?php echo ($search_status=="1")?"selected":"";?>>Active</option>
									<option value="2" <?php echo ($search_status=="2")?"selected":"";?>>Inactive</option>
								</select>
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."admin/group"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Group Name</th>
		                        <th>Status</th>
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
			                    if (!empty($getGroupList)){
			                    	$sno = 1;
			                    	foreach ($getGroupList as $groupData) {
			                    		$label  = ($groupData->status== "1")?"success":"danger";
			                    		$status = ($groupData->status== "1")?"Active":"Inactive";
			                ?>
					                    <tr>
					                        <td><?php echo $sno ?></td>
					                        <td><?php echo $groupData->group_name ?></td>
					                        <td><span class="label label-sm label-<?php echo $label ?> labelradius" style="padding: 1px 13px;"><?php echo $status ?></span></td>
					                        <td> <a href="group/addgroup/<?php echo $groupData->group_id ?>" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn deleteRowid" data-id="<?php echo $groupData->group_id ?>" data-type="group" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
					                    </tr>
			                <?php   
			                        $sno++; 
			                        }
			                    }
			                ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>