<?php
$search_name = $search_emp = $search_email=$search_mobile= $search_user=$search_area='';
if (!empty($search)) {
    $search_emp     = $search["emp_id"];
    $search_name    = $search["name"];
    $search_email   = $search["email"];
    $search_mobile  = $search["mobile"];
    $search_user    = $search["username"];
    $search_area    = $search["area"];
}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
	    <div class="alert alert-success alert-dismissible">
	        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	        <?php echo $msg ?>
	    </div>
    <?php
    }
    ?>
    <?php
    $msgs=$this->session->flashdata('message_failure');
    if(!empty($msgs)) {
    ?>
	    <div class="alert alert-danger alert-dismissible">
	        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	        <?php echo $msgs ?>
	    </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/supervisor.png" class="imgbasline"> Supervisor List</div>
            <div class="actions">
                <a href="supervisor/addsupervisor" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Supervisor</a>
            </div>
        </div>
        <div class="portlet-body">
        	<form name="frm_supervisorlist" id="frm_supervisorlist" action="" method="POST">
	        	<div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="supervisor[emp_id]" id="emp_id" placeholder="Emp ID" value="<?php echo $search_emp?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="supervisor[name]" id="name" placeholder="Name" value="<?php echo $search_name?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="supervisor[email]" id="email" placeholder="Email Address" value="<?php echo $search_email?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="supervisor[mobile]" id="mobile" placeholder="Mobile Number" value="<?php echo $search_mobile?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <select class="form-control" name="supervisor[area]" id="area">
									<option value="">Select Area</option>
									<?php
										foreach ($getActiveArea as $areaData) {
											$sel='';
											if($search_area==$areaData->area_id){
												$sel="selected";
											}
											echo "<option value=".$areaData->area_id." ".$sel.">".$areaData->area_name."</option>";
										}
									?>
								</select>
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="supervisor[username]" id="username" placeholder="Username" value="<?php echo $search_user ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."admin/supervisor"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Emp Id</th>
		                        <th>Name</th>
		                        <th>User Name</th>
		                        <th>Email</th>
		                        <th>Mobile</th>
		                        <th>Area</th>
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                    	if(!empty($getSupervisorList)) {
		                    		$sno=1;
		                    		foreach($getSupervisorList as $supervisorData) {
		                    ?>
		                    <tr>
		                        <td><?php echo $sno ?></td>
		                        <td><?php echo $supervisorData->emp_id ?></td>
		                        <td><?php echo $supervisorData->supervisor_name ?></td>
		                        <td><?php echo $supervisorData->user_name ?></td>
		                        <td><?php echo $supervisorData->supervisor_email ?></td>
		                        <td><?php echo $supervisorData->supervisor_mobile ?></td>
		                        <td><?php echo $supervisorData->area_name ?></td>
		                        <td> <a href="supervisor/addsupervisor/<?php echo $supervisorData->supervisor_id ?>" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn deleteRowid" data-id="<?php echo $supervisorData->supervisor_id ?>" data-type="supervisor" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
		                    </tr>
		                    <?php
		                            $sno++;
		                    		}
		                    	}
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>