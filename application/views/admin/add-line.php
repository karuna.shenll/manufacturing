<?php
    $line_id=$line_name=$doc_no=$area_id= $revision_add=$first_equ=$first_line_id='';
    $mode="Add";
    if (!empty($getSingleLine)) {
        $line_id   = $getSingleLine->line_id;
        $line_name = $getSingleLine->line_name;
        $doc_no    = $getSingleLine->doc_no;
        $area_id   = $getSingleLine->area_id;
        $revision_add = $getSingleLine->revision_add;
        $mode="Edit";
    }
    if(!empty($getArrLogLine)){
        $first_line_id= $getArrLogLine[0]->line_id;
        $first_equ= $getArrLogLine[0]->equ_id;
    }
?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/assembly.png" class="imgbasline"> <?php echo $mode?> Line
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_line" id="frm_line" action="" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="<?php echo $line_id; ?>">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Line Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="line_name" id="line_name" placeholder="Line Name" value="<?php echo $line_name; ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Doc No</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="doc_no" id="doc_no" placeholder="Doc No" value="<?php echo $doc_no; ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Area</label>
                                <div class="col-md-8">
                                   <select id='area_id' name='area_id' class="form-control">
                                        <option  value="">Select Area</option>
                                        <?php
                                            foreach($getArrArea as $AreaData) {
                                                $selected="";
                                                if ($AreaData->area_id==$area_id) {
                                                   $selected="selected";
                                                }
                                                echo "<option  value=".$AreaData->area_id." ".$selected.">".$AreaData->area_name."</option>"; 
                                            }
                                        ?>
                                   </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Revision Add</label>
                                <div class="col-md-8">
                                    <select name="sel_revision" id="sel_revision" class="form-control">
                                        <option  value="">Select Revision</option>
                                        <?php
                                            for ($i=1;$i<=10;$i++) {
                                                $selected="";
                                                if ($revision_add==$i) {
                                                   $selected="selected";
                                                }
                                                echo "<option value=".$i." ".$selected.">".$i."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Eqiupment</label>
                                <div class="col-md-7">
                                    <select name="sel_eqiupment[]" class="form-control sel_eqiupment">
                                        <option value="">Select Eqiupment</option>
                                        <?php
                                            foreach($getArrEquipment as $equipmentData) {
                                                $sel="";
                                                if ($first_equ == $equipmentData->equ_id ) {
                                                    $sel="selected";
                                                }
                                                echo "<option value=".$equipmentData->equ_id." ".$sel.">".$equipmentData->equ_name."</option>";
                                            } 
                                        ?>
                                    </select>
                                    <input type="hidden" name="hnd_log_id[]" value="<?php echo $first_line_id ?>">
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-primary customaddmorebtn" name="addmore" id="addmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>
                            <div id="appendContent">
                                <?php
                                if(!empty($line_id)) {
                                    if(!empty($getArrLogLine)) {
                                        $count=(count($getArrLogLine)-1);
                                        for($k=0;$k<=$count;$k++) {
                                            if($k!=0) {
                                            $equ_name_id = $getArrLogLine[$k]->equ_id;
                                            $line_log_id = $getArrLogLine[$k]->line_log_id;
                                ?>
                                <div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright">
                                    <div class="form-group">
                                        <label class="control-label col-md-4" style="padding-right: 0px;"></label>
                                        <div class="col-md-7">
                                            <select name="sel_eqiupment[]" class="form-control sel_eqiupment">
                                                <option value="">Select Eqiupment</option>
                                                <?php
                                                    foreach($getArrEquipment as $equipmentData) {
                                                        $sel="";
                                                        if ($equ_name_id == $equipmentData->equ_id ) {
                                                            $sel="selected";
                                                        }
                                                        echo "<option value=".$equipmentData->equ_id." ".$sel.">".$equipmentData->equ_name."</option>";
                                                    } 
                                                ?>
                                            </select>
                                            <input type="hidden" name="hnd_log_id[]" value="<?php echo $line_log_id ?>">
                                        </div>
                                    <div class="col-md-1">
                                        <button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div>
                                    </div>
                                </div>
                                <?php    
                                           }
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            
                        </div>
                        <!--/span-->
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>admin/line"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>