<!-- BEGIN CONTENT BODY -->
<?php 
$search_name ='';
if (!empty($search)) {
$search_name = $search['select_report'];   
}
?>
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/report.png" class="imgbasline">Reports</div>
            <div class="actions">
                <!-- <a href="assembly/addassembly" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Assembly</a> -->
            </div>
        </div>
        <div class="portlet-body">
        	<div class="row">
        		<form action="" method="post">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <select  class="form-control" name="report[select_report]" id="select_report">       
                            	<option value="Cluster1" <?php echo ($search_name=="Cluster1")?"selected":""; ?>>Cluster1</option>
                           		<option value="Cluster2" <?php echo ($search_name=="Cluster2")?"selected":""; ?>>Cluster2</option>
                           </select>
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="<?php //echo site_url('admin/dashboard/viewreport/'.$view_report['equ_id']); ?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </form>
	        </div>
        	<div class="table-responsive" style="overflow-x: auto;margin-top:0px;"><!--suppliertbl-->
	            <table class="table table-striped table-bordered" id="reportsheet">
		            <tbody>
		            	<tr>
			            	<td colspan="33" class="bordernone">
			            		<h3 class="emptaskhead">Master Preventive Maintenance Schedule</h3>
			            	</td>
			            </tr>
		            	<tr>
			            	<td colspan="32" class="borderright bordertop borderleft" style="border-bottom: :1px solid #000000;">
			            	</td>
			            	<td nowrap class="borderright">
			            		<b>Year : <?php echo date('Y'); ?></b>
			            	</td>
			            </tr>
			            <tr>
			            	<td class="text-center" colspan="3" nowrap>
			            		<b>Location : <?php echo $viewreports["line_name"]  ?></b>
			            	</td>
			            	<?php foreach ($viewreports["month_name"] as  $month_name) { ?>
				            	<td class="text-center" colspan="5">
				            		<b><?php echo $month_name; ?></b>
				            	</td>
			            	<?php } ?>
			            </tr>
			            <tr>
			            	<td nowrap><b>OPN No</b></td>
			            	<td nowrap><b>Equipment Name</b></td>
			            	<td nowrap><b>Plan Vs Actual</b></td>
			            	<?php 
			            		$frequencyArr=array('Weekly','Monthly','Quaterly','Half-Yearly','Yearly');
			            		foreach ($viewreports['month_name'] as $key => $month_name) { 
				            		foreach ($frequencyArr as $key => $value) {?>
						            	<td nowrap><?php echo $value; ?></td>
					            	<?php } 
					            } 
					        ?>
			            </tr>
			            <?php 
			            	if(!empty($viewreports['cluster_monthly_frequency'])) { 
			            		foreach ($viewreports['cluster_monthly_frequency'] as $key => $value) { ?>
						            <tr>
						            	<td class="centeropentd" nowrap rowspan="2"><b><?php echo $viewreports['equ_name'][$key]['equ_code']; ?></b></td>
						            	<td class="centerequtd" nowrap rowspan="2"><b><?php echo $viewreports['equ_name'][$key]['equ_name']; ?></b></td>
						            	<td nowrap class="text-center">Plan</td>
						            	<?php 
						            		foreach ($value as $key1 => $value1) { 
						            			foreach ($value1 as $key2 => $value2) { 
						            				if ($key2=="Weekly" && $value2>0) {
						            				 	$class = 'wcolor';
						            				 	$text = "W";
						            				} else if ($key2=="Monthly" && $value2>0) {
						            				 	$class = 'mcolor';
						            				 	$text = "M";
						            				} else if ($key2=="Quaterly" && $value2>0) {
						            				 	$class = 'qcolor';
						            				 	$text = "Q";
						            				} else if ($key2=="Half-Yearly" && $value2>0) {
						            				 	$class = 'hcolor';
						            				 	$text = "H";
						            				} else if ($key2=="Yearly" && $value2>0) {
						            				 	$class = 'ycolor';
						            				 	$text = "Y";
						            				} else {
						            					$class = '';
						            				 	$text = "";
						            				} ?>
						            				<td nowrap class="text-center <?php echo $class; ?>"><?php echo $text; ?></td>
						            			<?php }
						            		}
						            	?>
						            </tr>
						            <tr>
						            	<td nowrap class="text-center">Actuall</td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            	<td nowrap class="text-center"></td>
						            </tr>
			        			<?php }
			        		} 
			        	?>
			            <tr>
			            	<td class="bordernone"></td>
			            	<td class="bordernone"></td>
			            	<td class="bordernone"></td>
			            	<td class="borderright"></td>
			            	<td class="bordernone"></td>
			            	<td class="bordernone"></td>
			            	<td class="bordernone"></td>
			            	<td class="bordernone"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright borderbottom"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="bordernone"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="borderright"></td>
			            	<td class="bordernone"></td>
			            	<td class="bordernone"></td>
			            </tr>
			            <tr>
			            	<td nowrap rowspan="6" class="borderleft borderright borderbottom bordertop"></td>
			            	<td nowrap rowspan="6" colspan="2" class="borderbottom">Weekly ( Every once in Week)<br><br>Monthly ( once in a Month)<br><br> Quateraly (every 3 Months)<br><br>Haly Yearly (Once in 6 Months) <br><br>Yearly (Once in a year)	<br><br>PM Completed</td>
			            	<td nowrap class="text-center wcolor bordertop" >W</td>
			            	<td nowrap class="text-center borderbottom" colspan="4"></td>
			            	<td nowrap class="text-center" colspan="5"><b>Maintenance Engineer</b></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap class="text-center" colspan="6"><b>Manufacturing Manager</b></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap class="text-center" colspan="5"><b>REVISION</b></td>
			            	<td nowrap class="text-center" colspan="5"><b>DOCUMENT NO</b></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center mcolor">M</td>
			            	<td nowrap class="text-center borderbottom" colspan="4"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom" ></td>
			            	<td nowrap class="text-center borderbottom" colspan="6"></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5" ></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center qcolor">Q</td>
			            	<td nowrap class="text-center borderbottom" colspan="4"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom" ></td>
			            	<td nowrap class="text-center borderbottom" colspan="6"></td>
			            	<td nowrap class="text-center borderbottom" ></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center hcolor">H</td>
			            	<td nowrap class="text-center borderbottom" colspan="4"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap class="text-center borderbottom" colspan="6"></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5">4.0</td>
			            	<td nowrap class="text-center borderbottom" colspan="5">FA_AD_008</td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center ycolor">Y</td>
			            	<td nowrap class="text-center borderbottom" colspan="4"></td>
			            	<td nowrap colspan="5"><b>Sign :</b></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap colspan="6"><b>Sign :</b></td>
			            	<td nowrap class="text-center borderbottom"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            </tr>
			            <tr>
			            	<td nowrap class="text-center ccolor">C</td>
			            	<td nowrap class="text-center borderbottom" colspan="4"></td>
			            	<td nowrap colspan="5"><b>Date :<b></td>
			            	<td nowrap class="text-center borderbottom" ></td>
			            	<td nowrap colspan="6"><b>Date :<b></td>
			            	<td nowrap class="text-center borderbottom" ></td>
			            	<td nowrap class="text-center" colspan="5"></td>
			            	<td nowrap class="text-center" colspan="5"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            	<td nowrap class="text-center borderbottom borderright"></td>
			            </tr>
			            <tr>
			            	<td colspan="33" class="bordernone"></td>
			            </tr>
			            <tr>
			            	<td nowrap rowspan="5" class="bordernone">Rev date<br><br>28-Dec-14<br><br>2-Jan-15<br><br>13-May-15<br><br>22-Jun-15</td>
			            	<td nowrap rowspan="5" colspan="2" class="bordernone">Rev History<br><br>Final inspection station removed<br><br> Vibration station combined Cat tester<br><br>Annaul PM added for pointer press(1535)<br><br>Mask assy station modified</td>
			            	<td nowrap class="text-center bordernone"></td>
			            	<td nowrap class="text-center bordernone" colspan="4"></td>
			            	<td nowrap class="text-center bordernone" colspan="5" ></td>
			            	<td nowrap class="text-center bordernone"></td>
			            	<td nowrap class="text-center bordernone" colspan="6"></td>
			            	<td nowrap class="text-center bordernone"></td>
			            	<td nowrap class="text-center bordernone" colspan="5"></td>
			            	<td nowrap class="text-center bordernone" colspan="5" ></td>
			            	<td nowrap class="text-center bordernone" ></td>
			            	<td nowrap class="text-center bordernone"></td>
			            </tr>
		            </tbody>
	            </table>
	           
	        </div>
        </div>
    </div>
</div>