<?php
    $search_name=$search_group = $search_line = $search_frequency ='';
    if (!empty($search)) {
        $search_name  	  = $search['equipment_name'];
        $search_group 	  = $search['group_id'];
        $search_line      = $search['line_name'];
        $search_frequency = $search['sel_frequency'];
    }
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <?php
    $msgs=$this->session->flashdata('message_failure');
    if(!empty($msgs)) {
    ?>
        <div class="alert alert-danger alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msgs ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/maintenance.png" class="imgbasline"> Maintenance List</div>
            <div class="actions">
                <a href="maintenance/addmaintenance" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Maintenance</a>
            </div>
        </div>
        <div class="portlet-body">
        	<form name="frm_maintenancelist" id="frm_maintenancelist" action="" method="POST">
	        	<div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="maintenance[equipment_name]" id="equipment_name" placeholder="Equipment Name" value="<?php echo $search_name ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <select class="form-control" name="maintenance[group_id]" id="group_id">
									<option value="">Select Group</option>
									<?php 
										foreach($getArrGroup as $groupData) {
                                            $select="";
                                            if ($search_group == $groupData->group_id) {
                                                $select = "selected";
                                            }
                                            echo "<option value=".$groupData->group_id." ".$select.">".$groupData->group_name."</option>";
                                    
                                        }
									?>
								</select>
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="maintenance[line_name]" id="line_name" placeholder="Line Name" value="<?php echo $search_line?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<select id='sel_frequency' name='maintenance[sel_frequency]' class="form-control">
	                            <option  value="">Select Frequency</option>
	                            <option  value="Weekly" <?php echo ($search_frequency=="Weekly")?"selected":""?>>Weekly </option>
	                            <option  value="Monthly" <?php echo ($search_frequency=="Monthly")?"selected":""?>>Monthly</option>
	                            <option  value="Quateraly" <?php echo ($search_frequency=="Quateraly")?"selected":""?>>Quateraly</option>
	                            <option  value="Half-Yearly" <?php echo ($search_frequency=="Half-Yearly")?"selected":""?>>Haly Yearly</option>
	                            <option  value="Yearly" <?php echo ($search_frequency=="Yearly")?"selected":""?>>Yearly</option>
	                        </select>
		        		</div>
		        		<div class="col-md-12 text-center">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."supervisor/maintenance"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Equipment Name</th>
		                        <th>Group</th>
		                        <th>Line Name</th>
		                        <th>Frequency</th>
		                        <th>Execution Period</th>
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                        $sno=1;
		                    	if (!empty($getmaintenanceList)){
		                    		foreach ($getmaintenanceList as $maintenanceData) {
		                    ?>
		                    <tr>
		                        <td><?php echo $sno ?></td>
		                        <td><?php echo $maintenanceData->equ_name?></td>
		                        <td><?php echo $maintenanceData->group_name?></td>
		                        <td><?php echo $maintenanceData->line_name?></td>
		                        <td><?php echo $maintenanceData->frequency?></td>
		                        <td><?php echo $maintenanceData->execution_week?></td>
		                        <td> <a href="maintenance/addmaintenance/<?php echo $maintenanceData->mainten_id?>" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn deleteRowid" data-id="<?php echo $maintenanceData->mainten_id?>" data-type="maintenance" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
		                    </tr>
		                    <?php
		                            $sno++;
		                    		}
		                    	}
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>