				</div>
				<!-- END CONTENT -->
			</div>
			<!-- END CONTAINER -->
			<!-- BEGIN FOOTER -->
			<div class="page-footer">
				<div class="page-footer-inner"> <?php echo date("Y")?> &copy; Manufacturing-Maintenance
					<a target="_blank" href="javascript:void(0)"></a> &nbsp;|&nbsp;
					<a href="javascript:void(0)" target="_blank">All Rights Reserved.</a>
				</div>
				<div class="scroll-to-top">
					<i class="icon-arrow-up"></i>
				</div>
			</div>
			<!-- END FOOTER -->
		</div>
		<!-- BEGIN CORE PLUGINS -->
		
		<!-- END PAGE LEVEL PLUGINS -->
	</body>
</html>