<?php
error_reporting(0);
$navigationURL = reset(explode("?",end(explode("/",$_SERVER["REQUEST_URI"]))));
if (is_numeric($navigationURL)) {
    $navigationURL=$this->uri->segment(3);
}
?>
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 0px">
			<li class="sidebar-toggler-wrapper hide">
				<div class="sidebar-toggler">
					<span></span>
				</div>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='dashboard')?'active open':''; ?>">
				<a href="<?php echo base_url().'supervisor/dashboard'; ?>" class="nav-link nav-toggle">
					<i class="dashboard-image"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='task' || $navigationURL=='addtask' || $navigationURL=='edittask'||$navigationURL=='viewtask')?'active open':''; ?>">
				<a href="<?php echo base_url().'supervisor/task'; ?>" class="nav-link nav-toggle">
					<i class="icon-task"></i> 
					<span class="title">Task</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='maintenance' || $navigationURL=='addmaintenance' || $navigationURL=='editmaintenance')?'active open':''; ?>">
				<a href="<?php echo base_url().'supervisor/maintenance'; ?>" class="nav-link nav-toggle">
					<i class="icon-mainten"></i> 
					<span class="title">Maintenance</span>
					<span class="selected"></span>
				</a>
			</li>
			
			<li class="nav-item <?php echo ($navigationURL=='assigntask' || $navigationURL=='addassigntask' || $navigationURL=='editassigntask')?'active open':''; ?>">
				<a href="<?php echo base_url().'supervisor/assigntask'; ?>" class="nav-link nav-toggle">
					<i class="icon-assigntask"></i> 
					<span class="title">Assign task</span>
					<span class="selected"></span>
				</a>
			</li>
		</ul>
	</div>
</div>