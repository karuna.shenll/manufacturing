<div class="page-content"  id="dashboard">
    <?php
    $msg=$this->session->flashdata('success');
    if(!empty($msg)){
    ?>
    <div>
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo $msg ?>
        </div>
    </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/opentask.png" class="imgbasline"> Ongoing Task</div>
            <div class="actions">
               <!--  <a href="task/addtask" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Task</a> -->
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                <table class="table table-striped table-bordered table-hover suppliertbl" id="applied-tenders">
                    <thead>
                        <tr>
                            <th style="width: 50px;">SI.NO</th>
                            <th>Equipment Code</th>
                            <th>Equipment Model</th>
                            <th>Equipment Name</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    </tbody>
                        <?php
                        if (!empty($progressTaskList)) {
                            $sno=1;
                            foreach ($progressTaskList as $progressTask) {
                        ?>
                        <tr>
                            <td> <?php echo $sno ?> </td>
                            <td> <?php echo $progressTask->equ_code; ?> </td>
                            <td> <?php echo $progressTask->equ_model; ?> </td>
                            <td> <?php echo $progressTask->equ_name; ?> </td>
                            <td><span class="label label-sm label-warning labelradius"><?php echo $progressTask->task_status; ?></span></td>
                        </tr>
                        <?php
                            $sno++;
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/opentask.png" class="imgbasline"> Open Task</div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
                    <thead>
                        <tr>
                            <th style="width: 50px;">SI.NO</th>
                            <th>Equipment Code</th>
                            <th>Equipment Model</th>
                            <th>Equipment Name</th>
                            <th>Status</th>
                            <th>Action </th>
                        </tr>
                    </thead>
                    </tbody>
                        <?php
                        if (!empty($openTaskList)) {
                            $sno=1;
                            foreach ($openTaskList as $openTask) {
                        ?>
                        <tr>
                            <td> <?php echo $sno ?> </td>
                            <td> <?php echo $openTask->equ_code; ?> </td>
                            <td> <?php echo $openTask->equ_model; ?> </td>
                            <td> <?php echo $openTask->equ_name; ?> </td>
                            <td><span class="label label-sm label-success labelradius"><?php echo $openTask->task_status; ?></span></td>
                            <td><a href="javascript:void(0);" type="button" class="btn grey-cascade btn-xs customaddbtn opentaskpopup" title="Edit" data-id="<?php echo $openTask->task_id; ?>"><i class="fa fa-ticket"></i> Assign Task</a></td>
                        </tr>
                        <?php
                            $sno++;
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/opentask.png" class="imgbasline"> Completed Task</div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                <table class="table table-striped table-bordered table-hover suppliertbl" id="award-tenders">
                    <thead>
                        <tr>
                            <th style="width: 50px;">SI.NO</th>
                            <th>Equipment Code</th>
                            <th>Equipment Model</th>
                            <th>Equipment Name</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    </tbody>
                        <?php
                        if (!empty($completedTaskList)) {
                            $sno=1;
                            foreach ($completedTaskList as $completedTask) {
                        ?>
                        <tr>
                            <td> <?php echo $sno ?> </td>
                            <td> <?php echo $completedTask->equ_code; ?> </td>
                            <td> <?php echo $completedTask->equ_model; ?> </td>
                            <td> <?php echo $completedTask->equ_name; ?> </td>
                            <td><span class="label label-sm label-completed labelradius"><?php echo $completedTask->task_status; ?></span></td>
                        </tr>
                        <?php
                            $sno++;
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/opentask.png" class="imgbasline"> Expired Task</div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                <table class="table table-striped table-bordered table-hover suppliertbl" id="applied-tender">
                    <thead>
                        <tr>
                            <th style="width: 50px;">SI.NO</th>
                            <th>Equipment Code</th>
                            <th>Equipment Model</th>
                            <th>Equipment Name</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    </tbody>
                        <?php
                        if (!empty($expiredTaskList)) {
                            $sno=1;
                            foreach ($expiredTaskList as $expiredTask) {
                        ?>
                        <tr>
                            <td> <?php echo $sno; ?> </td>
                            <td> <?php echo $expiredTask->equ_code; ?> </td>
                            <td> <?php echo $expiredTask->equ_model; ?> </td>
                            <td> <?php echo $expiredTask->equ_name; ?> </td>
                            <td><span class="label label-sm label-danger labelradius"><?php echo $expiredTask->task_status; ?></span></td>
                        </tr>
                        <?php
                            $sno++;
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="modal fade in" id="assignPopup" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="assing_status" id="assing_status" method="POST">
                <input type="hidden" name="hnd_id" id="hnd_id">
                <input type="hidden" name="hndeid" id="hndeid">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><b>Assign Task</b></h4>
                </div>
                <div class="modal-body"> 
                     <div class="row">
                        <div class="col-md-11" style="margin-top: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4">Emp Id</label>
                                <div class="col-md-8">  
                                    <select name="emp_id" id="changesempid" class="form-control">
                                        <option value="">Select Employee ID</option>
                                    <?php
                                        foreach ($openEmployeeList as $Employeeid) {
                                            echo "<option value=".$Employeeid->emp_id.">".$Employeeid->emp_id."</option>";
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                     </div>
                    <div class="row">
                        <div class="col-md-11" style="margin-top: 15px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4">Employee Name:</label>
                                <div class="col-md-8">
                                    <input type="text" name="emp_name" id="empname" class="form-control" Palceholder="Employee Name">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4">Line:</label>
                                <div class="col-md-8 vendor-detail">
                                    <select  name="line" id="appendline" class="form-control clickdropnone"  readonly>
                                    <?php
                                        foreach ($getActiveLine as $LineData) {
                                            echo "<option value=".$LineData->line_id.">".$LineData->line_name."</option>";
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4">Area:</label>
                                <div class="col-md-8 vendor-detail">
                                    <select name="area" id="appendarea" class="form-control clickdropnone" readonly> 
                                    <?php
                                        foreach ($getActiveArea as $AreaData) {
                                            echo "<option value=".$AreaData->area_id.">".$AreaData->area_name."</option>";
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4">Equipment Code:</label>
                                <div class="col-md-8 vendor-detail">
                                    <input type="text" name="equ_code" id="appendequcode" class="form-control" Palceholder="Equipment Code" value="" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4">Equipment Name:</label>
                                <div class="col-md-8 vendor-detail">
                                    <select  name="equ_name" id="appendequname" class="form-control clickdropnone"  readonly>
                                    <?php
                                        foreach ($getActiveEquipment as $EquipmentData) {
                                            echo "<option value=".$EquipmentData->equ_id.">".$EquipmentData->equ_name."</option>";
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4">Frequency:</label>
                                <div class="col-md-8 vendor-detail">
                                    <select id='selfrequency' name='sel_frequency' class="form-control clickdropnone" readonly>
                                        <option  value="">Select Frequency</option>
                                        <option  value="Weekly">Weekly </option>
                                        <option  value="Monthly">Monthly</option>
                                        <option  value="Quateraly">Quateraly</option>
                                        <option  value="Half-Yearly">Half-Yearly</option>
                                        <option  value="Yearly">Yearly</option>
                                    </select>
                                </div>
                             </div>
                        </div>
                    </div>
                     <div class="modal-footer">
                        <button type="button" class="btn green customaddbtn" id="statuschange"> <i class="fa fa-check"></i> Save</button>
                        <button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
                    </div>
                </div>
            </form>
        <!-- /.modal-content -->
       </div>
    <!-- /.modal-dialog -->
   </div>
</div>