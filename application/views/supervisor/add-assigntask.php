<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/assigntask.png" class="imgbasline"> Add Assigntask
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_task" id="frm_task" action="<?php echo base_url() ?>supervisor/assigntask" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Employee Id</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_id" id="emp_id" placeholder="Employee Id" value="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Employee Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="Employee Name" value="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Line Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="assembly_name" id="assembly_name" placeholder="Line Name" value="">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Area</label>
                                <div class="col-md-8">
                                    <select name="area" id="area" class="form-control" disabled>
                                        <option value="">Select Area</option>
                                        <option value="FA" selected>FA</option>
                                        <option value="FB">FB</option>
                                        <option value="FC">FC</option>
                                        <option value="FD">FD</option>
                                        <option value="FE">FE</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Equipment Code</label>
                                <div class="col-md-8">
                                    <select name="area" id="area" class="form-control" disabled>
                                        <option value="">Equipment Code</option>
                                        <option value="EM001" selected>EM001</option>
                                        <option value="EM002">EM002</option>
                                        <option value="EM003">EM003</option>
                                        <option value="EM004">EM004</option>
                                        <option value="EM005">EM005</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Equipment Name</label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="equipment_name" id="equipment_name" placeholder="Equipment Name" value="Light Pipe Assy" disabled>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>supervisor/assigntask"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>