<!-- BEGIN CONTENT BODY -->
<?php
$search_empid = $search_ename = $search_lname = $search_aname = $search_code = $search_name = "";
if (!empty($search)) {
    $search_empid  = $search["emp_id"];
    $search_ename  = $search["emp_name"];
    $search_lname  = $search["line_name"];
    $search_aname  = $search["area_name"];
    $search_code   = $search["equ_code"];
    $search_name   = $search["equ_name"];
}
?>
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <?php
    $msgs=$this->session->flashdata('message_failure');
    if(!empty($msgs)) {
    ?>
        <div class="alert alert-danger alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msgs ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/assigntask.png" class="imgbasline"> Assigntask List</div>
            <div class="actions">
               <!--  <a href="assigntask/addassigntask" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Assigntask</a> -->
            </div>
        </div>
        <div class="portlet-body">
        	<form id=frm_assingtask" name="frm_assingtask" method="POST">
	        	<div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="asssigntask[emp_id]" id="emp_id" placeholder="Employee ID" value="<?php echo $search_empid?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="asssigntask[emp_name]" id="emp_name" placeholder="Employee  Name" value="<?php echo $search_ename?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="asssigntask[line_name]" id="assembly_name" placeholder="Line Name" value="<?php echo $search_lname?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="asssigntask[area_name]" id="area" placeholder="Area" value="<?php echo $search_aname?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="asssigntask[equ_code]" id="equ_code" placeholder="Equipment Code" value="<?php echo $search_code?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                            <input type="text" class="form-control" name="asssigntask[equ_name]" id="equ_name" placeholder="Equipment Name" value="<?php echo $search_name?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."supervisor/assigntask"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Emp Id</th>
		                        <th>Emp Name</th>
		                        <th>Line Name</th>
		                        <th>Area</th>
		                        <th>Equipment Code</th>
		                        <th>Equipment Name</th>
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                    if (!empty($getAssignTask)) {
		                    	$sno=1;
		                    	foreach ($getAssignTask as $assigntaskData) {
		                    ?>
		                    <tr>
		                        <td><?php echo $sno ?></td>
		                        <td><?php echo $assigntaskData->emp_id ?></td>
		                        <td><?php echo $assigntaskData->emp_name ?></td>
		                        <td><?php echo $assigntaskData->line_name ?></td>
		                        <td><?php echo $assigntaskData->area_name ?></td>
		                        <td><?php echo $assigntaskData->equ_code ?></td>
		                        <td><?php echo $assigntaskData->equ_name ?></td>
		                        <td> <a href="assigntask/editassigntask/<?php echo $assigntaskData->assign_task_id ?>" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn deleteRowid" data-id="<?php echo $assigntaskData->assign_task_id ?>" data-type="assigntask" title="Delete"><i class="fa fa-trash"></i> Delete</a></td>
		                    </tr>
		                    <?php
		                        $sno++;
		                    	}
		                    }
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>