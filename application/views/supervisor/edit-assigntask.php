<?php
    if (!empty($getAssignSingle)) {
        $e_id     = $getAssignSingle->eid;
        $empcode  = $getAssignSingle->empcode;
        $emp_name = $getAssignSingle->emp_name;
        $task_id  = $getAssignSingle->task_id;
        $line_id  = $getAssignSingle->line_id;
        $area_id  = $getAssignSingle->area_id;
        $equ_code = $getAssignSingle->equ_code;
        $equ_id   = $getAssignSingle->equ_id;
        $assign_task_id  = $getAssignSingle->assign_task_id;
    }
?>
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/assigntask.png" class="imgbasline"> Edit Assigntask
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_assigntask" id="frm_assigntask" action="" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="<?php echo $assign_task_id?>">
                <input type="hidden" name="hndeid" id="hndeid" value="<?php echo $e_id ?>">
                <input type="hidden" name="hndtaskid" value="<?php echo $task_id?>">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Employee Id</label>
                                <div class="col-md-8">
                                    <select type="text" class="form-control" name="emp_id" id="changesempid">
                                        <option value="">Select Employee Id</option>
                                        <?php
                                            foreach ($openEmployeeList as $EmployeeListData) {
                                                $selected="";
                                                if ($empcode == $EmployeeListData->emp_id) {
                                                    $selected = "selected";
                                                }
                                                echo "<option value=".$EmployeeListData->emp_id." ".$selected.">".$EmployeeListData->emp_id."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Employee Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="emp_name" id="empname" placeholder="Employee Name" value="<?php echo $emp_name ?>">
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Line Name</label>
                                <div class="col-md-8">
                                    <select name="line_id" id="line_id" class="form-control clickdropnone" readonly>
                                        <option value="">Select Line</option>
                                        <?php
                                            foreach ($getActiveLine as $lineData) {
                                                $sel="";
                                                if ($line_id == $lineData->line_id) {
                                                    $sel = "selected";
                                                }
                                                echo "<option value=".$lineData->line_id." ".$sel.">".$lineData->line_name."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Area</label>
                                <div class="col-md-8">
                                    <select name="area" id="area" class="form-control clickdropnone" readonly>
                                        <option value="">Select Area</option>
                                        <?php
                                            foreach ($getActiveArea as $areaData) {
                                                $se="";
                                                if ($area_id == $areaData->area_id) {
                                                    $se = "selected";
                                                }
                                                echo "<option value=".$areaData->area_id." ".$se.">".$areaData->area_name."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Equipment Code</label>
                                <div class="col-md-8">
                                    <select name="equ_code" id="equ_code" class="form-control clickdropnone" readonly>
                                        <option value="">Select Equipment Code</option>
                                        <?php
                                            foreach ($getActiveEquipment as $equipmentData) {
                                                $s="";
                                                if ($equ_code == $equipmentData->equ_code) {
                                                    $s = "selected";
                                                }
                                                echo "<option value=".$equipmentData->equ_code." ".$s.">".$equipmentData->equ_code."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Equipment Name</label>
                                <div class="col-md-8">
                                  <select name="equipment_name" id="equipment_name" class="form-control clickdropnone" readonly>
                                        <option value="">Select Equipment Name</option>
                                        <?php
                                            foreach ($getActiveEquipment as $equipmentData) {
                                                $select="";
                                                if ($equ_id == $equipmentData->equ_id) {
                                                    $select = "selected";
                                                }
                                                echo "<option value=".$equipmentData->equ_id." ".$select.">".$equipmentData->equ_name."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>supervisor/assigntask"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>