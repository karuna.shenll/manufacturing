<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/maintenance.png" class="imgbasline"> Edit Maintenance
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_maintenance" id="frm_maintenance" action="<?php echo base_url() ?>supervisor/maintenance" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Euipment Name</label>
                                <div class="col-md-8">
                                    <select name="sel_eqiupment" id="sel_eqiupment" class="form-control">
                                        <option value="">Select Eqiupment</option>
                                        <option value="BA Stepper Motor Press">BA Stepper Motor Press</option>
                                        <option value="BA Dialplate Assy">BA Dialplate Assy</option>
                                        <option value="BA Mask & Lens Assy">BA Mask & Lens Assy </option>
                                        <option value="BA Pointer Press">BA Pointer Press</option>
                                    </select>
                                    
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Group</label>
                                <div class="col-md-8">
                                    <select id='sel_category' name='sel_category' class="form-control" disabled>
                                        <option  value="">Select Group</option>
                                        <option  value="Group 1">Group 1</option>
                                        <option  value="Group 2">Group 2</option>
                                        <option  value="Group 3">Group 3</option>
                                        <option  value="Group 4">Group 4</option>
                                        <option  value="Group 5">Group 5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Line Name</label>
                                <div class="col-md-8">
                                    <select id='sel_assembly' name='sel_assembly' class="form-control" disabled>
                                        <option  value="">Select Line</option>
                                        <option  value="W105/U301/BOLERO">W105/U301/BOLERO</option>
                                        <option  value="B515/HA">B515/HA</option>
                                        <option  value="HONDA/X11M">HONDA/X11M</option>
                                        <option  value="TATA X0/X1/X2">TATA X0/X1/X2</option>
                                        <option  value="W201/W207/NANO">W201/W207/NANO</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Frequency</label>
                                <div class="col-md-8">
                                   <select id='sel_frequency' name='sel_frequency' class="form-control">
                                        <option  value="">Select Frequency</option>
                                        <option  value="Weekly">Weekly </option>
                                        <option  value="Monthly" selected>Monthly</option>
                                        <option  value="Quateraly">Quateraly</option>
                                        <option  value="Haly Yearly">Haly Yearly</option>
                                        <option  value="Yearly">Yearly</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Execution Period</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <div class="icheck-inline">
                                            <label class="">
                                                <div class="iradio_flat-green" style="position: relative;"><input type="radio" name="radio2" class="icheck customweekbtn" data-radio="iradio_flat-green" value="Weekly" checked><ins class="iCheck-helper"></ins></div> Weekly 
                                            </label>
                                            <label class="">
                                                <div class="iradio_flat-green " style="position: relative;"><input type="radio" name="radio2" class="icheck customweekbtn" data-radio="iradio_flat-green" value="Monthly"><ins class="iCheck-helper" ></ins></div> Monthly  
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Comments</label>
                                <div class="col-md-8">
                                    <textarea type="text" class="form-control" name="comments" id="comments" placeholder="Comments" value=""></textarea>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row assignweekshow" style="display:none">
                        <div class="col-md-6 paddingbottom">
                            <div class="form-group">
                                <label class="control-label col-md-4">Execution Weekly</label>
                                <div class="col-md-8">
                                    <select id='sel_week' name='sel_week' class="form-control">
                                        <option  value="">Select Weekly</option>
                                        <option  value="1" selected>1</option>
                                        <option  value="2">2</option>
                                        <option  value="3">3</option>
                                        <option  value="4">4</option>
                                        <option  value="5">5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6 paddingbottom">
                            
                        </div>
                        <!--/span-->
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>supervisor/maintenance"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>