<?php $this->load->view('supervisor/components/header'); ?>
<?php include('components/session_check.php'); ?>
<body>
	<div class="page-container">
		<?php $this->load->view('supervisor/components/sidebar'); ?>
		<div class="page-content-wrapper">
			<?php echo $subview ?>
		</div>
	</div>
	<?php $this->load->view('supervisor/components/footer'); ?>
