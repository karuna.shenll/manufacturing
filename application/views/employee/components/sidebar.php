<?php
error_reporting(0);
$navigationURL = reset(explode("?",end(explode("/",$_SERVER["REQUEST_URI"]))));
if (is_numeric($navigationURL)) {
    $navigationURL=$this->uri->segment(3);
}
?>
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 0px">
			<li class="sidebar-toggler-wrapper hide">
				<div class="sidebar-toggler">
					<span></span>
				</div>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='dashboard')?'active open':''; ?>">
				<a href="<?php echo base_url().'employee/dashboard'; ?>" class="nav-link nav-toggle">
					<i class="dashboard-image"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='task' || $navigationURL=='addtask' || $navigationURL=='edittask')?'active open':''; ?>">
				<a href="<?php echo base_url().'employee/task'; ?>" class="nav-link nav-toggle">
					<i class="icon-tasklist"></i> 
					<span class="title">Task List </span>
					<span class="selected"></span>
				</a>
			</li>
			<li class="nav-item <?php echo ($navigationURL=='opentask' || $navigationURL=='addopentask' || $navigationURL=='editopentask')?'active open':''; ?>">
				<a href="<?php echo base_url().'employee/opentask'; ?>" class="nav-link nav-toggle">
					<i class="icon-opentask"></i> 
					<span class="title">Open task </span>
					<span class="selected"></span>
				</a>
			</li>
		</ul>
	</div>
</div>