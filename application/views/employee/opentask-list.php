<?php
$search_code = $search_model = $search_name = "";
if (!empty($search)) {
    $search_code  = $search["equcode"];
    $search_model  = $search["equmodel"];
    $search_name  = $search["equname"];
}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
	    <div class="alert alert-success alert-dismissible">
	      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	          <?php echo $msg ?>
	    </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/opentask.png" class="imgbasline"> Open Task</div>
            <div class="actions">
               <!--  <a href="task/addtask" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Task</a> -->
            </div>
        </div>
        <div class="portlet-body">
        	<form name="frm_openlist" id="frm_openlist" method="POST">
	        	<div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="task[equcode]" id="equcode" placeholder="Equipment Code" value="<?php echo $search_code ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="task[equmodel]" id="equmodel" placeholder="Equipment Model" value="<?php echo $search_model ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="task[equname]" id="equname" placeholder="Equipment Name" value="<?php echo $search_name ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."employee/opentask"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Equipment Code</th>
		                        <th>Equipment Model</th>
		                        <th>Equipment Name</th>
		                        <th>Status</th>
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                    if (!empty($openTaskList)) {
		                    	$sno=1;
		                    	foreach($openTaskList as $getTaskList) {
		                    ?>
		                    <tr>
		                        <td><?php echo $sno ?></td>
		                        <td><?php echo $getTaskList->equ_code ?></td>
		                        <td><?php echo $getTaskList->equ_model ?></td>
		                        <td><?php echo $getTaskList->equ_name ?></td>
		                        <td><span class="label label-sm label-success labelradius"><?php echo $getTaskList->task_status ?></span></td>
		                        <td><a href="javascript:void(0);" type="button" class="btn grey-cascade btn-xs customaddbtn assigntaskpopup" title="Assign Me" data-id="<?php echo $getTaskList->task_id ?>"><i class="fa fa-ticket"></i> Assign Me</a></td>
		                    </tr>
		                    <?php 
		                        $sno++;
		                		}
		                    }
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>
<div class="modal fade in" id="assigntaskpopup" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        	<form class="assing_status" id="assing_status" method="POST">
        		<input type="hidden" name="hndtaskid" id="hndtaskid">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	                <h4 class="modal-title"><b>Assign Task</b></h4>
	            </div>
	            <div class="modal-body"> 
	                <h3 class="assignmetext">Are you sure want to assign me a task?</h3>
	            </div>
	            <div class="modal-footer">
	            	<button type="button" class="btn green customaddbtn" id="Assingmetask"> <i class="fa fa-check"></i> Yes </button>
	            	<button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> No </button>
                </div>
            </form>
        <!-- /.modal-content -->
       </div>
    <!-- /.modal-dialog -->
   </div>
</div>
<!-- END CONTENT BODY -->