<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tasklist.png" class="imgbasline"> Add Task
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_task" id="frm_task" action="<?php echo base_url() ?>employee/task" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <h3 class="emptaskhead">PM ADHERENCE REPORT</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                            <table class="table table-striped table-bordered table-hover" id="employeesheet">
                                <thead>
                                    <tr>
                                        <th nowrap>Machine Name :</th>
                                        <th>Light Pipe Assembly</th>
                                        <th></th>
                                        <th></th>
                                        <th>Month:</th>
                                    </tr>
                                    <tr>
                                        <th>Frequency :</th>
                                        <th>MONTHLY</th>
                                        <th></th>
                                        <th>Machine ID :1505</th>
                                        <th>Year : 2014</th>
                                    </tr>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Activcity Description</th>
                                        <th>Observation</th>
                                        <th>Action Taken</th>
                                        <th>Comments</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            Air flow control valve should be in correct position.
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="observation" id="observation" placeholder="Observation" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="action_taken" id="action_taken" placeholder="Action Taken" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="comments" id="comments" placeholder="Comments">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>
                                            No bend and damage in the cylinder.
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="observation" id="observation" placeholder="Observation" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="action_taken" id="action_taken" placeholder="Action Taken" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="comments" id="comments" placeholder="Comments">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>
                                            Cylinder screws should not be in loosen condition
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="observation" id="observation" placeholder="Observation" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="action_taken" id="action_taken" placeholder="Action Taken" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="comments" id="comments" placeholder="Comments">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>
                                            Pneumatic hose should be proper routing condition
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="observation" id="observation" placeholder="Observation" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="action_taken" id="action_taken" placeholder="Action Taken" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="comments" id="comments" placeholder="Comments">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>
                                            No air leak in the cylinder end
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="observation" id="observation" placeholder="Observation" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="action_taken" id="action_taken" placeholder="Action Taken" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="comments" id="comments" placeholder="Comments">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>
                                            Cylinder seal should be in good condition
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="observation" id="observation" placeholder="Observation" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="action_taken" id="action_taken" placeholder="Action Taken" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="comments" id="comments" placeholder="Comments">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>
                                            All pneumatic fitting should be in good  condition.
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="observation" id="observation" placeholder="Observation" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="action_taken" id="action_taken" placeholder="Action Taken" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="comments" id="comments" placeholder="Comments">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>
                                            All pneumatic hoses should be in good condition.
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="observation" id="observation" placeholder="Observation" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="action_taken" id="action_taken" placeholder="Action Taken" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="comments" id="comments" placeholder="Comments">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>
                                            Sensor should be in correct position & No damage to sensor & cable
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="observation" id="observation" placeholder="Observation" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="action_taken" id="action_taken" placeholder="Action Taken" value="">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="comments" id="comments" placeholder="Comments">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td><td><b>Done by : <input type="text"  name="done_by" id="done_by" placeholder="Done by" value="" disabled style="width:200px;height:32px"></b></td><td></td><td></td><td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td><b>Date : &nbsp;&nbsp; &nbsp; &nbsp;<input type="text"  name="date" id="date" placeholder="Date" value="" style="width:200px;height:32px"></b></td><td></td><td><b>Verified by :</td><td> <input type="text"  name="verified_by" id="verified_by" placeholder="Verified by" value="" disabled class="form-control"></b></td>
                                    </tr>
                                    <tr>
                                        <td></td><td>DOCUMENT NO : FA_PM_309</td><td></td><td></td><td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td>REVISION LEVEL : <input type="text"  name="date" id="date" placeholder="REVISION LEVEL" value="NIL" disabled style="width:200px;height:32px"></td><td></td><td></td><td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td>REVISION DATE : <input type="text"  name="date" id="date" placeholder="REVISION DATE" value="NIL" disabled style="width:200px;height:32px"></td><td></td><td></td><td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-actions formbtncenter">
                    <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button>
                    <a href="<?php echo base_url() ?>employee/task"  class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Cancel</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>