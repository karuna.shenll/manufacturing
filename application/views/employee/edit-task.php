<?php
if (count($getViewtask["taskdetails"])>0) {
    $task_id        =   $getViewtask["taskdetails"]["task_id"];
    $equ_code       =   $getViewtask["taskdetails"]["equ_code"];
    $equ_model      =   $getViewtask["taskdetails"]["equ_model"];
    $equ_name       =   $getViewtask["taskdetails"]["equ_name"];
    $frequency      =   $getViewtask["taskdetails"]["frequency"];
    $created_date   =   $getViewtask["taskdetails"]["created_date"];
    $doc_no         =   $getViewtask["taskdetails"]["doc_no"];
    $revision_add   =   $getViewtask["taskdetails"]["revision_add"];
    $revision_date  =   $getViewtask["taskdetails"]["revision_date"];
}
$supervisor_name='';
if (!empty($getViewtask["assingtask"])) {
    $supervisor_name        =   !empty($getViewtask["assingtask"]["supervisor_name"])?$getViewtask["assingtask"]["supervisor_name"]:"";
}
 $revision_level= $verified_by ='';
if (count($getViewtask["logtaskdetails"])>0) {
    $completed_date = $getViewtask["logtaskdetails"][0]["completed_date"];
    if ($completed_date=="0000-00-00" || $completed_date=="" ){
        $completed_date ="";
    } else{
        $completed_date = date("d/m/Y",strtotime($getViewtask["logtaskdetails"][0]["completed_date"]));
    }
}
?>
<div class="page-content">
    <?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tasklist.png" class="imgbasline"> Edit Task
            </div>
            <div class="actions">
                <a href="<?php echo base_url() ?>employee/task"  class="btn red customrestbtn"> <i class="fa fa-chevron-left"></i> Back</a>
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frms_task" id="frms_task" action="" class="horizontal-form" method="POST">
                <input type="hidden" name="hndid" value="">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12 paddingbottom">
                            <h3 class="emptaskhead">PM ADHERENCE REPORT</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive" style="margin-top:0px;">
                            <table class="table table-striped table-bordered table-hover" id="employeesheet">
                                <thead>
                                    <tr>
                                        <th nowrap>Machine Name :</th>
                                        <th><?php echo $equ_name?></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>Month :<?php echo date("F"); ?></th>
                                    </tr>
                                    <tr>
                                        <th>Frequency :</th>
                                        <th><?php echo $frequency?></th>
                                        <th></th>
                                        <th></th>
                                        <th>Machine ID : <?php echo $equ_model?></th>
                                        <th>Year : <?php echo date("Y"); ?></th>
                                    </tr>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Activcity Description</th>
                                        <th>Observation</th>
                                        <th>Action Taken</th>
                                        <th>Comments</th>
                                        <th>Comments</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($getViewtask["logtaskdetails"])>0) {
                                        $sno=1;
                                        foreach ($getViewtask["logtaskdetails"] as $addtaskdetails) {
                                           
                                    ?>
                                    <tr>
                                        <td><?php echo $sno ?></td>
                                        <td>
                                            <?php echo $addtaskdetails["task_name"] ?>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="observation_<?php echo $sno?>" id="observation_<?php echo $sno?>" placeholder="Observation" value="<?php echo $addtaskdetails["observation"]?>">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="action_taken_<?php echo $sno?>" id="action_taken_<?php echo $sno?>" placeholder="Action Taken" value="<?php echo $addtaskdetails["action_taken"]?>">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="comments_<?php echo $sno?>" id="comments_<?php echo $sno?>" placeholder="Comments" value="<?php echo $addtaskdetails["comments"]?>">
                                        </td>
                                        <td>

                                            <select name="selstatus_<?php echo $sno?>" id="selstatus_<?php echo $sno?>" class="form-control">
                                            <option  value="">Status</option>
                                            <option  value="Inprogress" <?php if($addtaskdetails['log_status']=='Inprogress') { echo 'Selected'; } ?>>Inprogress</option>
                                            <option  value="Completed" <?php if($addtaskdetails['log_status']=='Completed') { echo 'Selected'; } ?>>Completed</option>
                                            </select>

                                          <!--<a href="javascript:void(0);" type="button" class="btn grey-cascade btn-md customviewbtn statustaskpopup" title="change status" data-id="<?php echo $addtaskdetails["log_task_id"]?>" data-task="<?php echo $addtaskdetails["task_id"]?>"><i class="fa fa-tasks"></i> Status</a>-->
                                        </td>
                                        <td nowrap>
                                           
                                            <a type="button" class="btn green customaddbtn" id="savetaskbtn" data-save="<?php echo $addtaskdetails["log_task_id"]?>" data-inc="<?php echo $sno?>" data-taskid="<?php echo $addtaskdetails["task_id"]?>">
                                                <i class="fa fa-check"></i> Save
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                        $sno++;
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <td></td><td class="text-right"><b>Done by :</b></td><td><input type="text"  name="done_by" id="done_by" class="form-control" placeholder="Done by" value="<?php echo $this->session->userdata('emp_name')?>" readonly ></td><td></td><td></td><td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td class="text-right"><b>Date :</b></td><td><input type="text"  name="completed_date" id="completed_date" placeholder="Date" class="form-control" value="<?php echo $completed_date?>"></td><td class="text-right"><b>Verified by :</td><td> <input type="text"  name="verified_by" id="verified_by" placeholder="Verified by" value="<?php echo $supervisor_name?>" readonly class="form-control"></b></td><td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td class="text-right"><b>DOCUMENT NO :</b></td><td><?php echo $doc_no ?></td><td></td><td></td><td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td class="text-right"><b>REVISION LEVEL :</b></td><td><input type="text"  name="revision_level" id="revision_level" class="form-control" placeholder="REVISION LEVEL" value="<?php echo $revision_add ?>" readonly ></td><td></td><td></td><td></td>
                                    </tr>
                                    <tr>
                                        <td></td><td class="text-right"><b>REVISION DATE :</b></td><td><input type="text"  name="revision_date" id="revision_date" placeholder="REVISION DATE"  class="form-control" value="<?php echo date("d/m/Y",strtotime($revision_date)) ?>" readonly></td><td></td><td></td><td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="form-actions text-right">
                   <!--  <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                    </button> -->
                    <a href="<?php echo base_url() ?>employee/task"  class="btn red customrestbtn"> <i class="fa fa-chevron-left"></i> Back</a>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- <div class="modal fade in" id="statustaskpopup" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="frm_status" id="frm_status" method="POST">
                <input type="hidden" name="hndtaskid" id="hndtaskid">
                <input type="hidden" name="hndlogtaskid" id="hndlogtaskid"> 
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><b>Change Status</b></h4>
                </div>
                <div class="modal-body"> 
                    <div class="row">
                       <div class="col-md-12">
                           <div class="form-group">
                                <label class="control-label">Status:</label>
                                <div class="">
                                    <select id='selstatus' name='selstatus' class="form-control">
                                        <option  value="">Select Status</option>
                                        <option  value="Inprogress">Inprogress</option>
                                        <option  value="Completed">Completed</option>
                                    </select>
                                </div>
                             </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green customsavebtn" id="statusChangePopup"> <i class="fa fa-check"></i> Save </button>
                    <button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> Close </button>
                </div>
            </form>
        <!-- /.modal-content -->
       </div>
    <!-- /.modal-dialog -->
   </div>
</div> -->
<!-- END CONTENT BODY -->