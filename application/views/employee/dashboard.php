<div class="page-content"  id="dashboard">
    <?php
    $msg=$this->session->flashdata('success');
    if(!empty($msg)){
    ?>
    <div>
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo $msg ?>
        </div>
    </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/opentask.png" class="imgbasline"> Today Task</div>
            <div class="actions">
               <!--  <a href="task/addtask" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Task</a> -->
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
                    <thead>
                        <tr>
                            <th style="width: 50px;">SI.NO</th>
                            <th>Equipment Code</th>
                            <th>Equipment Model</th>
                            <th>Equipment Name</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    </tbody>
                    <?php
                        if (!empty($getAssignedTask)) {
                            $sno=1;
                            foreach ($getAssignedTask as $taskData) {
                    ?>
                        <tr>
                            <td><?php echo  $sno?></td>
                            <td><?php echo $taskData->equ_code;?></td>
                            <td><?php echo $taskData->equ_model;?></td>
                            <td><?php echo $taskData->equ_name;?></td>
                            <td><span class="label label-sm label-success labelradius"><?php echo $taskData->task_status;?></span></td>
                        </tr>
                    <?php
                        $sno++;
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>