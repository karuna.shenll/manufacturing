<?php
$search_code = $search_model = $search_name= "";
if (!empty($search)) {
    $search_code   = $search["equcode"];
    $search_model  = $search["equmodel"];
    $search_name   = $search["equname"];
}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
    $msg=$this->session->flashdata('message_success');
    if(!empty($msg)) {
    ?>
        <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $msg ?>
        </div>
    <?php
    }
    ?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                 <img src="<?php echo base_url(); ?>assets/layouts/layout/img/de-active/tasklist.png" class="imgbasline"> Task List</div>
            <div class="actions">
                <!-- <a href="task/addtask" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Task</a> -->
            </div>
        </div>
        <div class="portlet-body">
        	<form id="frm_emptask" name="frm_emptask" method="POST">
	        	<div class="row">
		        	<div class="col-md-12 paddingleftright">
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="task[equcode]" id="equcode" placeholder="Equipment Code" value="<?php echo $search_code ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="task[equmodel]" id="equmodel" placeholder="Equipment Model" value="<?php echo $search_model ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3 paddingbottom">
		        			<div class="col-md-12 paddingleftright">
	                           <input type="text" class="form-control" name="task[equname]" id="equname" placeholder="Equipment Name" value="<?php echo $search_name ?>">
	                        </div>
		        		</div>
		        		<div class="col-md-3">
		        			<div class="col-md-12 paddingleftright">
		        				<button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
		        				<a href="<?php echo base_url()."employee/task"?>" type="button" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
		        			</div>
		        		</div>
		        	</div>
		        </div>
	        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
		            <table class="table table-striped table-bordered table-hover suppliertbl" id="admin-list">
		            	<thead>
		                    <tr>
		                        <th style="width: 50px;">SI.NO</th>
		                        <th>Equipment Code</th>
		                        <th>Equipment Model</th>
		                        <th>Equipment Name</th>
		                        <th>Status</th>
		                        <th>Action </th>
		                    </tr>
		                </thead>
		                </tbody>
		                    <?php
		                    if (!empty($AssignedTaskList)) {
		                    	$sno=1;
		                    	foreach ($AssignedTaskList as $taskdata) {
		                    	$status=$taskdata->task_status;
		                    	if (strtolower($status)=="open") {
                                    $label="info";
		                    	}
		                    	else if (strtolower($status)=="inprogress") {
		                    		$label="warning";
		                    	}
		                    	else if (strtolower($status)=="completed") {
		                    		$label="completed";
		                    	} else {
		                    		$label="success";
		                    	}
		                    ?>
		                    <tr>
		                        <td><?php echo  $sno ?></td>
		                        <td><?php echo  $taskdata->equ_code ?></td>
		                        <td><?php echo  $taskdata->equ_model ?></td>
		                        <td><?php echo  $taskdata->equ_name ?></td>
		                        <td><span class="label label-sm label-<?php echo $label?> labelradius"><?php echo  $status ?></span></td>
		                        <td> <a href="task/edittask/<?php echo  $taskdata->task_id ?>" type="button" class="btn grey-cascade btn-xs custominvitebtn" title="Edit"><i class="fa fa-edit"></i> Edit</a> <!-- <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn" title="Delete"><i class="fa fa-trash"></i> Delete</a> --></td>
		                    </tr>
		                    <?php 
		                        $sno ++;
		                    	}
		                    }
		                    ?>
		                </tbody>
		            </table>
		        </div>
		    </form>
        </div>
    </div>
</div>