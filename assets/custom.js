/*manufacturing-maintenance script start here*/
$(document).ready(function() {
	$('#assembly').multiselect({includeSelectAllOption:true,buttonWidth: '310px',nonSelectedText: "Select Line",});
});
var i = 1;
$(document).on('click','#addmore', function() {
	var selectEqiupment=$(this).parent().prev().html();
	var newfield ='<div class="col-md-12 col-sm-12 col-xs-12 margintop10 paddingleftright"><div class="form-group"><label class="control-label col-md-4" style="padding-right: 0px;"></label><div class="col-md-7">'+selectEqiupment+'</div><div class="col-md-1"><button type="button" class="btn btn-danger customaddmorebtn" name="remove" id="remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div></div>';
	
	$('#appendContent').append(newfield);
	i++;
});
$(document).on('click','#remove', function(){
	$(this).parent().parent().parent('div').remove();
});
i++;

var j=1;
$(document).on('click','#addtaskmore', function(){
	
	var newtaskfield ='<div class="col-md-12 col-sm-12 col-xs-12 paddingleftright"><div class="form-group"><label class="control-label col-md-3"><span class="required" aria-required="true"></span></label><div class="col-md-5"><input type="text" class="form-control" name="add_task[]" id="add_task_'+j+'" placeholder="Add Task (Position)" value=""> <input type="hidden" class="form-control" name="hidden_log[]" id="hidden_log"  value=""></div><div class="col-md-1"><button type="button" class="btn btn-danger customaddmorebtn" name="removetask" id="removetask"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></div></div>';
	$('#appendTaskContent').append(newtaskfield);
	    var answers	=$('#add_task_'+j).val();
        if(answers=="")
		{
			$(document).find('#add_task_'+j).rules('add', {
				required: true,
				messages: {
					required: 'Please enter add task'
				}
			});
		}
	j++;
});
$(document).on('click','#removetask', function(){
	$(this).parent().parent().parent('div').remove();
});
j++;
$(document).ready(function () {
	var checked =$(".checked input[name=exe_period]").val();
	//alert(checked);
	if (checked=="Weekly") {
		$(".assignweekshow").css("display","block");
	} else {
		$(".assignweekshow").css("display","none");
	}
    $('input').on('ifClicked', function (event) {
        var data = $(this).val();
        if (data=="Weekly") {
    		$(".assignweekshow").css("display","block");
		} else {
			$(".assignweekshow").css("display","none");
		}
    });
});
$(document).ready(function () {
	var selReport=$("#select_report option:selected").val();
	if (selReport=="Cluster1") {
        $(".clusteroneshow").css("display","block");
	} else {
	   $(".clustertwoshow").css("display","none");
	   $(".clusteroneshow").css("display","none");
	}
});
$(document).on('click','.opentaskpopup', function() {
	$("#changesempid").val("");
	$("#empname").val("");
	var task_id=$(".opentaskpopup").attr("data-id");
	if (task_id!="") {
       $.ajax({
	        url: base_url+"admin/common/commonTaskPopupAjax",
	        type: 'POST',
	        data: {task_id: task_id},
	        dataType: 'text',
	        success: function(data) {
	        	var result= data.split("##$$##");
	        	if (result[0]=="success") {
	        		$("#assignPopup").modal("show");
	        		$("#hnd_id").val(result[1]);
	        		$("#appendline").val(result[2]);
	        		$("#appendarea").val(result[3]);
	        		$("#appendequcode").val(result[4]);
	        		$("#appendequname").val(result[5]);
	        		$("#selfrequency").val(result[6]);
	        		
	        	} else {
	        		$("#assignPopup").modal("show");
	        		$("#hnd_id").val("");
	        		$("#appendline").val("");
	        		$("#appendarea").val("");
	        		$("#appendequcode").val("");
	        		$("#appendequname").val("");
	        		$("#selfrequency").val("");
	        	}
	        }
	    });
	}
	
});
$(document).on("click","#statuschange",function(){
	$('#assignPopup .modal-content').addClass('pageloader');
	$('#loader').show();

	$("#assing_status").valid();
	var e_id      = $("#hndeid").val();
	var task_id   = $("#hnd_id").val();
	var emp_name  = $("#empname").val();
	var line_id   = $("#appendline").val();
	var area_id   = $("#appendarea").val();
	var equcode   = $("#appendequcode").val();
	var equid     = $("#appendequname").val();
	var frequency = $("#selfrequency").val();
	var wurl      = window.location.href;
	if (e_id!="") {
        $.ajax({
	        url: base_url+"admin/common/commonAssignAjax",
	        type: 'POST',
	        data: {employeeid: e_id,emp_name:emp_name,task_id:task_id,line_id:line_id,area_id:area_id,equcode:equcode,equid:equid,frequency:frequency},
	        dataType: 'text',
	        success: function(result) {
	        	if (result=="success") {
	        		setTimeout(function(){ window.location.href=wurl },1000);
	        	} else {
	        	}
	        }
	    });
	} else {
		return false;
	}
});
$(document).on('click','.viewpopup',function(){
   $("#viewPopup").modal("show");
});
$(document).on('click','.searchreport', function() {
	var select_report=$("#select_report").val();
	if(select_report=="Cluster1"){
       $(".clusteroneshow").css("display","block");
       $(".clustertwoshow").css("display","none");
	} else if(select_report=="Cluster2"){
		$(".clustertwoshow").css("display","block");
		$(".clusteroneshow").css("display","none");
	} else{
       $(".clustertwoshow").css("display","none");
	   $(".clusteroneshow").css("display","none");
	}
});
$(document).on("click",".assigntaskpopup", function() {
   var task_id=$(".assigntaskpopup").attr("data-id");
   $("#hndtaskid").val(task_id);
   $("#assigntaskpopup").modal("show");
});
$(document).on("click","#Assingmetask", function() {
	var task_id = $("#hndtaskid").val();
	if (task_id!="") {
        $.ajax({
	        url: base_url+"admin/common/commonAssignmeAjax",
	        type: 'POST',
	        data: {task_id:task_id},
	        dataType: 'text',
	        success: function(result) {
	        	if (result=="success") {
	        		setTimeout(function(){ window.location.href=base_url+"employee/task"},4000);
	        	} else {
	        		$("#assigntaskpopup").modal("hide");
	        	}
	        }
	    });
    }
});
/*manufacturing-maintenance script end here*/
function validate_value(input_value)
{
	if(input_value!='' && typeof input_value != 'undefined' && input_value!=0 && input_value != 'failure' )
		return true;
	else
		return false;
}
function tender_validate_data(data)
{
	return btoa(data);
}

function tender_decrypt_data(data)
{
	return atob(data);
}

function tender_serialize(form)
{
	var form_data = jQuery(form).serialize();
	if(validate_value(form_data)) {
		return tender_validate_data(form_data);
	}
}
$(document).ready(function() {
	$(".dataTables_empty").empty().append("No record(s) found.");
	$('#supplier-quote').DataTable({
		"bPaginate": true,
		"bLengthChange": true,
		// "lengthMenu": [[10, 25, 50,100, -1], [10, 25, 50,100, "All"]],
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":25,
		"ordering": false
	});
	$('#applied-tenders').DataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":25,
		"ordering": false
	});
	$('#award-tenders').DataTable({
		"bPaginate": true,
		"bLengthChange": true,
		// "lengthMenu": [[10, 25, 25,100, -1], [10, 25, 25,100, "All"]],
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":25,
		"ordering": false
	});
	$('#admin-list').DataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":25,
		"ordering": false
	});
	$('#applied-tender').DataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":25,
		"ordering": false
	});
	$('#award-list').DataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":25,
		"ordering": false
	});
	$('#invitetbl').DataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":25,
		"ordering": false
	});
	$('#vendor_bidding_log').DataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":25,
		"ordering": false
	});
	$('#send-invite').DataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":25,
		"ordering": false
	});
	$('#tender-invite').DataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":25,
		"ordering": false
	});
	$('#tendertbl').DataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":25,
		"ordering": false
	});
	$('#vendor-list').DataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":25,
		"ordering": false
	});
	$('#biddingtbl').DataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": false,
		"bInfo": false,
		"iDisplayLength":25,
		"ordering": false
	});
    $(".dataTables_empty").empty().append("No record(s) found.");
});
$(document).ready(function() {
	$("#frm_area").validate({
		rules:{
			area:{required:true,
                remote: {
                    url:base_url+"admin/area/area_name_exists",
                    type:"POST",
                    data:{area_name:function(){ return $("#area").val();},area_id:function(){return $('input[name="hndid"]').val()}},
                },},
			status:{required:true},
		},
		messages: {
			area:{required:"Please enter area",remote:"Group name already exists!"},
			status:{required:"Please select status"},
		},
	});
	$("#frm_group").validate({
		rules:{
			group_name:{required:true,
                remote: {
                    url:base_url+"admin/group/group_name_exists",
                    type:"POST",
                    data:{group_name:function(){ return $("#group_name").val();},group_id:function(){return $('input[name="hndid"]').val()}},
                },
            },
			status:{required:true},
		},
		messages: {
			group_name:{required:"Please enter group name",remote:"Group name already exists!"},
			status:{required:"Please select status"},
		},
	});
	$("#frm_equipment").validate({
		rules:{
			equipment_code:{required:true},
			equipment_model:{required:true},
			equipment_name:{required:true},
			group:{required:true},
		},
		messages: {
			equipment_code:{required:"Please enter equipment code"},
			equipment_model:{required:"Please enter equipment model"},
			equipment_name:{required:"Please enter equipment name"},
			group:{required:"Please select group"},
		},
	});
	$("#frm_line").validate({
		rules:{
			line_name:{required:true},
			doc_no:{required:true},
			area_id:{required:true},
			sel_revision:{required:true},
			"sel_eqiupment[]":{required:true},
		},
		messages: {
			line_name:{required:"Please enter line name"},
			doc_no:{required:"Please enter document number"},
			area_id:{required:"Please select area name"},
			sel_revision:{required:"Please select revision"},
			"sel_eqiupment[]":{required:"Please select eqiupment name"},
		},
	});
	$("#frm_role").validate({
		rules:{
			role:{required:true,
                remote: {
                    url:base_url+"admin/roles/role_name_exists",
                    type:"POST",
                    data:{role_name:function(){ return $("#role").val();},role_id:function(){return $('input[name="hndid"]').val()}},
                },},
			status:{required:true},
		},
		messages: {
			role:{required:"Please enter role",remote:"Role name already exists!"},
			status:{required:"Please select status"},
		},
	});
	$("#frm_supervisor").validate({
		rules:{
			emp_id:{required:true, 
                remote: {
                    url:base_url+"admin/supervisor/supervisor_name_exists",
                    type:"POST",
                    data:{emp_id:function(){ return $("#emp_id").val();},supervisor_id:function(){return $('input[name="hndid"]').val()}},
                },},
			name:{required:true},
			mobile:{required:true,number:true},
			email_id:{required:true,email:true},
			area_id:{required:true},
			"line_id[]":{required:true},
			username:{required:true},
			password:{required:true},
			conpassword:{required: true,equalTo: "#password"},
			usertype:{required:true},
		},
		messages: {
			emp_id:{required:"Please enter employee id",remote:"Employee id already exists!"},
			name:{required:"Please enter name"},
			mobile:{required:"Please enter moblie",number:"Please enter valid moblie number"},
			email_id:{required:"Please enter email",email:"Please enter vaild email"},
			area_id:{required:"Please select area"},
			"line_id[]":{required:"Please select line"},
			username:{required:"Please enter username"},
			password:{required:"Please enter password"},
			conpassword:{required:"Please enter confirm password",equalTo: "Your password and confirmation password do not match"},
			usertype:{required:"Please enter user role"},
		},
	});
	$("#frm_employee").validate({
		rules:{
			emp_id:{required:true, 
                remote: {
                    url:base_url+"admin/employee/employee_name_exists",
                    type:"POST",
                    data:{emp_id:function(){ return $("#emp_id").val();},e_id:function(){return $('input[name="hndid"]').val()}},
                },},
			name:{required:true},
			mobile:{required:true,number:true},
			email_id:{required:true,email:true},
			area_id:{required:true},
			skill:{required:true},
			"line_id[]":{required:true},
			username:{required:true},
			password:{required:true},
			conpassword:{required: true,equalTo: "#password"},
			usertype:{required:true},
		},
		messages: {
			emp_id:{required:"Please enter employee id",remote:"Employee id already exists!"},
			name:{required:"Please enter name"},
			mobile:{required:"Please enter moblie",number:"Please enter valid moblie number"},
			email_id:{required:"Please enter email",email:"Please enter vaild email"},
			area_id:{required:"Please select area"},
			skill:{required:"Please enter skill"},
			"line_id[]":{required:"Please select line"},
			username:{required:"Please enter username"},
			password:{required:"Please enter password"},
			conpassword:{required:"Please enter confirm password",equalTo: "Your password and confirmation password do not match"},
			usertype:{required:"Please enter user role"},
		},
	});
	
	$("#frm_customer").validate({
		rules:{
			customer_name:{required:true},
			mobile:{required:true,number:true},
			email:{required:true,email:true},
			address:{required:true},
			products:{required:true},
			line_id:{required:true},
		},
		messages: {
			customer_name:{required:"Please enter customer name"},
			mobile:{required:"Please enter mobile",number:"Please enter valid mobile number"},
			email:{required:"Please enter email",email:"Please enter valid email"},
			address:{required:"Please enter address"},
			products:{required:"Please enter product"},
			line_id:{required:"Please select line"},
		},
	});
	$("#frm_maintenance").validate({
		rules:{
			sel_equipment:{required:true},
			sel_group:{required:true},
			sel_line:{required:true},
			sel_frequency:{required:true},
			exe_period:{required:true},
			sel_week:{required:true},
			comments:{required:true},
		},
		messages: {
			sel_equipment:{required:"Please select equipment"},
			sel_group:{required:"Please select group"},
			sel_line:{required:"Please select line"},
			sel_frequency:{required:"Please select frequency"},
			exe_period:{required:"Please select execution period"},
			sel_week:{required:"Please select execution weekly"},
			comments:{required:"Please enter comments"},
		},
	});
	$("#frm_task").validate({
		rules:{
			equipment_code:{required:true},
			equipment_model:{required:true},
			equipment_name:{required:true},
			"add_task[]":{required:true},
		},
		messages: {
			equipment_code:{required:"Please select equipment code"},
			equipment_model:{required:"Please select equipment model"},
			equipment_name:{required:"Please select equipment name"},
			"add_task[]":{required:"Please enter add task"},
		},
	});
	$("#assing_status").validate({
		rules:{
			emp_id:{required:true},
			emp_name:{required:true},
			line:{required:true},
			area:{required:true},
			equ_code:{required:true},
			equ_name:{required:true},
			sel_frequency:{required:true},
		},
		messages: {
			emp_id:{required:"Please select employee id"},
			emp_name:{required:"Please enter employee name"},
			line:{required:"Please select line name"},
			area:{required:"Please select area name"},
			equ_code:{required:"Please select equipment code"},
			equ_name:{required:"Please select equipment name"},
			sel_frequency:{required:"Please select frequency"},
		},
	});
	
	$("#frm_assigntask").validate({
		rules:{
			emp_id:{required:true},
			emp_name:{required:true},
			line_id:{required:true},
			area:{required:true},
			equ_code:{required:true},
			equ_name:{required:true}
		},
		messages: {
			emp_id:{required:"Please select employee id"},
			emp_name:{required:"Please enter employee name"},
			line:{required:"Please select line name"},
			area:{required:"Please select area name"},
			equ_code:{required:"Please select equipment code"},
			equ_name:{required:"Please select equipment name"}
		},
	});
	
	$("#frm_status").validate({
		rules:{
			selstatus:{required:true},
		},
		messages: {
			selstatus:{required:"Please select status"},
		},
	});
	$("#frms_task").validate({
		rules:{
			observation_1:{required:true},
			action_taken_1:{required:true},
			comments_1:{required:true},
			completed_date:{required:true},
		},
		messages: {
			observation_1:{required:"Please enter observation"},
			action_taken_1:{required:"Please enter action taken"},
			comments_1:{required:"Please enter comments"},
			completed_date:{required:"Please enter date"},
		},
	});
});
$("#search_result_length").hide();

$( function() {
	$("#award_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
$( function() {
	$("#award_enddate").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
$( function() {
	$("#delivery_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
$( function() {
	$("#start_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy H:i:S',autoclose: true });
});
$( function() {
	$("#end_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy H:i:S',autoclose: true });
});
$( function() {
	$("#dob").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
$(document).on("click",".deleteRowid",function() { 
	var deleteid = $(this).attr("data-id");
	var deltype = $(this).attr("data-type");
	if (deleteid!="" && deltype!="") {
		msg='Are you sure you want to delete this '+deltype+' ?';
		confirmcheck = confirm(msg);
        if (confirmcheck == true) {
        	$.ajax({
		        url: base_url+"admin/common/commonDeleteAjax",
		        type: 'POST',
		        data: {deleteid: deleteid,deltype:deltype},
		        dataType: 'text',
		        success: function(data) {
		        	if (data=="success") {
						location.reload();
		        	}
		        }
		    });
        } else {
        	return false;
        }
	} else {
		return false;
	}
});
$(document).on("click",".viewPopupequ",function() { 
	var popupid = $(this).attr("data-popup");
	if (popupid!="") {
        $.ajax({
	        url: base_url+"admin/common/commonLineEquAjax",
	        type: 'POST',
	        data: {popupid: popupid},
	        dataType: 'text',
	        success: function(data) {
	        	var result= data.split("##^^##");
	        	if (result[0]=="success") {
	        		$("#appendLinedetail").empty().append(result[1]);
	        		$("#viewLineModal").modal("show");
	        	} else {
	        		$("#viewLineModal").modal("hide");
	        	}
	        }
	    });
	} else {
		return false;

	}
});
$(document).on("change","#equipment_name",function(){
    var equipmentid = $("#equipment_name").val();
    //alert(equipmentid);
	if (equipmentid!="") {
        $.ajax({
	        url: base_url+"admin/common/commonEquipmentAjax",
	        type: 'POST',
	        data: {equipmentid: equipmentid},
	        dataType: 'text',
	        success: function(data) {
	        	var result= data.split("##$$##");
	        	if (result[0]=="success") {
	        		$("#equipment_code").val(result[1]);
	        		$("#equipment_model").val(result[2]);
	        	} else {
	        		$("#equipment_code").val("");
	        		$("#equipment_model").val("");
	        	}
	        }
	    });
	} else {
		return false;
	}
});
$(document).on("change","#changesempid",function() {
	var employeeid = $("#changesempid").val();
    //alert(equipmentid);
	if (employeeid!="") {
        $.ajax({
	        url: base_url+"admin/common/commonEmployeeAjax",
	        type: 'POST',
	        data: {employeeid: employeeid},
	        dataType: 'text',
	        success: function(data) {
	        	var result= data.split("##$$##");
	        	if (result[0]=="success") {
	        		$("#empname").val(result[1]);
	        		$("#hndeid").val(result[2]);
	        	} else {
	        		$("#empname").val("");
	        		$("#hndeid").val("");
	        	}
	        }
	    });
	} else {
		return false;
	}
});
$(document).on("click",".statustaskpopup",function() {
	$("#selstatus").val("");
    var task_log_id = $(this).attr("data-id");
	var task_id     = $(this).attr("data-task");
	if(task_log_id!="" && task_id!="") {
		$("#hndlogtaskid").val(task_log_id);
		$("#hndtaskid").val(task_id);
		$.ajax({
	        url: base_url+"admin/common/commonGetStatusAjax",
	        type: 'POST',
	        data: {task_log_id: task_log_id,task_id: task_id},
	        dataType: 'text',
	        success: function(data) {
	        	var result= data.split("##**##");
	        	if (result[0]=="success") {
	        		$("#statustaskpopup").modal("show");
	        		$("#selstatus").val(result[1]);
	        	} else {
	        		$("#selstatus").val("");
	        	}
	        }
	    });
    }
});
$(document).on("click","#statusChangePopup",function() {
	$("#frm_status").valid();
    var task_log_id = $("#hndlogtaskid").val();
	var task_id 	= $("#hndtaskid").val();
	var selstatus   = $("#selstatus").val();

	if(task_log_id!="" && task_id!="" && selstatus!="") {
		 $.ajax({
	        url: base_url+"admin/common/commonStatusAjax",
	        type: 'POST',
	        data: {task_log_id: task_log_id,task_id: task_id,selstatus: selstatus},
	        dataType: 'text',
	        success: function(data) {
	        	if (data=="success") {
	        		location.reload();
	        	} else {
	        	}
	        }
	    });
    }
});

$(document).on("click","#savetaskbtn",function(){
	var task_log_id = $(this).attr("data-save");
	var inc_id      = $(this).attr("data-inc");
    var taskid      = $(this).attr("data-taskid"); 
	$('[name=observation_'+inc_id+']').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "please enter observation"
            }
        });
    });
    $('[name=action_taken_'+inc_id+']').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "please enter action taken"
            }
        });
    });
    $('[name=comments_'+inc_id+']').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "please enter comments"
            }
        });
    });
     $('[name=selstatus_'+inc_id+']').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "please select status"
            }
        });
    });
    var observation = $("#observation_"+inc_id).val();
    var action_taken = $("#action_taken_"+inc_id).val();
    var comments = $("#comments_"+inc_id).val();
    var selstatus = $("#selstatus_"+inc_id).val();
    var done_by = $("#done_by").val();
    var com_date = $("#completed_date").val();
    var rev_level = $("#revision_level").val();
    var rev_date = $("#revision_date").val();
    var verified_by = $("#verified_by").val();
    $("#frms_task").valid();
    if(observation!="" && action_taken!="" && comments!="" && selstatus!="" && task_log_id!="") {
		 $.ajax({
	        url: base_url+"admin/common/commonTaskcompleteAjax",
	        type: 'POST',
	        data: {observation: observation,action_taken: action_taken,comments: comments,task_log_id:task_log_id,done_by:done_by,com_date:com_date,rev_level:rev_level,rev_date:rev_date,verified_by:verified_by,taskid:taskid,selstatus:selstatus},
	        dataType: 'text',
	        success: function(data) {
	        	console.log(data);
	        	if (data=="success") {
	        		location.reload();
	        	} else {
	        	}
	        }
	    });
    }
});
$(document).on("click",".removetasklog",function() {
	var delete_log_id=$(this).attr("data-remove");
	var data_type=$(this).attr("data-type");
	
	if (delete_log_id!="" && data_type =="removeadd") {
		msg='Are you sure you want to delete this task ?';
		confirmcheck = confirm(msg);
        if (confirmcheck == true) {
        	$.ajax({
		        url: base_url+"admin/common/commonAddtaskDeleteAjax",
		        type: 'POST',
		        data: {deleteid: delete_log_id,deltype:data_type},
		        dataType: 'text',
		        success: function(data) {
		        	if (data=="success") {
						location.reload();
		        	}
		        }
		    });
        } else {
        	return false;
        }
	} else {
		return false;
	}

});
$(function () {
    $("#from_date").datepicker({todayHighlight: true, format: 'dd/mm/yyyy', autoclose: true});
});
$(function () {
    $("#to_date").datepicker({todayHighlight: true, format: 'dd/mm/yyyy', autoclose: true});
});
$(function () {
    $("#completed_date").datepicker({todayHighlight: true, format: 'dd/mm/yyyy', autoclose: true});
});